<?php
ob_start();
session_start();
require_once("profile_header.php")



?>
<link rel="stylesheet" type="text/css" href="css/main.css">
<div class="tab-pane" id="edit">
                <div class="container-contact1000 py-3 bg-primary">
		<div class="wrap-contact100 ">
			
			<div class="ml-auto py-4"> <h2>Change Password </h2></div>

				<div>
					<?php
				if(array_key_exists('passChangeErr',$_SESSION)){
									echo  '<p class="text-center bg-danger text-white p-2 mb-2">'. $_SESSION['passChangeErr'].'</p><br>';
									unset($_SESSION['passChangeErr']);
								}

								if(array_key_exists('errors',$_SESSION)){
									$errors=$_SESSION['errors'];

									
									foreach($errors as $error){ 
										
										echo  '<p class="text-center bg-danger text-white p-2 mb-2">'.$error.'</p><br>';
									}
									


									
									unset($_SESSION['errors']);
								}

				?>
							
				</div>
			<form action="changePassProcess.php" method="post" class="contact100-form validate-form">
				<div class="wrap-input100 validate-input">
					<span class="label-input100">Old Password :</span>
					<input class="input100" type="password"  name="oldPass" placeholder="Enter old password">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100">
					<span class="label-input100">New Password:</span>
					<input class="input100" type="password" name="newPass" placeholder="Enter new Password">
					<span class="focus-input100"></span>
				</div>

		 	<div style="width: 100%;"> 
			<div class="container-contact100-form-btn">
					<button type="submit" class="contact100-form-btn">
						 Confirm 	
					</button>
				</div>
				
				</div>
			
			</form>
		</div>
	</div>
                    
                </div>
         



<?php require_once("footer.php")?>