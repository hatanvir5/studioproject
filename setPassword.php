<?php 
ob_start();
session_start();
require 'header.php';

?>
<!-- Custom css start just use only contact form -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<!-- Custom  css end just use only contact form -->

<div class="container-contact1000 py-3 bg-primary">
		<div class="wrap-contact100 ">
			
			<div class="ml-auto py-4"> <h2> Set New Password</h2></div>
<div><p class="text-center">Please Set your Password </p></div>
<div>
<?php 
		  if(array_key_exists('passwordErr',$_SESSION)){
			echo  '<p class="text-center bg-warning text-white p-2 mb-2">'. $_SESSION['passwordErr'].'</p><br>';
			unset($_SESSION['passwordErr']);
		  }
	?>
</div>
			<form action="setPasswordProcess.php" method="post" class="contact100-form validate-form">
				<div class="wrap-input100 validate-input">
					<span class="label-input100">New Password:</span>
					<input class="input100" type="password" name="password" placeholder="Enter New Password">
					<span class="focus-input100"></span>
                </div>
                
                <div class="wrap-input100 validate-input">
					<span class="label-input100">Confirm New Password:</span>
					<input class="input100" type="password" name="confirmPassword" placeholder=" Confirm New Password">
					<span class="focus-input100"></span>
				</div>
			<div class="container-contact100-form-btn">
					<button type="submit" class="contact100-form-btn">
						 Submit
					</button>
				</div>
			</form>
		</div>
	</div>
<?php require_once("footer.php")?>