<?php
ob_start();
session_start();
require 'db.php';


$id='';
if(array_key_exists('id',$_GET)){
    $id=$_GET['id'];

}

if (array_key_exists('id', $_SESSION)) {
    $id=$_SESSION['id'];
}


$email='';
if(array_key_exists('email',$_SESSION)){
  $email=$_SESSION['email'];
     
  }
  
  $number='';
  if(array_key_exists('number',$_SESSION)){
      $number=$_SESSION['number'];
    
      }


  if( $email=='' && $number==''){
    require 'header.php';
  }


if(!empty($email) || !empty($number)){
    require 'profile_header.php';
}



$sql='SELECT * 
from getservicedetails
where getServiceDetailsId=:id';
$statement=$connection->prepare($sql);
$statement->execute([
    'id'=>$id
]);
$singleBLog=$statement->fetch(PDO::FETCH_OBJ);

$sql='SELECT * 
from getservicedetails ORDER BY getServiceDetailsId DESC LIMIT 3 ';
$statement=$connection->prepare($sql);

$statement->execute();
$populerBlog=$statement->fetchAll(PDO::FETCH_OBJ);

?>


<style>
* {
  box-sizing: border-box;
}

/* Add a gray background color with some padding */

/* Header/Blog Title */
.header {
  padding: 30px;
  font-size: 40px;
  text-align: center;
  background: white;
}
.title{
    margin-top:28px;
}

/* Create two unequal columns that floats next to each other */
/* Left column */
.leftcolumn {   
  float: left;
  width: 75%;
}

/* Right column */
.rightcolumn {
  float: left;
  width: 25%;
  padding-left: 20px;
}

/* Fake image */
.fakeimg {
  background-color: #aaa;
  width: 100%;
  padding: 20px;
  margin-top:10px;
}

/* Add a card effect for articles */
.card {
   background-color: white;
   padding: 20px;

}
.share{
    margin-top: 20px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Footer */
.footer {
  padding: 20px;
  text-align: center;
  background: #ddd;
  margin-top: 20px;
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 800px) {
  .leftcolumn, .rightcolumn {   
    width: 100%;
    padding: 0;
  }
}
</style>
<div class="container bg-light"> 
<div class="row">
  <div class="leftcolumn">
    <div class="card">
      <h3><?=$singleBLog->title;?></h3>
      <p>Published: <span>  <?=$singleBLog->date;?></span></p>
      <div style="height:200px;"><img src="images/<?=$singleBLog->serviceImage;?>" height="200px" width="500px"></div>
      <h3 class="title"><?=$singleBLog->catagory;?></h3>
      <p><?=$singleBLog->fullDescription;?></p>
    </div>
  </div>
  <div class="rightcolumn">
    <div class="card">
      <h3>Popular Post</h3>
      <?php foreach($populerBlog as $siglePopulerBlog):?>
      <div>
        <a class="text-dark" href="getServiceDetails.php?id=<?=$siglePopulerBlog->getServiceDetailsId;?>">
        <img src="images/<?=$siglePopulerBlog->serviceImage;?>" height="50px" width="100px">
      <span><?=$siglePopulerBlog->title;?></span></a>
      </div><br>
      <?php endforeach;?>
      
    </div>
    <div class="card share">
      <h3>Share Post</h3>
      <p>Facebook</p>
      <p>Twitter</p>
    </div>
  </div>
</div>

</div>
<?php require_once("footer.php")  ?>
