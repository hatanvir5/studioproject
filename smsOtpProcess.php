<?php
ob_start();
session_start();
require_once 'db.php';
$success = "";
$error_message = "";

if (strtoupper($_SERVER['REQUEST_METHOD'])=='POST') {
    $number=$_POST['phone'];
    $sql="SELECT * FROM users WHERE number='{$number}' ";
    $statement=$connection->prepare($sql);
    $statement->execute();
    $count = $statement->fetchColumn();

    if ($count>0) {
        // generate OTP
        $otp = rand(100000, 999999);
        // Send OTP
        require_once("sms.php");
        // $mail_status = send_sms($number, $otp);

        if (send_sms($number, $otp)) {
            $sql="INSERT INTO otp_expiry(otp,is_expired) VALUES (:otp,0)";
            $statement=$connection->prepare($sql);
            $result=$statement->execute([':otp'=>$otp]);

            $current_id = $connection->lastInsertId();
            if (!empty($current_id)){
                $success=1;
                $_SESSION['otpnumber']=$number;
                header('location:submitotp.php');
            }
        }
    } else {
        $error_message = "Number not exists!";
        $_SESSION['error_message']=$error_message;
        header('location:ForgetPassword.php');


    }
}