<?php
ob_start();
session_start();
require 'db.php';
require 'test.php';
$email='';
if(array_key_exists('email',$_SESSION)){
  $email=$_SESSION['email'];
     
  }
  
  $number='';
  if(array_key_exists('number',$_SESSION)){
      $number=$_SESSION['number'];
    
      }

      if( $email=='' && $number==''){
        require 'header.php';
      }
    
    
    if(!empty($email) || !empty($number)){
        require 'profile_header.php';
    }

$sql='SELECT * 
from chakrinews  ORDER BY id DESC LIMIT 3';
$statement=$connection->prepare($sql);

$statement->execute();
$blog=$statement->fetchAll(PDO::FETCH_OBJ);




$sql2='SELECT * 
from slider LIMIT 1';
$statement1=$connection->prepare($sql2);
$statement1->execute();
$slider=$statement1->fetch(PDO::FETCH_OBJ);


$sql1='SELECT * from servicedesign ORDER BY serviceDesignId DESC  LIMIT 6';
$statement1=$connection->prepare($sql1);
$statement1->execute();
$serviceDesign=$statement1->fetchAll(PDO::FETCH_OBJ);




$sql4='SELECT * 
from notice LIMIT 1';
$statement4=$connection->prepare($sql4);

$statement4->execute();
$notice=$statement4->fetch(PDO::FETCH_OBJ);


$sql1='SELECT * from getservicedesign ORDER BY getServiceDesignId DESC LIMIT 6';
$statement1=$connection->prepare($sql1);
$statement1->execute();
$getServiceDesign=$statement1->fetchAll(PDO::FETCH_OBJ);



$sql="SELECT * FROM review ORDER BY id DESC limit 1";
$statement=$connection->prepare($sql);
$statement->execute();
$reviews=$statement->fetch(PDO::FETCH_OBJ);

$activeId=$reviews->id;


$sql="SELECT * FROM review
where id!='{$activeId}'";
$statement=$connection->prepare($sql);
$statement->execute();
$review=$statement->fetchAll(PDO::FETCH_OBJ);

$sql="SELECT * FROM videopage limit 1";
$statement=$connection->prepare($sql);
$statement->execute();
$video=$statement->fetch(PDO::FETCH_OBJ);



?>



<!-- Slider Image Responsive Mode View  -->
<style> 
a:hover{
	text-decoration: none;
}
.re_font{
	font-size: 20px;
}
@media only screen and (max-width: 600px) {
  .sliderimg {
    height: 300px;
  }
  .rsp{
    padding-top: 3rem !important;
	padding-right: 2px !important;
	padding-left: 2px !important;
	font-size:18px
  }

  .re_font{
	font-size: 18px;
}

}
</style>


<!------------Slider start here----------------->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block sliderimg w-100"  src="images/<?=$slider->imageOne;?>" height="600px" alt="First slide" >
    </div>
    <div class="carousel-item">
      <img class="d-block sliderimg w-100" src="images/<?=$slider->imageTwo;?>"  height="600px" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block sliderimg w-100" src="images/<?=$slider->imageThree;?>"  height="600px" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<!-------------Slider END ---------->

<div class="container">
	<div class="row">
	<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" onclick="this.parentNode.parentNode.removeChild(this.parentNode);" class="close" data-dismiss="alert"><span aria-hidden="true">×</span
  ><span class="sr-only">Close</span>
</button>
  <div class="d-flex"> 
  <strong style="width: 74px;">
	  নোটিশ : 
	</strong>
	 <marquee scrollamount="3"><p style="font-size: 18px"><?=$notice->notice;?></p></marquee>
  </div>
</div>
	</div>
</div> 
<!-------Notice END--->


<!------Post start------>
<div class="container bg-primary pb-5"> 
	<div class="row"> 
	<?php foreach($blog as $singleBlog):?>
		<div class="col-md-4 mt-5"> 
			
			<div class="card" >
  				<img class="card-img-top text-center"  src="images/<?=$singleBlog->image;?>" height="168" alt="Card image cap">
		   	 <div style="height:auto" class="card-body">
					<div class="card-title"><strong class=""><?=lengthCheck($singleBlog->title,0,35);?> </strong></div>
					<p class="text-muted"> <?=$singleBlog->date;?> </p>
    				<p class="card-text"><?=lengthCheck($singleBlog->shortDescription,0,230);?></p>
 				 </div>
				  <a class="btn btn-success" href="chakriFullDescription.php?id=<?=$singleBlog->id;?>"> Read More</a>
			</div>
			
		</div>

	<?php endforeach;?>
	</div>
</div>
<!-----Service start here--->
<section> 
	<div class="container bg-primary"> 
		<div class="row"> 
	    	<div class="col-md-12 mt-5 text-light text-center"> 
				<h2 class="text-light" style="font-size:1.75rem">আমাদের সেবা সমূহ</h2>
			</div>
		</div>
		<div class="row"> 
<div class="container">  
	<div class="row">   
<?php foreach($serviceDesign as $service):?>

	<div class="col-md-4 mt-5"> 
	<a href="<?=$service->url;?>">   
<div class="card text-center p-3"> 
<img alt="" style="height: auto;width: 89px;" class="mx-auto" src="images/<?=$service->image;?>">
 <h4 class="pt-3"> <?=$service->serviceTitle;?> </h4>
</div>
</a>
</div>
<?php endforeach;?>
 </div>    
 </div>
		</div>
	</div>
</section>	


<!-----Service start here--->
<section> 
	<div class="container pt-5 bg bg-primary"> 
		<div class="row"> 
	    	<div class="col-md-12 mt-5 text-light text-center"> 
				<h2 class="text-light" style="font-size:1.75rem"> আপনি কেন আমাদের সেবা নিবেন </h2>
			</div>
		</div>
		<div class="row"> 
			<div class="container">   
				<div class="row">  

		<?php foreach($getServiceDesign as $getService):?>

	<div class="col-md-4 mt-5"> 
	<a href="<?=$getService->url;?>">   
<div class="card text-center p-3"> 
<img alt="" style="height: auto;width: 89px;" class="mx-auto" src="images/<?=$getService->image;?>">
 <h4 class="pt-3"> <?=$getService->serviceTitle;?> </h4>
</div>
</a>
</div>

<?php endforeach;?>
  </div>
</div>
		</div>
	</div>
</section>	

<!-- Testomonial part start -->
<div class="container rsp px-5 bg-primary pt-5">
	    	<div class="col-md-12 pt-5 text-light text-center"> 
				<h2 class="text-light" style="font-size:1.75rem"> কাস্টমার আমাদের সম্পর্কে যা যা বলেছেন </h2>
					
					</div> 
	<div class="row"> 
		<div class="col-md-12 pt-5"> 
			
<div id="myCarousel" class="carousel slide time" data-ride="carousel">
    <!-- Carousel indicators -->
     <!-- Wrapper for carousel items -->
	
    <div class="carousel-inner">
        <div class="item carousel-item active">
            <div class="img-box"><img src="images/<?=$reviews->image;?>" alt=""></div>
            <p class="testimonial text-light re_font"><?=$reviews->fullDescription;?></p>
            <p class="overview text-light"><b class="text-light"><?=$reviews->name;?></b><?=$reviews->designation;?></p>
            <div class="star-rating"> </div>
        </div>
	
<?php foreach($review as $singleReview):?>
	<div class="item carousel-item">
            <div class="img-box"><img src="images/<?=$singleReview->image;?>" alt=""></div>
            <p class="testimonial text-light re_font"><?=$singleReview->fullDescription;?></p>
            <p class="overview text-light"><b class="text-light"><?=$singleReview->name;?></b><?=$singleReview->designation;?></p>
            <div class="star-rating"> </div>
        </div>
<?php endforeach; ?>

    </div> 
</div>
		</div>
	</div>
			
</div>
<!----------------Video Part start Here----------------->
<div class="video_part p-5 bg-primary rsp"> 
	<div class="container"> 
		<div class="row"> 
				<div class="col-md-7 col-sm-12 text-light g-2"> 
					<div class="video_title "> 
						<h3 class="text-center p-3"><?=$video->title;?> </h3>
				</div>
				<div class="video_discription"> 
					<p class=""><?=$video->fullDescription;?></p>
			</div>
				</div>
				<div class="col-md-5 col-sm-12"> 
				<iframe width="100%" height="315" src="https://www.youtube.com/embed/<?=$video->url;?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>

<?php require_once("footer.php")?>