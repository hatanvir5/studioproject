<?php
session_start();
 require_once("profile_header.php");
require 'db.php';


if(array_key_exists('email',$_SESSION)){
    $email=$_SESSION['email'];
       
    }
    
    if(array_key_exists('number',$_SESSION)){
        $number=$_SESSION['number'];
      
        }


        $limit = 15;

        $sql="SELECT * from users
        join jobapplicaton on jobapplicaton.userId=users.id
         WHERE users.email=:email or users.number=:number";
    
        $statement=$connection->prepare($sql);
       $statement->execute([
            ':email'=>$email,
            ':number'=>$number
        ]);
        
        
        $total_results = $statement->rowCount();
        $total_pages = ceil($total_results/$limit);
        
        if (!isset($_GET['page'])) {
            $page = 1;
        } else{
            $page = $_GET['page'];
        }
        
        
        $start = ($page-1)*$limit;
        
        $stmt = $connection->prepare("SELECT * from users
        join jobapplicaton on jobapplicaton.userId=users.id
        WHERE users.email=:email or users.number=:number ORDER BY jobapplicaton.id DESC LIMIT $start, $limit");
        $stmt->execute([
          ':email'=>$email,
          ':number'=>$number
      ]);
        
        // set the resulting array to associative
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        
        $profile = $stmt->fetchAll();
           
        




// $userId='';
// if(isset($_SESSION['userId'])){
//     $userId=$_SESSION['userId'];
//     echo $userId;
// }

// if(isset($_GET['id'])){
//     $userId=$_GET['id'];
// }



// $sql="SELECT * from jobapplicaton
// WHERE userId=:userId";
// $statement=$connection->prepare($sql);
// $statement->execute([
//     ':userId'=>$userId
// ]);
// $profile=$statement->fetchAll(PDO::FETCH_OBJ);

?>

    


<div class=" bg-primary" id="messages" style="padding-bottom:105px;">
                    

<table class="table table-striped text-light" >
  <thead>
    <tr>
    
      <th scope="col">Organization Name</th>
      <th scope="col">Post Name</th>
      <th scope="col">Date</th>
      <th scope="col">Status</th>
      <th scope="col">Amount</th>
      <th scope="col">Pay</th>

      
    </tr>
  </thead>
  <tbody>

  <?php foreach($profile as $activity):?>
    <tr>
  
      <td><?=$activity->organizationName;?></td>
      <td><?=$activity->positionName;?></td>
      <td><?=$activity->date;?></td>
      <td>

      <?php
      if($activity->status==1){
        echo '<span class="bg-success p-2"> Confirm <span>';
      }elseif($activity->status==2){
        echo '<span class="bg-danger p-2"> Rejected <span>';
      }
      
      else{
        echo '<span class="bg-warning text-dark p-2"> Pending <span>';
      }
      ?>
      </td>
      <td> <?=$activity->amount + 50;?></td>
      <td> 
      <a class="btn btn-success" href="#"> Pay here</a>
      </td>

    </tr>
    <?php endforeach;?>
  </tbody>
</table>
<div class="row">
            <div class="col-md-8"></div>
            <div class="mt-5 col-md-4">
            <ul class="pagination justify-content-end mr-2">
         <li class="page-item"><a class="page-link" href="?page=1">First</a></li>
        
        <?php for($p=1; $p<=$total_pages; $p++){?>
            
            <li class="<?= $page == $p ? 'active' : ''; ?>"><a class="page-link" href="<?= '?page='.$p; ?>"><?= $p; ?></a></li>
        <?php }?>
        <li class="page-item"><a class="page-link" href="?page=<?= $total_pages; ?>">Last</a></li>
  </ul>
</div>
          </div>
</div>

<?php require_once("footer.php")?>