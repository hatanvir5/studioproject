<?php
ob_start();
session_start();
require_once("header.php");
require_once("db.php");




if(array_key_exists('email',$_SESSION)){
$email=$_SESSION['email'];
    header('location:profile.php');
}


if(array_key_exists('number',$_SESSION)){
    $number=$_SESSION['number'];
    header('location:profile.php');
   
    }




?>
<!-- Custom css start just use only contact form -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<!-- Custom  css end just use only contact form -->

<div class="container-contact1000 py-3 bg-primary">
		<div class="wrap-contact100 ">
			
			<div class="ml-auto py-4"> <h2> LOG IN </h2></div>
			<div class="text-center">
				<?php
				  if(array_key_exists('success',$_SESSION)){
					echo  '<p class="text-center bg-success text-white p-2 mb-2">'. $_SESSION['success'].'</p><br>';
					unset($_SESSION['success']);
				  }

				  if(array_key_exists('loginError',$_SESSION)){
					echo  '<p class="text-center bg-danger text-white p-2 mb-2">'. $_SESSION['loginError'].'</p><br>';
					unset($_SESSION['loginError']);
				  }

				  
				  if(array_key_exists('loginErrorShow',$_SESSION)){
					echo  '<p class="text-center bg-danger text-white p-2 mb-2">'. $_SESSION['loginErrorShow'].'</p><br>';
					unset($_SESSION['loginErrorShow']);
				  }
				  if(array_key_exists('passChangeMsg',$_SESSION)){
					echo  '<p class="text-center bg-success text-white p-2 mb-2">'. $_SESSION['passChangeMsg'].'</p><br>';
					unset($_SESSION['passChangeMsg']);
					session_destroy();
				  }
				  
				?>
			</div>
			<form action="loginProcess.php" method="post" class="contact100-form validate-form">
				<div class="wrap-input100 validate-input">
					<span class="label-input100">Email or Phone :</span>
					<input class="input100" type="text" name="emailPhone" placeholder="Enter Phone Or Email addess">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100">
					<span class="label-input100">Password:</span>
					<input class="input100" type="password" name="Password" placeholder="Enter Password">
					<span class="focus-input100"></span>
				</div>

			<div style="width: 100%;display: flex;"> 
			<div class="container-contact100-form-btn">
					<button class="contact100-form-btn" type="submit">
						Log In 
					</button>
				</div>
				<div class="container-contact100-form-btn">
					<button class="contact100-form-btn mr-5">
						<a class="text-light" href="cvform.php"> Create CV   </a>		
					</button>
				</div>
				</div>
				
				<div style="margin-top:30px;"> 
					<a href="ForgetPassword.php" style="color: black;">Forget Password?</a>
				</div>

			</form>
		</div>
	</div>
<?php require_once("footer.php")?>