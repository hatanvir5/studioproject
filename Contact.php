<?php
ob_start();
session_start();


$email='';
if(array_key_exists('email',$_SESSION)){
    $email=$_SESSION['email'];
       
    }
	
	$number='';
    if(array_key_exists('number',$_SESSION)){
        $number=$_SESSION['number'];
      
        }
        
    
    
    if(!empty($email) || !empty($number)){
        require 'profile_header.php';
    }else{
        require 'header.php';

	}
require 'db.php';
        
	$sql4='SELECT * 
	from contactimage LIMIT 1';
	$statement4=$connection->prepare($sql4);
	
	$statement4->execute();
	$contactimage=$statement4->fetch(PDO::FETCH_OBJ);
		 

?>
<!-- Custom css start just use only contact form -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<!-- Custom  css end just use only contact form -->

<div class="container-contact100 py-3 bg-primary">
	
 
		<div class="wrap-contact100">
			<div class="contact100-form-title" >
			<img src="images/<?=$contactimage->image;?>" alt="" style="width: 100%;height: auto;">
			</div>
			<div class="ml-auto py-4"> <h2>Contact Us </h2></div>
			<div>
<?php 
		  if(array_key_exists('contactSent',$_SESSION)){
			echo  '<p class="text-center bg-success text-white p-2 mb-2">'. $_SESSION['contactSent'].'</p><br>';
			unset($_SESSION['contactSent']);
		  }
	?>
</div>
			<div class="text-center"> <p> নিশ্চিন্তে আপনার মতামত জানান ।</p></div>

			<form class="contact100-form validate-form" method="post" action="contactProcess.php">
				<div class="wrap-input100 validate-input" data-validate="Name is required">
					<span class="label-input100">Full Name:<span class="red">*</span></span>
					<input class="input100" type="text" name="name" placeholder="Enter full name" required>
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
					<span class="label-input100">Email: <span class="red">*</span></span>
					<input class="input100" type="email" name="email" placeholder="Enter email addess" required>
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate="Phone is required">
					<span class="label-input100">Phone: <span class="red">*</span></span>
					<input class="input100" type="text" name="phone" placeholder="Enter phone number" required>
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate = "Message is required">
					<span class="label-input100">Message:</span>
					<textarea class="input100" name="message" placeholder="Your Comment..." required></textarea>
					<span class="focus-input100"></span>
				</div>

				<div class="container-contact100-form-btn">
					<button class="contact100-form-btn">
						<span>
							Submit
							<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
						</span>
					</button>
				</div>
			</form>
		</div>
	</div>


<?php require_once("footer.php")?>