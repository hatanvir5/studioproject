<!DOCTYPE html>
<html lang="en">
<head>
  <title>Fan Club List</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    
<?php
   require 'db.php';
    
    $limit = 2;
    $s = $connection->prepare("SELECT * FROM blog");
    $s->execute();
    $allResp = $s->fetchAll(PDO::FETCH_ASSOC);
    // echo '<pre>';
    // var_dump($allResp);
    $total_results = $s->rowCount();
    $total_pages = ceil($total_results/$limit);
    
    if (!isset($_GET['page'])) {
        $page = 1;
    } else{
        $page = $_GET['page'];
    }


    $start = ($page-1)*$limit;

    $stmt = $connection->prepare("SELECT * FROM blog ORDER BY id DESC LIMIT $start, $limit");
    $stmt->execute();

    // set the resulting array to associative
    $stmt->setFetchMode(PDO::FETCH_OBJ);
    
    $results = $stmt->fetchAll();
       
    $conn = null;
    
    // var_dump($results);
    


?>

<div class="container">
  <h2 class="">Fan Club List <span class="badge">Total: <?= $total_results; ?></span></h2>

  <table class="table table-bordered">
    <thead>
      <tr>
        <th>title</th>
        <th>catagory</th>
        <th>short description</th>
        <th>full description</th>
        <th>image</th>
        <th>date</th>
      </tr>
    </thead>
    <tbody>
        <?php foreach($results as $result){?>
          <tr>
            <td><?= $result->title; ?></td>
            <td><?= $result->catagory; ?></td>
            <td><?= $result->shortDescription; ?></td>
            <td><?= $result->fullDescription; ?></td>
            <td><img src="images/<?= $result->image; ?>" width="85px" height="85px"></td>
            <td><?= $result->date; ?></td>

          </tr>
        <?php } ?>
    </tbody>
  </table>
    <ul class="pagination">
        <li><a href="?page=1">First</a></li>
        
        <?php for($p=1; $p<=$total_pages; $p++){?>
            
            <li class="<?= $page == $p ? 'active' : ''; ?>"><a href="<?= '?page='.$p; ?>"><?= $p; ?></a></li>
        <?php }?>
        <li><a href="?page=<?= $total_pages; ?>">Last</a></li>
    </ul> 
</div>

</body>
</html>