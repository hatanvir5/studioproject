<?php
ob_start();
session_start();

$emailad='';
if(isset($_SESSION['emailad'])){
   $emailad=$_SESSION['emailad'];
    if($emailad!='superadmin@onlinestudiobd.com'){
        
        header('location:dashbored.php');
    }
}
if($emailad==''){
  $loginErrorShow='Login First';
  $_SESSION['loginErrorShow']=$loginErrorShow;
      header('location:index.php');

}


require_once("sitedesign_header.php"); 
require_once '../db.php';
$sql="SELECT * FROM footer limit 1";
$statement=$connection->prepare($sql);
$statement->execute();
$footer=$statement->fetch(PDO::FETCH_OBJ);


 ?>

<!-- MAIN CONTENT-->
<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
      <div class="row">
<!---------- preview Mode----------- -->
        <div class="col-md-12"> 
          <div class="row">  
            <div class="col-md-2"></div>
            <div class="col-md-8"> 
            <div class="card">
<form action="footerProcess.php"  enctype="multipart/form-data" method="post">
                   <div class="card-body">
                   <label for=""><b>Name</b></label>
                   <input name="name" type="text" value="<?=$footer->name;?>" placeholder="Shop Name" class="form-control">  
                   <h5 class="card-title">
                   <label for="">Description</label>

                   <textarea placeholder="Shop Description" name="content2" class="form-control" id="full-featured-non-premium" rows="3"><?=$footer->fullDescription;?></textarea>
                   <label for="">Phone Number</label>
                  
                   <input type="text" name="phone" value="<?=$footer->phone;?>" placeholder="Phone Number" class="form-control">  
                   <label for="">Email</label>
                   
                   <input type="email" name="email" value="<?=$footer->email;?>" placeholder="Email" class="form-control">  
                </h5>
                 </div>
                   <button class="btn btn-primary" type="submit"> Submit</button>
</form>
                </div>

            </div>
<div class="col-md-2 "></div>
            <div class="col-md-4 pt-5 bg-light"> 
           <div class="card"> 
           <div class="card-title"> 
           <h3>গুরুত্বপূর্ন লিংক সমূহ </h3>
           
           </div>
            <div class="card-body"> 
            <ul> 
              <li><a href="footerinfo.php">গোপনীয়তা </a></li>
              <li><a href="footerinfo.php">আমাদের সম্পর্কে </a> </li>
            </ul>
            </div>
           </div>
          </div>
        </div>    
        <!---row-->
      </div>
    
    </div>
  </div>
</div>
</div>
<!-- END PAGE CONTAINER-->

</div>

<!-- Jquery JS-->
<script src="vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="vendor/bootstrap-4.1/popper.min.js"></script>
<script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS ---->
<script src="vendor/slick/slick.min.js">
</script>
<script src="vendor/wow/wow.min.js"></script>
<script src="vendor/animsition/animsition.min.js"></script>
<script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
</script>
<script src="vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="vendor/counter-up/jquery.counterup.min.js">
</script>
<script src="vendor/circle-progress/circle-progress.min.js"></script>
<script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="vendor/chartjs/Chart.bundle.min.js"></script>
<script src="vendor/select2/select2.min.js">
</script>

<!-- Main JS-->
<script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
