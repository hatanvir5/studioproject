<?php
ob_start();
session_start();

require_once("header.php");

$emailad='';
if(isset($_SESSION['emailad'])){
   $emailad=$_SESSION['emailad'];
   
    if($emailad!='superadmin@onlinestudiobd.com'){
        
        header('location:dashbored.php');
    }
}
if($emailad==''){
    $loginErrorShow='Login First';
    $_SESSION['loginErrorShow']=$loginErrorShow;
    header('location:index.php');

}
require '../db.php';
if (strtoupper($_SERVER['REQUEST_METHOD'])=='POST') {
    $data=$_POST;

    $userName=$data['userName'];
    $userNumber=$data['userNumber'];
    
    $useremail=$data['userEmail'];
    $userPassword=$data['userPassword'];


    $image='';
    if(array_key_exists('image',$_FILES)){
     $image = $_FILES['image']['name'];
    }
         if ($image!='') {
             $image = $_FILES['image']['name'];
            
             $tmp_dir = $_FILES['image']['tmp_name'];
             $imgSize = $_FILES['image']['size'];
         
             $upload_dir = '../images/'; // upload directory
     $imgExt = strtolower(pathinfo($image, PATHINFO_EXTENSION)); // get image extension
     $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
     $updateImage = $image;
             if (in_array($imgExt, $valid_extensions)) {
                 if ($imgSize < 5000000) {
                   
                     move_uploaded_file($tmp_dir, $upload_dir.$updateImage);
                 } else {
                     $errMSG = "Sorry, your file is too large it should be less then 1MB";
                 }
             } else {
                 $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
             }
         } else {
             // if no image selected the old image remain as it is.
             $errMSG = "No file found";
    
         }
     




    $searchEmail="SELECT count(*) From studioregistrations where studioregistrations.email=:email";
    $statement=$connection->prepare($searchEmail);
    $statement->execute([':email'=>$useremail]);
    $rowCount=$statement->fetchColumn();
    if ($rowCount>0) {
        $regEmailExistErr= "The email address   is exist, try another email";
        $_SESSION['regEmailExistErr']=$regEmailExistErr;
        header('location:register.php');
        exit;
    }
    
    $sql="INSERT into studioregistrations(userName,email,number,password,image) values(:userName,:email,:number,md5(:password),:image)";

    $statement=$connection->prepare($sql);
    if($statement->execute([
       ':userName'=>$userName,
       ':email'=>$useremail,
       ':number'=>$userNumber,
       ':password'=>$userPassword,
       ':image'=>$image
   ])){
    echo 'registration completed';
    $regSuccess= "registration Successfull, please login";
    $_SESSION['adreg']=$regSuccess;
    header('location:adminview.php');
   };
}