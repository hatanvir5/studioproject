<?php 
ob_start();
session_start();
$emailad='';
if(array_key_exists('emailad',$_SESSION)){
    $emailad=$_SESSION['emailad'];
     
    }
    if($emailad==''){
		$loginErrorShow='Login First';
		$_SESSION['loginErrorShow']=$loginErrorShow;
        header('location:index.php');
	}
    
require '../db.php';

    $id=$_GET['id'];
 

        $sql="SELECT * from users
        join registrations on users.registrationId=registrations.id
        WHERE users.id='{$id}'";

        $statement=$connection->prepare($sql);
        $statement->execute();
		$profile=$statement->fetch(PDO::FETCH_OBJ);
        
  



?>




<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Welcome to studio website</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/aos.css" media="all" />	
  <link rel="shortcut icon" type="image/x-icon" href="img/mlogo.png">
 
  <script src="../js/jquery.min.js"></script>
	<script src="../js/bjqs-1.3.min.js"></script>	
	<link rel="stylesheet" href="../css/bjqs.css">
  <link rel="stylesheet" type="text/css" href="../css/style.css" media="all" />
 



</head>
<body>

<div class="container bg-primary text-light">
    <div class="row">
        <div class="col-lg-12 ">
            <div class="tab-content py-4">
                <div class="tab-pane active" id="profile">
                    <div class="row bg-primary">
                        <div class="col-md-12">
                          <!-- profile info start here -->
                          <div class="container"> 
                        <div class="row">  
                           <div class="col-md-2"> 
                            <a class="btn btn-danger" href="dashbored.php">Dashbored</a>       
                        </div>
                            <div class="col-md-4">         
                                 <img style="width:50%" src="../images/<?=$profile->applicantPhoto;?>" class="img-fluid mx-auto img-circle d-block" alt="avatar">
                                 
                            </div>
                            <div class="col-md-6 mt-2">
                            <h3>  Name : <strong><?=$profile->applicantName;?></strong> </h3>
                            <p class="lead">  Date of Birth : <strong> <?=$profile->birthDate;?>/<?=$profile->birthMonth;?>/<?=$profile->birthYear;?></strong> </p>
                            <p class="lead">Father Name : <strong><?=$profile->fatherName;?> </strong> </p>
                            <p class="lead">Mother Name : <strong> <?=$profile->motherName;?></strong> </p>


                            </div>
                          
                        </div>
                        </div>
                    <div class=" mt-3"> 
                    <div class="row "> 
                        <div class="col-md-2"> </div>
                      <div class="col-md-4"> 
                         <h5 class="eng"> Gender : <strong> <?=$profile->gender;?></strong> </h5>

                       </div>
                      <div class="col-md-4"> 
                      <h5 class="eng"> Religion : <strong> <?=$profile->religion;?></strong></h5>

                       </div>
                        <div class="col-md-2">  </div>
                      </div>
                    </div>

                    <div class="mt-3"> 
                    <div class="row "> 
                        <div class="col-md-2"> </div>
                      <div class="col-md-4"> 
                         <h5 class="eng"> Place of brith : <strong> <?=$profile->birthPlace;?></strong> </h5>

                       </div>
                      <div class="col-md-4"> 
                      <h5 class="eng"> Blood Group : <strong><?=$profile->bloodGroup;?></strong></h5>

                       </div>
                        <div class="col-md-2">  </div>
                      </div>
                    </div>

                    <div class="mt-3"> 
                    <div class="row "> 
                        <div class="col-md-2"> </div>
                      <div class="col-md-4"> 
                         <h5 class="eng"> National ID : <strong> <?=$profile->nid;?></strong> </h5>

                       </div>
                      <div class="col-md-4"> 
                      <h5 class="eng"> Passport ID : <strong><?=$profile->passport;?></strong></h5>

                       </div>
                        <div class="col-md-2">  </div>
                      </div>
                    </div>

                    <div class="mt-3"> 
                    <div class="row "> 
                        <div class="col-md-2"> </div>
                      <div class="col-md-4"> 
                         <h5 class="eng"> Birth Registration: <strong> <?=$profile->birthCirtificate;?></strong> </h5>

                       </div>
                      <div class="col-md-3"> 
                      <h5 class="eng"> Marital Staus : <strong><?=$profile->maritalStatus;?></strong></h5>

                       </div>
                       <div class="col-md-3"> <h5 class="eng"> Spouse Name:  <strong><?=$profile->wifeName;?></strong></h5> </div>

                      </div>
                    </div>
                    <div class="mt-3"> 
                    <div class="row "> 
                        <div class="col-md-2"> </div>
                      <div class="col-md-4"> 
                         <h5 class="eng">  Quota:<strong> <?=$profile->quota;?></strong> </h5>
                       </div>
                      <div class="col-md-4"> 
                          <h5 class="eng"> Email : <strong><?=$profile->email;?></strong></h5>
                       </div>
                        <div class="col-md-2">  </div>
                      </div>
                    </div>

                    <div class="mt-3"> 
                    <div class="row "> 
                        <div class="col-md-2"> </div>
                      <div class="col-md-4"> 
                         <h5 class="eng">Mobile Number: <strong><?=$profile->number;?></strong> </h5>
                       </div>
                      <div class="col-md-4"> 
                          <h5 class="eng">   <span> </span></h5>
                       </div>
                        <div class="col-md-2">  </div>
                      </div>
                    </div>
<!-- Address box start here  -->
                <div class="container text-light"> 
                    <div class="row "> 
                        <div class="col-md-2"> </div>
                        <div class="col-md-4 text-center"> 
                            <h2 class="lead text-light"> Mailing/Present Address</h2>
                            <h5> Care of:<strong><?=$profile->presentGurdianName;?></strong></h5>
                            <h5 class="eng">Village/Town/ Road/House/Flat: <br> <strong> <?=$profile->PresentVillage;?></strong> </h5> 
                            <h5 class="eng"> District : <strong> <?=$profile->presentDistrict;?></strong> </h5>
                            <h5 class="eng"> P.S/Upazila : <strong> <?=$profile->presentUpazila;?></strong></h5>
                            <h5 class="eng"> Post Office : <strong> <?=$profile->presenPostOffice;?></strong> </h5>
                            <h5 class="eng"> Post Code : <strong> <?=$profile->presentPostCode;?></strong> </h5>
                            
                    </div>
                    <!-- Left Side address bar -->
                        <div class="col-md-4 text-center">
                        <h2 class="lead text-light"> Parmanent Address</h2>
                        <h5> Care of:<strong> <?=$profile->parmanentGurdianName;?></strong></h5>
                            <h5 class="eng">Village/Town/Road/House/Flat: <br> <strong><?=$profile->parmanentVillage;?></strong> </h5> 
                            <h5 class="eng"> District : <strong> <?=$profile->parmanentDistrict;?></strong> </h5>
                            <h5 class="eng"> P.S/Upazila : <strong> <?=$profile->parmanentUpazila;?></strong></h5>
                            <h5 class="eng"> Post Office : <strong> <?=$profile->parmanentPostOffice;?></strong> </h5>
                            <h5 class="eng"> Post Code : <strong> <?=$profile->parmanentPostCode;?></strong> </h5>

                    </div>
                        <div class="col-md-2"> </div>
                    </div>
                </div>

<!-- jsc exam start -->
                <div class="container mt-4"> 
                    <div class="row "> 
                        <div class="col-md-12"> 
                            <h2 class="lead text-light"> JSC or Equalvalent Level </h2>
                        </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3"> <h5> Examination: <strong> <?=$profile->jscExam;?></strong></h5></div>
                        <div class="col-md-3"> <h5> Board: <strong> <?=$profile->jscBoard;?></strong></h5></div>
                        <div class="col-md-3"> <h5> Roll No: <strong> <?=$profile->jscRoll;?></strong></h5></div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3 mt-3"> <h5> Group/Subject: <strong> <?=$profile->jscSubject;?></strong></h5></div>
                        <div class="col-md-3 mt-3"> <h5> Passing Year: <strong> <?=$profile->jscYear;?></strong></h5></div>
                        <div class="col-md-4 mt-3"> <h5> Result : <strong><?=$profile->jscResult;?> <span><?=$profile->jscGPA;?></span></strong></h5></div>
                    </div>
                </div>
<!-- end jsc exam -->
<!-- ssc exam start -->
<div class="container mt-4"> 
                    <div class="row "> 
                        <div class="col-md-12"> 
                            <h2 class="lead text-light"> SSC or Equalvalent Level </h2>
                        </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3"> <h5> Examination: <strong> <?=$profile->sscExam;?></strong></h5></div>
                        <div class="col-md-3"> <h5> Board: <strong> <?=$profile->sscBoard;?></strong></h5></div>
                        <div class="col-md-3"> <h5> Roll No: <strong> <?=$profile->sscRoll;?></strong></h5></div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3 mt-3"> <h5> Group/Subject: <strong> <?=$profile->sscGroup;?></strong></h5></div>
                        <div class="col-md-3 mt-3"> <h5> Passing Year: <strong> <?=$profile->sscYear;?></strong></h5></div>
                        <div class="col-md-4 mt-3"> <h5> Result : <strong><?=$profile->sscResult;?>  <span> <?=$profile->sscGPA;?></span></strong></h5></div>
                    </div>
                </div>
<!-- end ssc exam -->
<!-- HSC exam start -->
<div class="container mt-4"> 
                    <div class="row "> 
                        <div class="col-md-12"> 
                            <h2 class="lead text-light"> HSC or Equalvalent Level </h2>
                        </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3"> <h5> Examination: <strong> <?=$profile->hscExam;?></strong></h5></div>
                        <div class="col-md-3"> <h5> Board: <strong> <?=$profile->hscBoard;?></strong></h5></div>
                        <div class="col-md-3"> <h5> Roll No: <strong> <?=$profile->hscRoll;?></strong></h5></div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3 mt-3"> <h5> Group/Subject: <strong> <?=$profile->hscSubject;?></strong></h5></div>
                        <div class="col-md-3 mt-3"> <h5> Passing Year: <strong> <?=$profile->hscYear;?></strong></h5></div>
                        <div class="col-md-4 mt-3"> <h5> Result : <strong><?=$profile->hscResult;?>  <span> <?=$profile->hscGPA;?></span></strong></h5></div>
                    </div>
                </div>
<!-- end HSC exam -->
<!-- dgree exam start -->
<div class="container mt-4"> 
                    <div class="row "> 
                        <div class="col-md-12"> 
                            <h2 class="lead text-light"> Degree/Honars or Equalvalent Level </h2>
                        </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3"> <h5> Examination: <strong> <?=$profile->degreeExam;?></strong></h5></div>
                        <div class="col-md-3"> <h5> Subject/Degree: <strong> <?=$profile->degreeSubject;?></strong></h5></div>
                        <div class="col-md-3"> <h5>Course Duration:<strong><?=$profile->degreeCourseDuration;?></strong></h5></div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3 mt-3"> <h5>University/Institute: <strong> <?=$profile->degreeUniversity;?></strong></h5></div>
                        <div class="col-md-3 mt-3"> <h5> Passing Year: <strong> <?=$profile->degreePassingYear;?></strong></h5></div>
                        <div class="col-md-4 mt-3"> <h5> Result : <strong><?=$profile->degreeResult;?> <span><?=$profile->degreeCGPA;?></span></strong></h5></div>
                    </div>
                </div>
<!-- end dgree exam -->
<!-- Master exam start -->
<div class="container mt-4"> 
                    <div class="row "> 
                        <div class="col-md-12"> 
                            <h2 class="lead text-light"> Master Equalvalent Level</h2>
                        </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3"> <h5> Examination: <strong> <?=$profile->masterExam;?></strong></h5></div>
                        <div class="col-md-3"> <h5> Subject/Degree: <strong> <?=$profile->masterSubject;?></strong></h5></div>
                        <div class="col-md-3"> <h5>Course Duration:<strong> <?=$profile->masterCourseDuration;?></strong></h5></div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3 mt-3"> <h5>University/Institute: <strong> <?=$profile->masterUniversity;?></strong></h5></div>
                        <div class="col-md-3 mt-3"> <h5> Passing Year: <strong> <?=$profile->masterPassingYear;?></strong></h5></div>
                        <div class="col-md-4 mt-3"> <h5> Result : <strong><?=$profile->masterResult;?><span> <?=$profile->masterCGPA;?></span></strong></h5></div>
                    </div>
                </div>
<!-- end Master exam -->
<!-- Professional   start -->
<div class="container mt-4"> 
                    <div class="row "> 
                        <div class="col-md-12"> 
                            <h2 class="lead text-light"> Professional Experience</h2>
                        </div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-5"> <h5> Designation/Post name: <strong> <?=$profile->postName;?></strong></h5></div>
                        <div class="col-md-5"> <h5>Organization Name<strong> <?=$profile->organizationName;?></strong></h5></div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-5"> <h5>Service Start Date:<strong><?=$profile->jobStartTime;?> </strong></h5></div>
                        <div class="col-md-5 "> <h5>Service End Date: <strong> <?=$profile->jobEndTime;?></strong></h5></div>
                        <div class="col-md-1"> </div>
                    </div>
                </div>
<!-- end Professional   -->

                <div class="container mt-3"> 
                    <div class="row"> 
                        <div class="col-md-1"> </div>
                        <div class="col-md-6"> <h5> Responsibilities : <strong> <?=$profile->jobResponsibility;?></strong></h5></div>
                        <div class="col-md-5"> <h5> Departmental Candidate Status: <strong> <?=$profile->departmentalCandidateStatus;?></strong></h5></div>
                    </div>
                </div>

                <div class="container mt-3"> 
                    <div class="row"> 
                        <div class="col-md-1"> </div>
                        <div class="col-md-3"> <h5>Weight : <strong><?=$profile->weight;?></strong></h5></div>
                        <div class="col-md-4"> <h5> Height (Feet --inc): <strong><?=$profile->height;?></strong></h5></div>
                        <div class="col-md-3"> <h5> Chest Size <strong> <?=$profile->chestSize;?></strong></h5> </div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-6 mt-3"> <h5> Computer Training institute services :<strong> <?=$profile->computerTrainingServices;?></strong></h5></div>
                        <div class="col-md-5 mt-3"> <h5> Typeing speed : <span> Bangla: <strong> <?=$profile->banglaTypingSpeed;?></strong> <span> English : <strong> <?=$profile->englishTypingSpeed;?></strong></span></span></h5></div>
                        <div class="col-md-8"></div>
                        <div class="col-md-4 mt-5">         
                                 <img style="width:50%" src="../images/<?=$profile->applicantSignature;?>" class=" mx-auto img-circle d-block" alt="avatar">
                                 
                            </div>
                    </div>
                </div>
                <div class="container mt-5"> 
                    
                </div>
         
                        </div> 
                    </div>
                    <!--/row------------------>
                </div>
              
            </div>
        </div>
    </div>
</div>
</div>
	<script src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/acor.js"></script>
  <script src="../js/bootstrap.min.js"></script>

</body>
</html>