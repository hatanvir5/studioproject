<?php

ob_start();

$emailad='';
if(array_key_exists('emailad',$_SESSION)){
    $emailad=$_SESSION['emailad'];
     
    }
    if($emailad==''){
		$loginErrorShow='Login First';
		$_SESSION['loginErrorShow']=$loginErrorShow;
        header('location:index.php');
    }
    
require_once '../db.php';

$emailad=$_SESSION['emailad'];
$sql="SELECT * from studioregistrations where email='{$emailad}'";
$statement=$connection->prepare($sql);
$statement->execute();
$admin=$statement->fetch(PDO::FETCH_OBJ);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">
    <!-- Title Page-->
    <title>Welcome to Studio Dashboard </title>
    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">
    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">
</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="dashbored.php">
                            <img class="logoimg" src="images/mainlogo.png" alt="Mainlogo" />
                        </a>
                    </div>
                    <div class="header__navbar">
                        <ul class="list-unstyled">
                            <li class="has-sub">
                                <a href="dashbored.php">
                                    <i class="fas fa-tachometer-alt"></i>Dashboard
                                    <span class="bot-line"></span>
                                </a>
                            
                            </li>
                         
                            <li>
                                <a href="totalcv.php">
                                    <i class="fas fa-trophy"></i>
                                    <span class="bot-line"></span>Total CV</a>
                            </li>
                            <li class="has-sub">
                                <a href="#">
                                    <i class="fas fa-copy"></i>
                                    <span class="bot-line"></span>Admin Control</a>
                                <ul class="header3-sub-list list-unstyled">
                                    <li>
                                        <a href="register.php">Make a New Admin</a>
                                    </li>
                                    <li>
                                        <a href="adminview.php">Admin View</a>
                                    </li>
                                   
                                </ul>
                            </li>
                            <li class="has-sub">
                                <a href="sitedesign.php">
                                    <i class="sitedesign.php"></i>
                                    <span class="bot-line"></span>Site Design</a>
                            </li>
                        </ul>
                    </div>
                    <div class="header__tool">
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="image">
                                <img src="../images/<?=$admin->image;?>" alt="Admin" />
                                </div>
                                <div class="content">
                                    <a class="js-acc-btn" href="#"><?=$admin->userName;?></a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="info clearfix">
                                        <div class="image">
                                            <a href="#">
                                            <img src="../images/<?=$admin->image;?>" alt="Admin" />
                                            </a>
                                        </div>
                                        <div class="content">
                                            <h5 class="name">
                                                <a href="#"><?=$admin->userName;?></a>
                                            </h5>
                                            <span class="email"><?=$admin->email;?></span>
                                        </div>
                                    </div>
                                    <div class="account-dropdown__footer">
                                        <a href="logout.php">
                                            <i class="zmdi zmdi-power"></i>Logout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER DESKTOP-->

        <!-- HEADER MOBILE-->
        <header class="header-mobile header-mobile-2 d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="dashbored.php">
                            <img  class="logoimg" src="images/mainlogo.png" alt="mainlogo" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="has-sub">
                            <a class="js-arrow" href="dashbored.php">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                      
                        <li>
                            <a href="totalcv.php">
                                <i class="fas fa-table"></i>Total Cv</a>
                        </li>
                        <li>
                            <a href="form.html">
                                <i class="far fa-check-square"></i>Complete Application</a>
                        </li>
                        
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>Admin Control</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                <a href="register.html">Make a New admin</a>
                                </li>
                                <li>
                                <a href="adminview.php">Admin View</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="sitedesign.html">
                                <i class="fas fa-desktop"></i>Site Design</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="sub-header-mobile-2 d-block d-lg-none">
            <div class="header__tool">
              
                <div class="account-wrap">
                    <div class="account-item account-item--style2 clearfix js-item-menu">
                        <div class="image">
                            <img src="../images/<?=$admin->image;?>" alt="Admin" />
                        </div>
                        <div class="content">
                            <a class="js-acc-btn" href="#"><?=$admin->userName;?></a>
                        </div>
                        <div class="account-dropdown js-dropdown">
                            <div class="info clearfix">
                                <div class="image">
                                    <a href="#">
                                        <img src="images/icon/avatar-01.jpg" alt="John Doe" />
                                    </a>
                                </div>
                                <div class="content">
                                    <h5 class="name">
                                        <a href="#"><?=$admin->userName;?></a>
                                    </h5>
                                    <span class="email"><?=$admin->email;?></span>
                                </div>
                            </div>
                           
                            <div class="account-dropdown__footer">
                                <a href="logout.php">
                                    <i class="zmdi zmdi-power"></i>Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END HEADER MOBILE -->