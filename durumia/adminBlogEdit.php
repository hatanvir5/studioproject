<?php
ob_start();
session_start();

$emailad='';
if(isset($_SESSION['emailad'])){
   $emailad=$_SESSION['emailad'];
    if($emailad!='superadmin@onlinestudiobd.com'){
        
        header('location:dashbored.php');
    }
}
if($emailad==''){
  $loginErrorShow='Login First';
		$_SESSION['loginErrorShow']=$loginErrorShow;
        header('location:index.php');
}


require_once("sitedesign_header.php");
require '../db.php';




$limit = 3;

$sql="SELECT * from blog";
$statement=$connection->prepare($sql);
$statement->execute();
$statement->fetchAll(PDO::FETCH_OBJ);


$total_results = $statement->rowCount();
$total_pages = ceil($total_results/$limit);

if (!isset($_GET['page'])) {
    $page = 1;
} else{
    $page = $_GET['page'];
}

   
$start = ($page-1)*$limit;

$stmt = $connection->prepare("SELECT * FROM blog ORDER BY id DESC LIMIT $start, $limit ");
$stmt->execute();

// set the resulting array to associative
$stmt->setFetchMode(PDO::FETCH_OBJ);

$chakrinews = $stmt->fetchAll();
  


?>
<script src="ckeditor/ckeditor.js"></script>
  <script src="ckeditor/samples/js/sample.js"></script>
  <link rel="stylesheet" href="ckeditor/samples/css/samples.css">

<!-- MAIN CONTENT-->
<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
      <div class="row">
<!---------- preview Mode----------- -->
        <div class="col-md-12"> 
          <div class="row"> 
            
            <div class="col-md-12"> 


    <?php if(array_key_exists('id',$_GET)):
        $id=$_GET['id'];

                   $sql="SELECT * from blog where id='{$id}' ";
                  $statement=$connection->prepare($sql);
                  $statement->execute();
                  $result=$statement->fetchAll(PDO::FETCH_OBJ);
                  foreach ($result as $singleService):?>





            <div class="card">
            <form action="adminBLogUpdate.php" method="post" enctype="multipart/form-data"> 
            <input type="file" name="image" class="form-control form-control-lg p-3">
            <img class="ml-5" src="../images/<?=$singleService->image;?>" width="100px" height="150px">
            <input type="hidden" name="oldImage" value="<?=$singleService->image;?>">
            <input type="hidden" name="id" value="<?=$singleService->id;?>">
                   <div class="card-body">
                   <h5 class="card-title">
                        <input name="catagory" value="<?=$singleService->catagory;?>" class="form-control" placeholder="job Catagory" type="text" required >
                      </h5>
                      <h5 class="card-title">
                        <input class="form-control" value="<?=$singleService->title;?>"  name="title" placeholder="Job title" type="text" required>
                      </h5>
                      <div class="form-group">
                       <label for="exampleFormControlTextarea1">Mini Discription</label>
                       <textarea name="content2" class="form-control" id="exampleFormControlTextarea1" rows="3" required><?=$singleService->shortDescription;?></textarea>
                   </div>

                   <div class="form-group">
                       <label for="exampleFormControlTextarea2">Discription</label>
                       <textarea name="content" class="form-control" id="editor" rows="3" required><?=$singleService->fullDescription;?></textarea>
             
                   </div>

                           </div>
                           <button type="submit" class="btn btn-primary"> Post</button>
                         </form>
                          
                         </div>
<?php 
endforeach;
endif;
?>
            </div>




<?php foreach($chakrinews as $chakri): ?>

            <div class="col-md-4"> 
              <div class="card">
                  <img class="card-img-top" src="../images/<?=$chakri->image;?>" height="200px" alt="Card image cap">
                     <div class="card-body">
                        <h5 class="card-title"><?=$chakri->title;?></h5>
                           <p class="card-text" ><small class="text-muted"><?=$chakri->date;?></small></p>
                           <p class="card-text"><?=$chakri->catagory;?></p>
                            <p class="card-text"></p>
  
                             </div>
                             <a class="btn btn-primary" href="adminBlogEdit.php?id=<?=$chakri->id;?>"> Edit</a>
                             <a class="btn btn-danger" href="adminBlogDelete.php?id=<?=$chakri->id;?>"> Delete</a>
                  
                           </div>
              </div>

<?php endforeach;?>


  
          </div>

        </div>
        
        
        <!---row-->
      </div>
      <div class="row">
            <div class="col-md-8"></div>
            <div class="mt-5 col-md-4">
            <ul class="pagination justify-content-end">
         <li class="page-item"><a class="page-link" href="?page=1">First</a></li>
        
        <?php for($p=1; $p<=$total_pages; $p++){?>
            
            <li class="<?= $page == $p ? 'active' : ''; ?>"><a class="page-link" href="<?= '?page='.$p; ?>"><?= $p; ?></a></li>
        <?php }?>
        <li class="page-item"><a class="page-link" href="?page=<?= $total_pages; ?>">Last</a></li>
  </ul>
</div>
          </div>


    </div>
  </div>
</div>
</div>
<!-- END PAGE CONTAINER-->

</div>

<!-- Jquery JS-->
<script src="vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="vendor/bootstrap-4.1/popper.min.js"></script>
<script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="vendor/slick/slick.min.js">
</script>
<script src="vendor/wow/wow.min.js"></script>
<script src="vendor/animsition/animsition.min.js"></script>
<script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
</script>
<script src="vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="vendor/counter-up/jquery.counterup.min.js">
</script>
<script src="vendor/circle-progress/circle-progress.min.js"></script>
<script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="vendor/chartjs/Chart.bundle.min.js"></script>
<script src="vendor/select2/select2.min.js">
</script>

<!-- Main JS-->
<script src="js/main.js"></script>
<script>
	initSample();
</script>

</body>

</html>
<!-- end document-->
