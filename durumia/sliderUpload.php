<?php
ob_start();
session_start();

$emailad='';
if(isset($_SESSION['emailad'])){
   $emailad=$_SESSION['emailad'];
    if($emailad!='superadmin@onlinestudiobd.com'){
        
        header('location:dashbored.php');
    }
}
if($emailad==''){
    $loginErrorShow='Login First';
    $_SESSION['loginErrorShow']=$loginErrorShow;
    header('location:index.php');

}








require '../db.php';

$sql='SELECT * 
from slider LIMIT 1';
$statement1=$connection->prepare($sql);

$statement1->execute();
$slider=$statement1->fetch(PDO::FETCH_OBJ);


if (strtoupper($_SERVER['REQUEST_METHOD'])=='POST') {

$firstSlider=$_FILES['imageOne'];
$firstSlider=$_FILES['imageTwo'];
$firstSlider=$_FILES['imageThree'];


    $imageOne = $_FILES['imageOne']['name'];
    $tmp_dir = $_FILES['imageOne']['tmp_name'];
    $imgSize = $_FILES['imageOne']['size'];

    if ($imageOne) {
        $upload_dir = '../images/'; // upload directory
$imgExt = strtolower(pathinfo($imageOne, PATHINFO_EXTENSION)); // get image extension
$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
$firstSlider = $imageOne;
        if (in_array($imgExt, $valid_extensions)) {
            if ($imgSize < 1000000) {
                unlink($upload_dir.$slider->imageOne);
                move_uploaded_file($tmp_dir, $upload_dir.$firstSlider);
            } else {
                $errMSG = "Sorry, your file is too large it should be less then 5MB";
            }
        } else {
            $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        }
    } else {
        // if no image selected the old image remain as it is.
$firstSlider = $slider->imageOne; // old image from database
    }


    //2nd slider


    $imageTwo = $_FILES['imageTwo']['name'];
    $tmp_dir = $_FILES['imageTwo']['tmp_name'];
    $imgSize = $_FILES['imageTwo']['size'];

    if ($imageTwo) {
        $upload_dir = '../images/'; // upload directory
$imgExt = strtolower(pathinfo($imageTwo, PATHINFO_EXTENSION)); // get image extension
$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
$secondSlider = $imageTwo;
        if (in_array($imgExt, $valid_extensions)) {
            if ($imgSize < 1000000) {
                unlink($upload_dir.$slider->imageTwo);
                move_uploaded_file($tmp_dir, $upload_dir.$secondSlider);
            } else {
                $errMSG = "Sorry, your file is too large it should be less then 5MB";
            }
        } else {
            $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        }
    } else {
        // if no image selected the old image remain as it is.
$secondSlider = $slider->imageTwo; // old image from database
    }




    //3rd slider


    $imageThree = $_FILES['imageThree']['name'];
    $tmp_dir = $_FILES['imageThree']['tmp_name'];
    $imgSize = $_FILES['imageThree']['size'];

    if ($imageThree) {
        $upload_dir = '../images/'; // upload directory
$imgExt = strtolower(pathinfo($imageThree, PATHINFO_EXTENSION)); // get image extension
$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
$thidSlider =$imageThree;
        if (in_array($imgExt, $valid_extensions)) {
            if ($imgSize < 1000000) {
                unlink($upload_dir.$slider->imageThree);
                move_uploaded_file($tmp_dir, $upload_dir.$thidSlider);
            } else {
                $errMSG = "Sorry, your file is too large it should be less then 5MB";
            }
        } else {
            $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        }
    } else {
        // if no image selected the old image remain as it is.
$thidSlider = $slider->imageThree; // old image from database
    }




    $sql='UPDATE slider set
    imageOne=:imageOne,
    imageTwo=:imageTwo,
    imageThree=:imageThree';

    $statement=$connection->prepare($sql);
    if($statement->execute([
    ':imageOne'=>$firstSlider,
    ':imageTwo'=>$secondSlider,
    ':imageThree'=>$thidSlider
    ])){
        echo 'data updated successfully';
    }else{
        echo 'image check image size and extension';
    }
     


    $sql1='SELECT * 
    from slider LIMIT 1';
    $statement2=$connection->prepare($sql1);
    
    $statement2->execute();
    $slider=$statement2->fetch(PDO::FETCH_OBJ);
    



}


?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>Slider Change</title>
  </head>
  <body class="container bg-info">
<h3 class="text-center bg-black p-2">Slider Update</h3>
<form method="post" action=""  enctype="multipart/form-data"> 
  <input type="hidden" name="size" value="1000000">
 

    <div class="form-group">
    <label>1st Slider Image:</label><br>
    <img src="../images/<?=$slider->imageOne;?>" height="300px" width="500px"><br>
    <input class="form-control-file" type="file"  name="imageOne"><br>
    </div>
    <div class="form-group">
    <label>2nd Slider Image::</label><br>
    <img src="../images/<?=$slider->imageTwo;?>"  height="300px" width="500px"><br>
    <input class="form-control-file" type="file"   name="imageTwo"  ><br>
    </div>

    <div class="form-group">
    <label>3rd Slider Image::</label><br>
    <img src="../images/<?=$slider->imageThree;?>"     height="300px" width="500px"><br>

    <input class="form-control-file" type="file" name="imageThree"><br>
    </div>

      <button class="btn btn-success mb-5" type="submit">Update</button>
      <a href="dashbored.php" class="btn btn-success mb-5" >Go to dashboard</a>

</form>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  </body>
</html>