<?php
ob_start();
session_start();

$emailad='';
if(isset($_SESSION['emailad'])){
   $emailad=$_SESSION['emailad'];
    if($emailad!='superadmin@onlinestudiobd.com'){
        
        header('location:dashbored.php');
    }
}
if($emailad==''){
  $loginErrorShow='Login First';
  $_SESSION['loginErrorShow']=$loginErrorShow;
      header('location:index.php');

}


require_once("sitedesign_header.php"); 
require_once("../db.php"); 

$sql="SELECT * FROM videopage limit 1";
$statement=$connection->prepare($sql);
$statement->execute();
$video=$statement->fetch(PDO::FETCH_OBJ);

$sql="SELECT * FROM contact";
$statement=$connection->prepare($sql);
$statement->execute();
$contacts=$statement->fetchAll(PDO::FETCH_OBJ);




 ?>

<!-- MAIN CONTENT-->
<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
      <div class="row">
<!---------- preview Mode----------- -->
        <div class="col-md-12"> 
          <div class="row">  

          <div>
<?php 
		  if(array_key_exists('updateVideo',$_SESSION)){
			echo  '<p class="text-center bg-success text-white p-2 mb-2">'. $_SESSION['updateVideo'].'</p><br>';
			unset($_SESSION['updateVideo']);
      }
      if(array_key_exists('contactDelete',$_SESSION)){
        echo  '<p class="text-center bg-success text-white p-2 mb-2">'. $_SESSION['contactDelete'].'</p><br>';
        unset($_SESSION['contactDelete']);
        }
      
	?>
</div>
            <div class="col-md-2"></div>
            <div class="col-md-8"> 
            <div class="card">
<form action="videoUpdate.php"  enctype="multipart/form-data" method="post">

            <input type="text" value="<?=$video->title;?>" name="title" placeholder="Video Title" class="form-control">
             
                   <div class="card-body">
                   <h5 class="card-title">
                   <textarea placeholder="Video Description" name="content2" class="form-control" id="full-featured-non-premium" rows="3"><?=$video->fullDescription;?></textarea>
                </h5>
                      <h5 class="card-title">
                        <input class="form-control" value="<?=$video->url;?>" name="url"  placeholder="Video Link" type="text">
                      </h5>
                    
                           </div>
                           <button class="btn btn-primary" type="submit"> Submit</button>
</form>
                         </div>

            </div>
<div class="col-md-2 "></div>
            <div class="col-md-12 pt-5 bg-light"> 
            <h2 class="text-center">Messages </h2>
            <table class="table table-bordered">
  <thead>
    <tr>
     
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Phone</th>
      <th scope="col">Messages</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
<?php foreach($contacts as $contact): ?>
    <tr>
      
      <td><?=$contact->name;?></td>
      <td><?=$contact->email;?></td>
      <td><?=$contact->phone;?></td>
      <td><?=$contact->fullDescription;?></td>
      <td > <a class="btn btn-warning" href="deleteContact.php?id=<?=$contact->id;?>">Delete</a> </td>
    </tr>

<?php endforeach;?>
  </tbody>
</table>
          </div>

        </div>
        
        
        <!---row-->
      </div>
    
    </div>
  </div>
</div>
</div>
<!-- END PAGE CONTAINER-->

</div>

<!-- Jquery JS-->
<script src="vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="vendor/bootstrap-4.1/popper.min.js"></script>
<script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS ---->
<script src="vendor/slick/slick.min.js">
</script>
<script src="vendor/wow/wow.min.js"></script>
<script src="vendor/animsition/animsition.min.js"></script>
<script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
</script>
<script src="vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="vendor/counter-up/jquery.counterup.min.js">
</script>
<script src="vendor/circle-progress/circle-progress.min.js"></script>
<script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="vendor/chartjs/Chart.bundle.min.js"></script>
<script src="vendor/select2/select2.min.js">
</script>

<!-- Main JS-->
<script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
