<?php
ob_start();
session_start();

$emailad='';
if(isset($_SESSION['emailad'])){
   $emailad=$_SESSION['emailad'];
    if($emailad!='superadmin@onlinestudiobd.com'){
        
        header('location:dashbored.php');
    }
}
if($emailad==''){
  $loginErrorShow='Login First';
  $_SESSION['loginErrorShow']=$loginErrorShow;
      header('location:index.php');

}




require_once "blogHeader.php";
require '../db.php';



$limit = 6;

$sql="SELECT * from servicedetails";
$statement=$connection->prepare($sql);
$statement->execute();
$statement->fetchAll(PDO::FETCH_OBJ);


$total_results = $statement->rowCount();
$total_pages = ceil($total_results/$limit);

if (!isset($_GET['page'])) {
    $page = 1;
} else{
    $page = $_GET['page'];
}


$start = ($page-1)*$limit;

$stmt = $connection->prepare("SELECT * FROM servicedetails ORDER BY serviceDetailsId DESC LIMIT $start, $limit ");
$stmt->execute();

// set the resulting array to associative
$stmt->setFetchMode(PDO::FETCH_OBJ);

$blog = $stmt->fetchAll();
  

?> 
<!-- Editor Css Js -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>

      <!-- MAIN CONTENT-->
      <div class="main-content">
        <div class="section__content section__content--p30">
          <div class="container-fluid">
            <div class="row">
 <!---------- preview Mode----------- -->
              <div class="col-md-12">
                <div class="row">
                  
                  <div class="col-md-12">
                  <div class="card">


<?php

if (isset($_GET['id'])):
    $id = $_GET['id'];

    $sql = "SELECT * from servicedetails where serviceDetailsId='{$id}' ";
    $statement = $connection->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_OBJ);

    foreach ($result as $singleBlog): ?>

	      <form action="serviceDetailsUpdate.php"  enctype="multipart/form-data" method="post">

	            <input type="file" name="image" class="form-control form-control-lg p-3">
	            <img class="ml-5" src="../images/<?=$singleBlog->serviceImage;?>" width="100px" height="150px">
	            <input type="hidden" name="oldImage" value="<?=$singleBlog->serviceImage;?>">
	            <input type="hidden" name="id" value="<?=$singleBlog->serviceDetailsId;?>">
		                         <div class="card-body">

		                            <h5 class="card-title">
		                             <label for="">Title</label>

		                              <input class="form-control" name="title" value="<?=$singleBlog->title;?>" placeholder="Blog title" type="text">
		                            </h5>
                                
		                            <h5 class="card-title">
		                             <label for="">Catagory</label>

		                              <input name="catagory" class="form-control" value="<?=$singleBlog->catagory;?>" placeholder="Catagory" type="text">
		                            </h5>
		                            <div class="form-group">
		                             <label for="exampleFormControlTextarea1">Mini Discription</label>
		                             <textarea name="content2" class="form-control" id="summernote" rows="3"><?=$singleBlog->shortDescription;?></textarea>
		                         </div>

		                         <div class="form-group">
		                             <label for="exampleFormControlTextarea2">Discription</label>
		                             <textarea name="content" class="form-control" id="summernote2" rows="3"><?=$singleBlog->fullDescription;?></textarea>

		                         </div>

		                                 </div>
		                                 <button class="btn btn-primary" type="submit"> Post</button>
	                                   </form>




	<?php

endforeach;
endif;

?>



                               </div>

                  </div>



  <?php foreach ($blog as $singleBlog): ?>

                  <div class="col-md-4">
                    <div class="card">
                        <img  src="../images/<?=$singleBlog->serviceImage;?>" height="50" alt="blog image cap">
                           <div class="card-body">
                              <h5 class="card-title"><?=$singleBlog->title;?></h5>
                                 <p class="card-text"><small class="text-muted"><?=$singleBlog->date;?></small></p>
                                   </div>

                                   <a class="btn btn-primary" href="service_Post_Edit.php?id=<?=$singleBlog->serviceDetailsId?>"> Edit</a>

                                 </div>
                    </div>

  <?php endforeach;?>

                </div>

              </div>


              <!---row-->
            </div>
            <div class="row">
            <div class="col-md-8"></div>
            <div class="mt-5 col-md-4">
            <ul class="pagination justify-content-end">
         <li class="page-item"><a class="page-link" href="?page=1">First</a></li>
        
        <?php for($p=1; $p<=$total_pages; $p++){?>
            
            <li class="<?= $page == $p ? 'active' : ''; ?>"><a class="page-link" href="<?= '?page='.$p; ?>"><?= $p; ?></a></li>
        <?php }?>
        <li class="page-item"><a class="page-link" href="?page=<?= $total_pages; ?>">Last</a></li>
  </ul></div>
          </div>
            </div>
        </div>
      </div>
    </div>
    <!-- END PAGE CONTAINER-->

  </div>

  <!-- Jquery JS-->
 
  <!-- Bootstrap JS-->
  <script src="vendor/bootstrap-4.1/popper.min.js"></script>
  <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
  <!-- Vendor JS       -->
  <script src="vendor/slick/slick.min.js">
  </script>
  <script src="vendor/wow/wow.min.js"></script>
  <script src="vendor/animsition/animsition.min.js"></script>
  <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
  </script>
  <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
  <script src="vendor/counter-up/jquery.counterup.min.js">
  </script>
  <script src="vendor/circle-progress/circle-progress.min.js"></script>
  <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
  <script src="vendor/chartjs/Chart.bundle.min.js"></script>
  <script src="vendor/select2/select2.min.js">
  </script>

  <!-- Main JS-->
  <script src="js/main.js"></script>
  <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
</body>

</html>
<!-- end document-->
