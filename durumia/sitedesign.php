<?php
ob_start();
session_start();

$emailad='';
if(isset($_SESSION['emailad'])){
   $emailad=$_SESSION['emailad'];
    if($emailad!='superadmin@onlinestudiobd.com'){
        
        header('location:dashbored.php');
    }
}
if($emailad==''){
	$loginErrorShow='Login First';
	$_SESSION['loginErrorShow']=$loginErrorShow;
	header('location:index.php');

}




 require_once("sitedesign_header.php");

require '../db.php';

$sql4='SELECT * 
from contactimage LIMIT 1';
$statement4=$connection->prepare($sql4);

$statement4->execute();
$contactimage=$statement4->fetch(PDO::FETCH_OBJ);



$sql4='SELECT * 
from notice LIMIT 1';
$statement4=$connection->prepare($sql4);

$statement4->execute();
$notice=$statement4->fetch(PDO::FETCH_OBJ);


$sql='SELECT * 
from slider LIMIT 1';
$statement1=$connection->prepare($sql);

$statement1->execute();
$slider=$statement1->fetch(PDO::FETCH_OBJ);


if (strtoupper($_SERVER['REQUEST_METHOD'])=='POST') {
$data=$_POST;
$oldImageOne=$data['oldImageOne'];
$oldImageTwo=$data['oldImageTwo'];
$oldImageThree=$data['oldImageThree'];


$imageOne='';
if(array_key_exists('imageOne',$_FILES)){
    $imageOne = $_FILES['imageOne']['name'];

}

    if ($imageOne!='') {
		$tmp_dir = $_FILES['imageOne']['tmp_name'];
		$imgSize = $_FILES['imageOne']['size'];
        $upload_dir = '../images/'; // upload directory
$imgExt = strtolower(pathinfo($imageOne, PATHINFO_EXTENSION)); // get image extension
$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
$firstSlider = rand(1000, 1000000).".".$imgExt;
        if (in_array($imgExt, $valid_extensions)) {
            if ($imgSize < 1000000) {
                unlink($upload_dir.$oldImageOne);
                move_uploaded_file($tmp_dir, $upload_dir.$firstSlider);
            } else {
                $errMSG = "Sorry, your file is too large it should be less then 5MB";
            }
        } else {
            $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        }
    } else {
        // if no image selected the old image remain as it is.
$firstSlider = $oldImageOne; // old image from database
    }


	//2nd slider

	$imageTwo='';
	if(array_key_exists('imageTwo',$_FILES)){
		$imageTwo = $_FILES['imageTwo']['name'];
	
	}
	
		if ($imageTwo!='') {
			$tmp_dir = $_FILES['imageTwo']['tmp_name'];
			$imgSize = $_FILES['imageTwo']['size'];
			$upload_dir = '../images/'; // upload directory
	$imgExt = strtolower(pathinfo($imageTwo, PATHINFO_EXTENSION)); // get image extension
	$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
	$secondSlider = rand(1000, 1000000).".".$imgExt;
			if (in_array($imgExt, $valid_extensions)) {
				if ($imgSize < 1000000) {
					unlink($upload_dir.$oldImageTwo);
					move_uploaded_file($tmp_dir, $upload_dir.$secondSlider);
				} else {
					$errMSG = "Sorry, your file is too large it should be less then 5MB";
				}
			} else {
				$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			}
		} else {
			// if no image selected the old image remain as it is.
	$secondSlider = $oldImageTwo; // old image from database
		}




    //3rd slider


	$imageThree='';
	if(array_key_exists('imageThree',$_FILES)){
		$imageThree = $_FILES['imageThree']['name'];
	
	}
	
		if ($imageThree!='') {
			$tmp_dir = $_FILES['imageThree']['tmp_name'];
			$imgSize = $_FILES['imageThree']['size'];
			$upload_dir = '../images/'; // upload directory
	$imgExt = strtolower(pathinfo($imageThree, PATHINFO_EXTENSION)); // get image extension
	$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
	$thirdSlider = rand(1000, 1000000).".".$imgExt;
			if (in_array($imgExt, $valid_extensions)) {
				if ($imgSize < 1000000) {
					unlink($upload_dir.$oldImageThree);
					move_uploaded_file($tmp_dir, $upload_dir.$thirdSlider);
				} else {
					$errMSG = "Sorry, your file is too large it should be less then 5MB";
				}
			} else {
				$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			}
		} else {
			// if no image selected the old image remain as it is.
	$thirdSlider = $oldImageThree; // old image from database
		}






    $sql='UPDATE slider set
    imageOne=:imageOne,
    imageTwo=:imageTwo,
    imageThree=:imageThree';

    $statement=$connection->prepare($sql);
    if($statement->execute([
    ':imageOne'=>$firstSlider,
    ':imageTwo'=>$secondSlider,
    ':imageThree'=>$thirdSlider
    ])){
        echo 'data updated successfully';
    }else{
        echo 'image check image size and extension';
    }
     


    $sql1='SELECT * 
    from slider LIMIT 1';
    $statement2=$connection->prepare($sql1);
    
    $statement2->execute();
    $slider=$statement2->fetch(PDO::FETCH_OBJ);
    



}







?>

<!-- MAIN CONTENT-->
<div class="main-content">
  <div class="section__content section__content--p30">
	<div class="container-fluid">
	  <div class="row">
		<div class="col-md-12"> 
		<form method="post" action="sitedesign.php"  enctype="multipart/form-data"> 
		  <div class="row"> 
		  <div class="col-md-4"> 
		  <img src="../images/<?=$slider->imageOne;?>" height="200px" width="300px"><br>

			<div class="custom-file">
			  <input type="file" class="custom-file-input"  name="imageOne" id="customFile">
			  <input type="hidden" class="custom-file-input" value="<?=$slider->imageOne;?>" name="oldImageOne" id="customFile">
			  <label class="custom-file-label" for="customFile">Choose Image-1</label>
			</div>
		  </div>
		  <div class="col-md-4">
		  <img src="../images/<?=$slider->imageTwo;?>" height="200px" width="300px"><br>

			<div class="custom-file">
			  <input type="file" class="custom-file-input"  name="imageTwo" id="customFile">
			  <input type="hidden" class="custom-file-input" value="<?=$slider->imageTwo;?>" name="oldImageTwo" id="customFile">

			  <label class="custom-file-label" for="customFile">Choose Image-2</label>
			</div>
		  </div>
		  <div class="col-md-4">
		  <img src="../images/<?=$slider->imageThree;?>" height="200px" width="300px"><br>

			<div class="custom-file">
			  <input type="file" class="custom-file-input"  name="imageThree" id="customFile">
			  <input type="hidden" class="custom-file-input" value="<?=$slider->imageThree;?>" name="oldImageThree" id="customFile">

			  <label class="custom-file-label" for="customFile">Choose Image-3</label>
			</div>
		  </div>
		</div>
		<div class="col-md-12 my-5 text-center"> 
		  <button type="submit" class="btn btn-lg btn-success">Upload</button>
		</div>
</form>
		</div>
		<!---------Notice Update-->
		<div class="container">
		  <div class="row">
		  <div class="alert alert-danger alert-dismissible" role="alert">
		
		  <strong>
			<i class="fa fa-warning"></i> নোটিশ !
		  </strong>
		   <marquee behavior=scroll direction="left" scrollamount="3"><p class="ban" style="font-size: 18px"><?=$notice->notice;?></p></marquee>
		</div>
		  </div>
		</div>
	<div class="col-md-12"> 
		<form action="noticeUpdate.php" method="post">

	
			<textarea name="notice" id="" cols="80" class="form-control form-control-lg" rows="1"><?=$notice->notice;?></textarea>
		  <!-- <input name="notice" placeholder="Insert Notice" value="<?=$notice->notice;?>" class="form-control form-control-lg" type="text"> -->
		</div>
		<input type="hidden" name="id"value="<?=$notice->id;?>">
		<div class="col-md-3 text-center"> 
		  <button type="submit" class="btn btn-lg btn-success">Update</button>
		  </form>
		</div>
<!-- Input Contact Image Here  -->
<div class="col-md-4 pt-5">

		  <img src="../images/<?=$contactimage->image;?>" height="200px" width="300px"><br>

			<div class="custom-file">
				<form action="contactimage.php"  enctype="multipart/form-data" method="post">
			  <input type="file" class="custom-file-input"  name="image" id="contactimg">
			  <input type="hidden" class="custom-file-input" value="<?=$contactimage->id;?>" name="id" id="customFile">
			  <input type="hidden" class="custom-file-input" value="<?=$contactimage->image;?>" name="oldImage" id="customFile">

			  <label class="custom-file-label" for="customFile">Choose Contact Image</label>
			</div>
		  
		  <button type="submit" class="btn btn-lg btn-success">Update</button>
		  </form>
	  </div>
	  <!---row-->
	  </div>
	</div>
  </div>
</div>
</div>
<!-- END PAGE CONTAINER-->

</div>

<!-- Jquery JS-->
<script src="vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="vendor/bootstrap-4.1/popper.min.js"></script>
<script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="vendor/slick/slick.min.js">
</script>
<script src="vendor/wow/wow.min.js"></script>
<script src="vendor/animsition/animsition.min.js"></script>
<script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
</script>
<script src="vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="vendor/counter-up/jquery.counterup.min.js">
</script>
<script src="vendor/circle-progress/circle-progress.min.js"></script>
<script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="vendor/chartjs/Chart.bundle.min.js"></script>
<script src="vendor/select2/select2.min.js">
</script>

<!-- Main JS-->
<script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
