<?php
ob_start();
session_start();

$emailad='';
if(isset($_SESSION['emailad'])){
   $emailad=$_SESSION['emailad'];
    if($emailad!='superadmin@onlinestudiobd.com'){
        
        header('location:dashbored.php');
    }
}
if($emailad==''){
  $loginErrorShow='Login First';
  $_SESSION['loginErrorShow']=$loginErrorShow;
      header('location:index.php');

}
require_once "sitedesign_header.php";
require '../db.php';


$limit = 6;

$sql="SELECT * from blog";
$statement=$connection->prepare($sql);
$statement->execute();
$statement->fetchAll(PDO::FETCH_OBJ);


$total_results = $statement->rowCount();
$total_pages = ceil($total_results/$limit);

if (!isset($_GET['page'])) {
    $page = 1;
} else{
    $page = $_GET['page'];
}


$start = ($page-1)*$limit;

$stmt = $connection->prepare("SELECT * FROM blog ORDER BY id DESC LIMIT $start, $limit ");
$stmt->execute();

// set the resulting array to associative
$stmt->setFetchMode(PDO::FETCH_OBJ);

$blog = $stmt->fetchAll();
  


?> 
<!-- Editor Css Js -->
<script src="../js/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
<script src="ckeditor/ckeditor.js"></script>
  <script src="ckeditor/samples/js/sample.js"></script>
  <link rel="stylesheet" href="ckeditor/samples/css/samples.css">


  


      <!-- MAIN CONTENT-->
      <div class="main-content">
        <div class="section__content section__content--p30">
          <div class="container-fluid">
            <div class="row">
 <!---------- preview Mode----------- -->
              <div class="col-md-12">
                <div class="row">
                
                  <div class="col-md-12">
                  <div class="card">




<form action="adminBlogInsert.php"  enctype="multipart/form-data" method="post">

	            <input type="file" name="image" class="form-control form-control-lg p-3">
	         
	            
		                         <div class="card-body">

		                            <h5 class="card-title">
		                             <label for="">Title</label>

		                              <input class="form-control" name="title"  placeholder="Blog title" type="text">
		                            </h5>
                                
		                            <h5 class="card-title">
		                             <label for="">Catagory</label>

		                              <input name="catagory" class="form-control"  placeholder="Catagory" type="text">
		                            </h5>
		                            <div class="form-group">
		                             <label for="exampleFormControlTextarea1">Mini Discription</label>
		                             <textarea name="content2" class="form-control" id="full-featured-non-premium" rows="3"></textarea>
		                         </div>

		                         <div class="form-group">
		                             <label for="exampleFormControlTextarea2">Discription</label>
                                 <textarea id="editor" name="content" class="form-control" rows="3"> 

                                 </textarea>
                                 
                               
                               
                                
                              
    
		                         </div>

                                     </div>
                                    
		                                 <button class="btn btn-primary" type="submit"> Post</button>
                                     </form>
                                     







                               </div>

                  </div>



  <?php foreach ($blog as $singleBlog): ?>

                  <div class="col-md-4">
                    <div class="card">
                        <img class="bg_img"  src="../images/<?=$singleBlog->image;?>" height="50" alt="blog image cap">
                           <div class="card-body">
                              <h5 class="card-title"><?=$singleBlog->title;?></h5>
                                 <p class="card-text"><small class="text-muted"><?=$singleBlog->date;?></small></p>
                                   </div>

                                   <a class="btn btn-primary" href="adminBlogEdit.php?id=<?=$singleBlog->id?>"> Edit</a>

                                   <a class="btn btn-danger"href="adminBlogDelete.php?id=<?=$singleBlog->id?>"> Delete</a>
                                 </div>
                    </div>

  <?php endforeach;?>

                </div>

              </div>


              <!---row-->
            </div>
            <div class="row">
            <div class="col-md-8"></div>
            <div class="mt-5 col-md-4">
            <ul class="pagination justify-content-end">
         <li class="page-item"><a class="page-link" href="?page=1">First</a></li>
        
        <?php for($p=1; $p<=$total_pages; $p++){?>
            
            <li class="<?= $page == $p ? 'active' : ''; ?>"><a class="page-link" href="<?= '?page='.$p; ?>"><?= $p; ?></a></li>
        <?php }?>
        <li class="page-item"><a class="page-link" href="?page=<?= $total_pages; ?>">Last</a></li>
  </ul>
</div>
          </div>
            </div>
        </div>
      </div>
    </div>
    <!-- END PAGE CONTAINER-->

  </div>

  <!-- Jquery JS-->
 
  <!-- Bootstrap JS-->
  <script src="vendor/bootstrap-4.1/popper.min.js"></script>
  <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
  <!-- Vendor JS       -->
  <script src="vendor/slick/slick.min.js">
  </script>
  <script src="vendor/wow/wow.min.js"></script>
  <script src="vendor/animsition/animsition.min.js"></script>
  <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
  </script>
  <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
  <script src="vendor/counter-up/jquery.counterup.min.js">
  </script>
  <script src="vendor/circle-progress/circle-progress.min.js"></script>
  <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
  <script src="vendor/chartjs/Chart.bundle.min.js"></script>
  <script src="vendor/select2/select2.min.js">
  </script>

  <!-- Main JS-->
  <script src="js/main.js"></script>

  <script>
	initSample();
</script>

</body>

</html>
<!-- end document-->
