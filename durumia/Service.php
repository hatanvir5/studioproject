<?php
ob_start();
session_start();

$emailad='';
if(isset($_SESSION['emailad'])){
   $emailad=$_SESSION['emailad'];
    if($emailad!='superadmin@onlinestudiobd.com'){
        
        header('location:dashbored.php');
    }
}
if($emailad==''){
  $loginErrorShow='Login First';
  $_SESSION['loginErrorShow']=$loginErrorShow;
      header('location:index.php');

}
require_once "sitedesign_header.php";
require '../db.php';



$limit = 6;

$sql="SELECT * from servicedesign ORDER BY serviceDesignId DESC";
$statement=$connection->prepare($sql);
$statement->execute();
$statement->fetchAll(PDO::FETCH_OBJ);


$total_results = $statement->rowCount();
$total_pages = ceil($total_results/$limit);

if (!isset($_GET['page'])) {
    $page = 1;
} else{
    $page = $_GET['page'];
}


$start = ($page-1)*$limit;

$stmt = $connection->prepare("SELECT * FROM servicedesign ORDER BY serviceDesignId DESC LIMIT $start, $limit ");
$stmt->execute();

// set the resulting array to associative
$stmt->setFetchMode(PDO::FETCH_OBJ);

$blog = $stmt->fetchAll();
  


?> 
<script src="../js/jquery-3.5.1.min.js" crossorigin="anonymous"></script>



      <!-- MAIN CONTENT-->
      <div class="main-content">
        <div class="section__content section__content--p30">
          <div class="container-fluid">
            <div class="row">
 <!---------- preview Mode----------- -->
              <div class="col-md-12">
                <div class="row">
                
                  <div class="col-md-12">
                  <div class="card">




<form action="serviceDesginInsert.php"  enctype="multipart/form-data" method="post">

	            <input type="file" name="image" class="form-control form-control-lg p-3">
	         
	            
		                         <div class="card-body">

		                            <h5 class="card-title">
		                             <label for="">Title</label>

		                              <input class="form-control" name="title"  placeholder="Service  title" type="text">
		                            </h5>
                                
		                            <h5 class="card-title">
		                             <label for="">url(remove white space)</label>

		                              <input name="catagory" class="form-control"  placeholder="Link Url" type="text">
		                            </h5>
		                

		                                 </div>
		                                 <button class="btn btn-primary" type="submit"> Post</button>
	                                   </form>








                               </div>

                  </div>



  <?php foreach ($blog as $singleBlog): ?>

                  <div class="col-md-4">
                    <a href="<?=$singleBlog->url;?>" target="_blank">
                    <div class="card">
                        <img class="bg_img"  src="../images/<?=$singleBlog->image;?>" height="50" alt="blog image cap">
                           <div class="card-body">
                              <h5 class="card-title"><?=$singleBlog->serviceTitle;?></h5>
                                 
                                   </div>

                                   <a class="btn btn-primary" href="serviceDesignEdit.php?id=<?=$singleBlog->serviceDesignId?>"> Edit</a>

                                   <a class="btn btn-danger"href="serviceDesignDelete.php?id=<?=$singleBlog->serviceDesignId?>"> Delete</a>
                                 </div>
                                 </a>
                    </div>

  <?php endforeach;?>

                </div>

              </div>


              <!---row-->
            </div>
            <div class="row">
            <div class="col-md-8"></div>
            <div class="mt-5 col-md-4">
            <ul class="pagination justify-content-end">
         <li class="page-item"><a class="page-link" href="?page=1">First</a></li>
        
        <?php for($p=1; $p<=$total_pages; $p++){?>
            
            <li class="<?= $page == $p ? 'active' : ''; ?>"><a class="page-link" href="<?= '?page='.$p; ?>"><?= $p; ?></a></li>
        <?php }?>
        <li class="page-item"><a class="page-link" href="?page=<?= $total_pages; ?>">Last</a></li>
  </ul>
</div>
          </div>
            </div>
        </div>
      </div>
    </div>
    <!-- END PAGE CONTAINER-->

  </div>

  <!-- Jquery JS-->
 
  <!-- Bootstrap JS-->
  <script src="vendor/bootstrap-4.1/popper.min.js"></script>
  <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
  <!-- Vendor JS       -->
  <script src="vendor/slick/slick.min.js">
  </script>
  <script src="vendor/wow/wow.min.js"></script>
  <script src="vendor/animsition/animsition.min.js"></script>
  <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
  </script>
  <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
  <script src="vendor/counter-up/jquery.counterup.min.js">
  </script>
  <script src="vendor/circle-progress/circle-progress.min.js"></script>
  <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
  <script src="vendor/chartjs/Chart.bundle.min.js"></script>
  <script src="vendor/select2/select2.min.js">
  </script>

  <!-- Main JS-->
  <script src="js/main.js"></script>
  <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
</body>

</html>
<!-- end document-->
