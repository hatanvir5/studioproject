<?php 
ob_start();
session_start();
$emailad='';
if(array_key_exists('emailad',$_SESSION)){
    $emailad=$_SESSION['emailad'];
     
    }
    if($emailad==''){
		$loginErrorShow='Login First';
		$_SESSION['loginErrorShow']=$loginErrorShow;
        header('location:index.php');
	}
require_once("header.php");
require '../db.php';




$limit = 5;

$sql="SELECT * from jobapplicaton where status=3";
$statement=$connection->prepare($sql);
$statement->execute();
$applications=$statement->fetchAll(PDO::FETCH_OBJ);


$total_results = $statement->rowCount();
$total_pages = ceil($total_results/$limit);

if (!isset($_GET['page'])) {
    $page = 1;
} else{
    $page = $_GET['page'];
}


$start = ($page-1)*$limit;

$stmt = $connection->prepare("SELECT * FROM jobapplicaton where status=3 ORDER BY id DESC LIMIT $start, $limit ");
$stmt->execute();

// set the resulting array to associative
$stmt->setFetchMode(PDO::FETCH_OBJ);

$application = $stmt->fetchAll();
  

?>
            <!-- DATA TABLE-->
            <section class="p-t-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6"> </div>
                        <div class="col-md-6 pt-5">
                           
                            </div>
                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35">Processing Applicant </h3>
                          
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2">
                                    <thead class="text-center">
                                        <tr>
                                        
                                           
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Company</th>
                                            <th>Post</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <?php foreach($application as $completeApp):?>
                                        <tr class="tr-shadow">
                                        
                                            
                                            <td><?=$completeApp->name;?></td>
                                            <td>
                                                <span class="block-email"><?=$completeApp->email;?></span>
                                            </td>
                                            <td class="desc"><?=$completeApp->number;?></td>
                                            <td><?=$completeApp->organizationName;?></td>
                                            <td>
                                                <span class="status--process"><?=$completeApp->positionName;?></span>
                                            </td>
                                            <td><?=$completeApp->date;?></td>
                                           
                                            <td>
                                                <div  class="table-data-feature">
                                                   <a class="btn btn-info" href="userprofile.php?id=<?=$completeApp->userId;?>"> Preview</a>
                                                   <a class="btn btn-success mx-1" href="confirmAppProcess.php?id=<?=$completeApp->id;?>"> Complete</a>
                                                   <a class="btn btn-danger " href="deleteProcess.php?id=<?=$completeApp->id;?>"> Delete</a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>

                                    
                                    </tbody>
                                </table>


                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END DATA TABLE-->
            <nav class="py-3 mr-3" aria-label="Page navigation example">
            <ul class="pagination justify-content-end">
  <li class="page-item"><a class="page-link" href="?page=1">First</a></li>
        
        <?php for($p=1; $p<=$total_pages; $p++){?>
            
            <li class="<?= $page == $p ? 'active' : ''; ?>"><a class="page-link" href="<?= '?page='.$p; ?>"><?= $p; ?></a></li>
        <?php }?>
        <li class="page-item"><a class="page-link" href="?page=<?= $total_pages; ?>">Last</a></li>
  </ul>
</nav>

      
    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
