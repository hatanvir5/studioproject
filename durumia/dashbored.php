<?php 
ob_start();
session_start();
$emailad='';
if(array_key_exists('emailad',$_SESSION)){
    $emailad=$_SESSION['emailad'];
     
    }
    if($emailad==''){
		$loginErrorShow='Login First';
		$_SESSION['loginErrorShow']=$loginErrorShow;
        header('location:index.php');
	}

require_once("header.php");
require '../db.php';

$sql="SELECT * from studioregistrations where email='{$emailad}'";
$statement=$connection->prepare($sql);
$statement->execute();
$admin=$statement->fetch(PDO::FETCH_OBJ);



$sql1="SELECT count(*) from users";

$statement1=$connection->prepare($sql1);
$statement1->execute();
$noOfApplication=$statement1->fetchColumn();





$limit = 5;

$sql="SELECT * from jobapplicaton where status=0";
$statement=$connection->prepare($sql);
$statement->execute();
$applications=$statement->fetchAll(PDO::FETCH_OBJ);


$total_results = $statement->rowCount();
$total_pages = ceil($total_results/$limit);

if (!isset($_GET['page'])) {
    $page = 1;
} else{
    $page = $_GET['page'];
}


$start = ($page-1)*$limit;

$stmt = $connection->prepare("SELECT * FROM jobapplicaton where status=0 ORDER BY id DESC LIMIT $start, $limit ");
$stmt->execute();

// set the resulting array to associative
$stmt->setFetchMode(PDO::FETCH_OBJ);

$application = $stmt->fetchAll();
   




    $sql3="SELECT count(*) from jobapplicaton 
    where status=1";
    $statement3=$connection->prepare($sql3);
    $statement3->execute();
    $completeApplication=$statement3->fetchColumn();
    
    $sql4="SELECT count(*) from jobapplicaton 
    where status=0";
    $statement4=$connection->prepare($sql4);
    $statement4->execute();
    $pendingApplication=$statement4->fetchColumn();
    
    $sql5="SELECT count(*) from jobapplicaton 
    where status=3";
    $statement5=$connection->prepare($sql5);
    $statement5->execute();
    $processingApplication=$statement5->fetchColumn();
    


?>

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
            <!-- BREADCRUMB-->
            <section class="au-breadcrumb2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <span class="au-breadcrumb-span">You are here:</span>
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item active">
                                            <a href="#">Home</a>
                                        </li>
                                        <li class="list-inline-item seprate">
                                            <span>/</span>
                                        </li>
                                        <li class="list-inline-item">Dashboard</li>
                                    </ul>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END BREADCRUMB-->

            <!-- WELCOME-->
            <section class="welcome p-t-10">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title-4">Welcome back
                                <span><?=$admin->userName;?></span>
                            </h1>
                            <hr class="line-seprate">
                        </div>
                    </div>
                </div>
            </section>
            <!-- END WELCOME-->

            <!-- STATISTIC-->
            <section class="statistic statistic2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--green">
                                <h2 class="number"><?=$noOfApplication;?></h2>
                                <span class="desc">Total Members</span>
                                <div class="icon">
                                    <i class="zmdi zmdi-account-o"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--orange">
                                <h2 class="number"><?=$completeApplication;?></h2>
                                <span class="desc">Complete Application</span>
                                <div class="icon">
                                    <i class="zmdi zmdi-shopping-cart"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--blue">
                                <h2 class="number"><?=$pendingApplication;?></h2>
                                <span class="desc">Pending Application</span>
                                <div class="icon">
                                    <i class="zmdi zmdi-calendar-note"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--orange">
                                <h2 class="number"><?=$processingApplication;?></h2>
                                <span class="desc">Processing Application</span>
                                <div class="icon">
                                    <i class="zmdi zmdi-shopping-cart"></i>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <!-- END STATISTIC-->



            <!-- DATA TABLE-->
            <section class="p-t-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-3 text-right">

                        <a class="btn btn-warning" href="process.php"> Show processing List</a>

                        </div>
                        <div class="col-md-3 text-right"> 

                            <a class="btn btn-success" href="complet.php"> Show Complete List</a>
                        </div>

                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35">Total Application </h3>
                          
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2">
                                    <thead class="text-center">
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Date</th>
                                            <th>Company Name</th>
                                            <th>Post Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>


                                    <?php foreach($application as $singleApplication):?>
                                        <tr class="tr-shadow">
                                            <td><?=$singleApplication->name;?></td>
                                            <td>
                                                <span class="block-email"><?=$singleApplication->email;?></span>
                                            </td>
                                            <td class="desc"><?=$singleApplication->number;?></td>
                                            <td><?=$singleApplication->date;?></td>
                                            <td>
                                                <span class="status--process"><?=$singleApplication->organizationName;?></span>
                                            </td>
                                            <td><?=$singleApplication->positionName;?></td>
                                            <td>
                                                <div class="table-data-feature">
                                                    <a class="btn btn-info" href="userprofile.php?id=<?=$singleApplication->userId?>"> Preview</a>
                                                   <a class="btn btn-success mx-1" href="processingApplication.php?id=<?=$singleApplication->id?>"> Pending</a>
                                                   <a class="btn btn-danger" href="rejected.php?id=<?=$singleApplication->id?>"> Reject</a>
                                                </div>
                                            </td>
                                        </tr>

                                    <?php endforeach;?>


                           
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END DATA TABLE-->

            <nav class="py-3 mr-3" aria-label="Page navigation example">
  <ul class="pagination justify-content-end">
  <li class="page-item"><a class="page-link" href="?page=1">First</a></li>
        
        <?php for($p=1; $p<=$total_pages; $p++){?>
            
            <li class="<?= $page == $p ? 'active' : ''; ?>"><a class="page-link" href="<?= '?page='.$p; ?>"><?= $p; ?></a></li>
        <?php }?>
        <li class="page-item"><a class="page-link" href="?page=<?= $total_pages; ?>">Last</a></li>
  </ul>
</nav>
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
