<?php 
ob_start();
session_start();


require_once '../db.php';
$success = "";
$error_message = "";

if (strtoupper($_SERVER['REQUEST_METHOD'])=='POST') {
    $email=$_POST['email'];
    $sql="SELECT * FROM studioregistrations WHERE email='{$email}' ";
    $statement=$connection->prepare($sql);
    $statement->execute();
    $count = $statement->fetchColumn();

    if ($count>0) {
        // generate OTP
        $otp = rand(100000, 999999);
        // Send OTP
        require_once("mail_function.php");
        $mail_status = sendOTP($email, $otp);

        if ($mail_status == 1) {
            $sql="INSERT INTO otp_expiry(otp,is_expired) VALUES (:otp,0)";
            $statement=$connection->prepare($sql);
            $result=$statement->execute([':otp'=>$otp]);

            $current_id = $connection->lastInsertId();
            if (!empty($current_id)){
                $success=1;
                $_SESSION['otpemail']=$email;
                header('location:submitOtpPass.php');
            }
        }
    } else {
        $error_message = "Email not exists!";
        $_SESSION['error_message']=$error_message;
        header('location:forget-pass.php');


    }
}