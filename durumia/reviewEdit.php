<?php
ob_start();
session_start();

$emailad='';
if(isset($_SESSION['emailad'])){
   $emailad=$_SESSION['emailad'];
    if($emailad!='superadmin@onlinestudiobd.com'){
        
        header('location:dashbored.php');
    }
}
if($emailad==''){
  $loginErrorShow='Login First';
  $_SESSION['loginErrorShow']=$loginErrorShow;
      header('location:index.php');

}








require '../db.php';
require_once("sitedesign_header.php");


$limit = 3;

$sql="SELECT * from review";
$statement=$connection->prepare($sql);
$statement->execute();
$statement->fetchAll(PDO::FETCH_OBJ);


$total_results = $statement->rowCount();
$total_pages = ceil($total_results/$limit);

if (!isset($_GET['page'])) {
    $page = 1;
} else{
    $page = $_GET['page'];
}


$start = ($page-1)*$limit;

$stmt = $connection->prepare("SELECT * FROM review ORDER BY id DESC LIMIT $start, $limit ");
$stmt->execute();

// set the resulting array to associative
$stmt->setFetchMode(PDO::FETCH_OBJ);

$reviews = $stmt->fetchAll();
  

 
 
 
 ?>

<!-- MAIN CONTENT-->
<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
      <div class="row">
<!---------- preview Mode----------- -->
        <div class="col-md-12"> 
          <div class="row">  
            <div class="col-md-2"></div>
            <div class="col-md-8"> 
            <div class="card">
            <?php

if (isset($_GET['id'])):
    $id = $_GET['id'];

    $sql = "SELECT * from review where id='{$id}' ";
    $statement = $connection->prepare($sql);
    $statement->execute();
    $result = $statement->fetch(PDO::FETCH_OBJ);

  ?>
<form action="reviewUpdate.php"  enctype="multipart/form-data" method="post">

            <input type="file" name="image" placeholder="Customer image" class="form-control form-control-lg p-3">
            <img class="ml-5" src="../images/<?=$result->image;?>" width="100px" height="150px">
	            <input type="hidden" name="oldImage" value="<?=$result->image;?>">
	            <input type="hidden" name="id" value="<?=$result->id;?>">
                   <div class="card-body">
                   <h5 class="card-title">
                  <input class="form-control" name="name" value="<?=$result->name;?>" placeholder="Customer Name" type="text">
                </h5>
                      <h5 class="card-title">
                        <input class="form-control" value="<?=$result->designation;?>" name="designation"  placeholder="Occupation" type="text">
                      </h5>
                      <div class="form-group">
                       <label for="exampleFormControlTextarea1">Comments</label>
                       <textarea name="content2"  class="form-control" id="exampleFormControlTextarea1" rows="3"><?=$result->fullDescription;?></textarea>
                   </div>

                           </div>
                           <button class="btn btn-primary" type="submit"> Submit</button>
</form>
                         </div>
<?php    endif; ?>
            </div>
<div class="col-md-2"></div>
            <div class="col-md-12"> 
            <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Images</th>
      <th scope="col">Name</th>
      <th scope="col">Designation</th>
      <th scope="col">Comments</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($reviews as $review): ?>
    <tr>
      <th scope="row"> <img style="width:56px;" src="../images/<?=$review->image;?>" alt=""></th>
      <td><?=$review->name;?></td>
      <td><?=$review->designation;?></td>
      <td><?=$review->fullDescription;?></td>
      <td> 
        <a class="btn btn-primary ml-1" href="reviewEdit.php?id=<?=$review->id;?>"> Edit</a>
        <a class="btn btn-danger ml-1" href="#"> Delete</a>
    </td>
    </tr>
    <?php 
    endforeach;
 
    ?>
   
  </tbody>
</table>
         
          </div>

        </div>
        
        
        <!---row-->
      </div>
      <div class="row">
            <div class="col-md-8"></div>
            <div class="mt-5 col-md-4">
            <ul class="pagination">
         <li class="page-item"><a class="page-link" href="?page=1">First</a></li>
        
        <?php for($p=1; $p<=$total_pages; $p++){?>
            
            <li class="<?= $page == $p ? 'active' : ''; ?>"><a class="page-link" href="<?= '?page='.$p; ?>"><?= $p; ?></a></li>
        <?php }?>
        <li class="page-item"><a class="page-link" href="?page=<?= $total_pages; ?>">Last</a></li>
  </ul></div>
          </div>
    </div>
  </div>
</div>
</div>
<!-- END PAGE CONTAINER-->

</div>

<!-- Jquery JS-->
<script src="vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="vendor/bootstrap-4.1/popper.min.js"></script>
<script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS ---->
<script src="vendor/slick/slick.min.js">
</script>
<script src="vendor/wow/wow.min.js"></script>
<script src="vendor/animsition/animsition.min.js"></script>
<script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
</script>
<script src="vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="vendor/counter-up/jquery.counterup.min.js">
</script>
<script src="vendor/circle-progress/circle-progress.min.js"></script>
<script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="vendor/chartjs/Chart.bundle.min.js"></script>
<script src="vendor/select2/select2.min.js">
</script>

<!-- Main JS-->
<script src="js/main.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script> 
CKEDITOR.replace('content');
CKEDITOR.replace('content2');
</script>
</body>

</html>
<!-- end document-->
