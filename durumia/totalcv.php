<?php
ob_start();
session_start();

$emailad='';
if(isset($_SESSION['emailad'])){
   $emailad=$_SESSION['emailad'];
    if($emailad!='superadmin@onlinestudiobd.com'){
        
        header('location:dashbored.php');
    }
}
if($emailad==''){
    $loginErrorShow='Login First';
    $_SESSION['loginErrorShow']=$loginErrorShow;
    header('location:index.php');
}





require '../db.php';
require_once("header.php");



$limit = 5;

$sql="SELECT registrations.applicantPhoto,registrations.applicantName,users.number,users.email,users.date,users.id from users
join registrations on users.registrationId=registrations.id";
$statement=$connection->prepare($sql);
$statement->execute();
$applications=$statement->fetchAll(PDO::FETCH_OBJ);


$total_results = $statement->rowCount();
$total_pages = ceil($total_results/$limit);

if (!isset($_GET['page'])) {
    $page = 1;
} else{
    $page = $_GET['page'];
}


$start = ($page-1)*$limit;

$stmt = $connection->prepare("SELECT registrations.applicantPhoto,registrations.applicantName,users.number,users.email,users.date,users.id from users
join registrations on users.registrationId=registrations.id 
ORDER BY id DESC LIMIT $start, $limit ");
$stmt->execute();

// set the resulting array to associative
$stmt->setFetchMode(PDO::FETCH_OBJ);

$application = $stmt->fetchAll();
  


?>
            <!-- DATA TABLE-->
            <section class="p-t-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6"> </div>
                        <div class="col-md-6">
                             <form class="au-form-icon--sm" action="searchcv.php" method="post">
                                    <input class="au-input--w300 au-input--style2" type="text" name="searchQ" placeholder="Search for datas &amp; reports...">
                                    <button class="btn btn-primary" type="submit"> Search


                                    </button>
                                </form> 
                         
   
                            </div>


                            <div class="m-5 b-5">
                                    <?php
                                     if(array_key_exists('searchResult',$_SESSION)){
                                
                                        $result=$_SESSION['searchResult']; ?>
                                        <div class="table-responsive table-responsive-data2">
                                        <table class="table table-data2">
                                            <thead class="text-center">
                                                <tr>
                                                
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Date</th>
                                                    
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
        
                                            <?php foreach($result as $singleApp):?>
                                                <tr class="tr-shadow">
                                                
                                                    <td> <img style="width:56px;" src="../images/<?=$singleApp->applicantPhoto?>" alt=""></td>
                                                    <td><?=$singleApp->applicantName?></td>
                                                    <td>
                                                        <span class="block-email"><?=$singleApp->email?></span>
                                                    </td>
                                                    <td class="desc"><?=$singleApp->number?></td>
                                                    <td><?=$singleApp->date?></td>
                                                  
                                                   
                                                    <td>
                                                        <div class="table-data-feature">
                                                           <a class="btn btn-info" href="userprofile.php?id=<?=$singleApp->id;?>"> Preview</a>
                                                           <a class="btn btn-danger ml-1" href="deleteCv.php?id=<?=$singleApp->id;?>"> Banned $ Delete CV</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach;?>
                                                
                                            </tbody>
                                        </table>
        
        
                                    </div>

                                    <?php
                                        unset($_SESSION['searchResult']);
                         
                                      }
                                    
                                    
                                    ?>
                                </div>








                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35">Total Applicant Members </h3>
                            <div>
                
                            <?php
                  if (array_key_exists('deleteMsg', $_SESSION)) {
                      echo  '<p class="text-center bg-success text-white p-2 mb-2">'. $_SESSION['deleteMsg'].'</p><br>';
                      unset($_SESSION['deleteMsg']);
                  }
                    
                            ?>

                            </div>
                          
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2">
                                    <thead class="text-center">
                                        <tr>
                                        
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Date</th>
                                            
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <?php foreach($application as $singleApp):?>
                                        <tr class="tr-shadow">
                                        
                                            <td> <img style="width:56px;" src="../images/<?=$singleApp->applicantPhoto?>" alt=""></td>
                                            <td><?=$singleApp->applicantName?></td>
                                            <td>
                                                <span class="block-email"><?=$singleApp->email?></span>
                                            </td>
                                            <td class="desc"><?=$singleApp->number?></td>
                                            <td><?=$singleApp->date?></td>
                                          
                                           
                                            <td>
                                                <div class="table-data-feature">
                                                   <a class="btn btn-info" href="userprofile.php?id=<?=$singleApp->id;?>"> Preview</a>
                                                   <a class="btn btn-danger ml-1" href="deleteCv.php?id=<?=$singleApp->id;?>"> Banned $ Delete CV</a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                        
                                    </tbody>
                                </table>


                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END DATA TABLE-->
            <nav class="py-3 mr-3" aria-label="Page navigation example">
    <ul class="pagination justify-content-end">
         <li class="page-item"><a class="page-link" href="?page=1">First</a></li>
        
        <?php for($p=1; $p<=$total_pages; $p++){?>
            
            <li class="<?= $page == $p ? 'active' : ''; ?>"><a class="page-link" href="<?= '?page='.$p; ?>"><?= $p; ?></a></li>
        <?php }?>
        <li class="page-item"><a class="page-link" href="?page=<?= $total_pages; ?>">Last</a></li>
  </ul>
</nav>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.search-box input[type="text"]').on("keyup input", function(){
        /* Get input value on change */
        var inputVal = $(this).val();
        var resultDropdown = $(this).siblings(".result");
        if(inputVal.length){
            $.get("backend-search.php", {term: inputVal}).done(function(data){
                // Display the returned data in browser
                resultDropdown.html(data);
            });
        } else{
            resultDropdown.empty();
        }
    });
    
    // Set search input value on click of result item
    $(document).on("click", ".result p", function(){
        $(this).parents(".search-box").find('input[type="text"]').val($(this).text());
        $(this).parent(".result").empty();
    });
});
</script>
      
    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
