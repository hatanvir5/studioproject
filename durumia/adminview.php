<?php
ob_start();
session_start();
require_once('../db.php');
require_once("header.php");

$emailad='';
if(isset($_SESSION['emailad'])){
   $emailad=$_SESSION['emailad'];
   
    if($emailad!='superadmin@onlinestudiobd.com'){
        
        header('location:dashbored.php');
    }
}
if($emailad==''){
    $loginErrorShow='Login First';
    $_SESSION['loginErrorShow']=$loginErrorShow;
    header('location:index.php');

}




$limit = 6;

$sql="SELECT * from studioregistrations where email !='superadmin@onlinestudiobd.com'";
$statement=$connection->prepare($sql);
$statement->execute();
$statement->fetchAll(PDO::FETCH_OBJ);


$total_results = $statement->rowCount();
$total_pages = ceil($total_results/$limit);

if (!isset($_GET['page'])) {
    $page = 1;
} else{
    $page = $_GET['page'];
}


$start = ($page-1)*$limit;

$stmt = $connection->prepare("SELECT * FROM studioregistrations where email !='superadmin@onlinestudiobd.com' ORDER BY id DESC LIMIT $start, $limit ");
$stmt->execute();

// set the resulting array to associative
$stmt->setFetchMode(PDO::FETCH_OBJ);

$result = $stmt->fetchAll();
  

?>




        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
          <!-- USER DATA-->
          <div class="user-data m-b-30">
                                    <h3 class="title-3 m-b-30">
                                        <i class="zmdi zmdi-account-calendar"></i>user data</h3>
                                    <div>
                                        <?php
                                        
     

                                        if(array_key_exists('banned',$_SESSION)){
                                            echo  '<p class="text-center bg-danger text-white p-2 mb-2">'. $_SESSION['banned'].'</p><br>';
                                            unset($_SESSION['banned']);
                                          }
                                          if(array_key_exists('adreg',$_SESSION)){
                                            echo  '<p class="text-center bg-success text-white p-2 mb-2">'. $_SESSION['adreg'].'</p><br>';
                                            unset($_SESSION['adreg']);
                                          }

                                        ?>
                                    </div>
                                    <div class="table-responsive table-data">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                 
                                                    <td>name</td>
                                                    
                                                    <td> Action</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($result as $ad): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data__info">
                                                            <h6><?=$ad->userName;?></h6>
                                                            <span>
                                                                <a href="#"><?=$ad->email;?></a>
                                                            </span>
                                                        </div>
                                                    </td>
                                                
                                                  <td> 
                                                      <a class="btn btn-danger" href="adDelete.php?id=<?=$ad->id;?>">Banned Admin </a>
                                                  </td>
                                                
                                                </tr> <?php endforeach; ?>
                                                
                                                
                                                
                                                   
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div>
                                <!-- END USER DATA-->




            <nav class="py-3 mr-3" aria-label="Page navigation example">
            <ul class="pagination justify-content-end">
         <li class="page-item"><a class="page-link" href="?page=1">First</a></li>
        
        <?php for($p=1; $p<=$total_pages; $p++){?>
            
            <li class="<?= $page == $p ? 'active' : ''; ?>"><a class="page-link" href="<?= '?page='.$p; ?>"><?= $p; ?></a></li>
        <?php }?>
        <li class="page-item"><a class="page-link" href="?page=<?= $total_pages; ?>">Last</a></li>
  </ul>
</nav>
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
