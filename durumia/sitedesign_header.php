<?php
ob_start();

$emailad='';
if(array_key_exists('emailad',$_SESSION)){
    $emailad=$_SESSION['emailad'];
     
    }
    if($emailad==''){
		$loginErrorShow='Login First';
		$_SESSION['loginErrorShow']=$loginErrorShow;
        header('location:index.php');
	}
require_once '../db.php';
$emailad=$_SESSION['emailad'];
$sql="SELECT * from studioregistrations where email='{$emailad}'";
$statement=$connection->prepare($sql);
$statement->execute();
$admin=$statement->fetch(PDO::FETCH_OBJ);

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags-->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="au theme template">
  <meta name="author" content="Hau Nguyen">
  <meta name="keywords" content="au theme template">
  <!-- Title Page-->
  <title>Typography</title>
  <!-- Fontfaces CSS-->
  <link href="css/font-face.css" rel="stylesheet" media="all">
  <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
  <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
  <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
  <!-- Bootstrap CSS-->
  <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">
  <!-- Vendor CSS-->
  <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
  <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
  <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
  <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
  <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
  <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
  <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

  <!-- Main CSS-->
  <link href="css/theme.css" rel="stylesheet" media="all">
  <script src="https://cdn.tiny.cloud/1/si2g4rc6wcoraownfkvbg2jow2uytk2sxxm9sts6sg84tn2n/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
</head>
<body class="animsition">
  <div class="page-wrapper">
    <!-- HEADER MOBILE-->
    <header class="header-mobile d-block d-lg-none">
      <div class="header-mobile__bar">
        <div class="container-fluid">
          <div class="header-mobile-inner">
            <a class="logo" href="dashbored.php">
              <img class="logoimg" src="images/mainlogo.png" alt="mainlogo" />
            </a>
            <button class="hamburger hamburger--slider" type="button">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
          </div>
        </div>
      </div>
      <nav class="navbar-mobile">
        <div class="container-fluid">
          <ul class="navbar-mobile__list list-unstyled">
            <li class="has-sub">
              <a href="dashbored.php">Dashboard</a>
            </li>
            <li>
              <a href="sitedesign.php">Slider & Notice</a>
            </li>
            <li>
              <a href="admin_blog.php">Blog Post</a>
            </li>
            <li>
              <a href="jobpost.php">Job Post</a>
            </li>
            <li>
            <a href="Service.php">Our Service</a>
            </li>
            <li>
            <a href="review.php">Customer Review</a>
            </li>
            <li>
              <a href="videodescription.php">Video & Messages</a>
            </li>
            <li>
              <a href="footer.php">Footer</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <!-- END HEADER MOBILE-->

    <!-- MENU SIDEBAR-->
    <aside class="menu-sidebar d-none d-lg-block">
      <div class="logo">
        <a href="dashbored.php">
        <img class="logoimg" src="images/mainlogo.png" alt="mainlogo" />
        </a>
      </div>
      <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
          <ul class="list-unstyled navbar__list">
            <li class="has-sub">
              <a  href="dashbored.php">Dashboard</a>
            </li>
            <li class="">
              <a href="sitedesign.php"> Slider & Notice</a>
            </li>
            <li>
              <a href="admin_blog.php">Blog Post</a>
            </li>
            <li>
              <a href="jobpost.php">Job Post</a>
            </li>
            <li>
              <a href="Service.php">Our Service</a>
            </li>
            <li>
              <a href="Service2.php">Why You Get Our Service</a>
            </li>
            <li>
              <a  href="review.php">Customer Review</a>
            </li>
            <li>
              <a href="videodescription.php">Video & Messages</a>
            </li>
            <li>
              <a href="footer.php">Footer</a>
            </li>
          </ul>
        </nav>
      </div>
    </aside>
    <!-- END MENU SIDEBAR-->

    <!-- PAGE CONTAINER-->
    <div class="page-container">
      <!-- HEADER DESKTOP-->
      <header class="header-desktop">
        <div class="section__content section__content--p30">
          <div class="container-fluid">
            <div class="header-wrap">
              <form class="form-header" action="" method="POST">
               
              </form>
              <div class="header-button">
                <div class="noti-wrap">
                 
               
                </div>
                <div class="account-wrap">
                  <div class="account-item clearfix js-item-menu">
                    <div class="image">
                    <img src="../images/<?=$admin->image;?>" alt="John Doe" />
                    </div>
                    <div class="content">
                      <a class="js-acc-btn" href="#"><?=$admin->userName;?></a>
                    </div>
                    <div class="account-dropdown js-dropdown">
                      <div class="info clearfix">
                        <div class="image">
                          <a href="#">
                            <img src="../images/<?=$admin->image;?>" alt="John Doe" />
                          </a>
                        </div>
                        <div class="content">
                          <h5 class="name">
                            <a href="#"><?=$admin->userName;?></a>
                          </h5>
                          <span class="email"><?=$admin->email;?></span>
                        </div>
                      </div>
                      <div class="account-dropdown__footer">
                        <a href="logout.php">
                          <i class="zmdi zmdi-power"></i>Logout</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <!-- END HEADER DESKTOP-->