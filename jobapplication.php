<?php
ob_start();
session_start();

require 'db.php';

$email='';
if(array_key_exists('email',$_SESSION)){
    $email=$_SESSION['email'];
       
    }
	
	$number='';
    if(array_key_exists('number',$_SESSION)){
        $number=$_SESSION['number'];
      
        }
        
        
        if( $email=='' && $number==''){
            		$loginErrorShow='Login First';
		$_SESSION['loginErrorShow']=$loginErrorShow;
		header('location:userlogin.php');
        // require 'header.php';
      }
    
    
    if(!empty($email) || !empty($number)){
        require 'profile_header.php';
    }
        
        
        
        
    
        $sql="SELECT registrations.applicantName,users.email,users.number,users.id from users
        join registrations on users.registrationId=registrations.id
         WHERE users.email=:email or users.number=:number";
    
        $statement=$connection->prepare($sql);
        $statement->execute([
            ':email'=>$email,
            ':number'=>$number
        ]);
		$profile=$statement->fetch(PDO::FETCH_OBJ);
		


?>








<!-- Custom css start just use only contact form -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<!-- Custom  css end just use only contact form -->

<div class="container-contact100 py-3 bg-primary">
	
 
		<div class="wrap-contact100 my-5">
			<div class="job100-form-title" style="padding: 50px;">
			<h2>Wellcome my Online Studio BD</h2>
			<div class="text-center mt-4"> <h4>আবেদনের র্নির্দেশিকা</h4></div>
		  <div class="text-danger mt-4">
		  <p>১. চাকুরীর সারকুলার ভালো ভাবে পড়ুন যে আপনার যোগ্যতা, বয়স, আপনি কোন জেলার প্রার্থী আবেদন করতে পারবে কি না ।</p>
		  <p>২. সিভি তে আপনার মোবাইল নং ও ই-মেইল আবেদন ফরমে দিন।</p>
		  <p>৩.আবেদন ফরম পূরণ করার সঙ্গে সঙ্গে বিকাশ/রকেট/শিওরক্যাশ এর মাধ্যমে আবেদন ফি জমা দিন। </p>
		  <p>৪. চাকুরি নির্দিষ্ট পদের আবেদন ফি লিখুন এবং আবেদন করার সাথে সাথে আমাদের সার্ভিস চার্জ ৫০ টাকা যোগ হয়ে যাবে। </p>
		  </div>
			</div>		
			<form class="contact100-form validate-form" action="jobApplicatonStore.php" method="POST">
				<div class="wrap-input100 validate-input" data-validate="Name is required">
					<span class="label-input100">নাম:<span class="red">*</span> </span>
					<input class="input100" type="text" value="<?=$profile->applicantName;?>" name="name" placeholder="আপনার নাম দিন" required>
					<span class="focus-input100"></span>
				</div>
				<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
					<span class="label-input100">ই-মেইল: <span class="red">*</span></span>
					<input class="input100" type="text" value="<?=$profile->email;?>" name="email" placeholder="আপনার ইমেইল দিন" required>
					<span class="focus-input100"></span>
				</div>
				<div class="wrap-input100 validate-input" data-validate="Phone is required">
					<span class="label-input100">মোবাইল নং: <span class="red">*</span></span>
					<input class="input100" type="text" name="number" value="<?=$profile->number;?>" placeholder="আপনার মোবাইল নং দিন" required>
					<span class="focus-input100"></span>
				</div>
				<div class="wrap-input100 validate-input" data-validate="instution is required">
					<span class="label-input100">প্রতিষ্ঠানের নাম:<span class="red">*</span></span>
					<input class="input100" type="text" name="organizationName" placeholder="" required>
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate="Phone is required">
					<span class="label-input100">পদের নাম:<span class="red">*</span></span>
					<input class="input100" type="text" name="positionName" placeholder="" required>
					<span class="focus-input100"></span>
				</div>
				<div class="wrap-input100 validate-input" data-validate="Phone is required">
					<span class="label-input100">Amount<span class="red">*</span> </span>
					
					<input class="input100" type="text" name="amount" placeholder="" required>
					<span class="focus-input100"></span>
				
				</div>
				<span style="color:red">বি:দ্র:- আবেদন ফি সাথে সার্ভিস চার্জ ৫০ টাকা যোগ করা হবে</span>
				<input type="hidden" name="userId" value="<?=$profile->id;?>">

				<div class="container-contact100-form-btn">
					<button type="submit" class="contact100-form-btn">
						<span>
							Submit
							<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
						</span>
					</button>
				</div>
			</form>
		</div>
	</div>


<?php require_once("footer.php")?>