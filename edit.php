<?php
ob_start();
session_start();

require 'db.php';
if(array_key_exists('email',$_SESSION)){
    $email=$_SESSION['email'];
       
    }
    
    if(array_key_exists('number',$_SESSION)){
        $number=$_SESSION['number'];
      
        }

        if($email=='' && $number==''){
          $loginErrorShow='Login First';
          $_SESSION['loginErrorShow']=$loginErrorShow;
          header('location:userlogin.php');
  
      }

          
  if(!empty($email) || !empty($number)){
      require 'profile_header.php';
  }
    
    
        $sql="SELECT * from users
        join registrations on users.registrationId=registrations.id
         WHERE users.email=:email or users.number=:number";
    
        $statement=$connection->prepare($sql);
        $statement->execute([
            ':email'=>$email,
            ':number'=>$number
        ]);
        $profile=$statement->fetch(PDO::FETCH_OBJ);
    

?>

<div class="container p-5 bg-primary"> 




   <form enctype="multipart/form-data" method="POST" action="updateProcess.php" enctype="multipart/form-data">
     <div class="row pb-3">  
      <div class="col-md-3"> 
        <h5 class="eng"> Applicant Name :  <span class="red" >* </span> </h5>
      </div>
     <input type="hidden" name="registerId" value="<?=$profile->id;?>">
      <div class="col-md-8"> 
        <input class="form-control-lg  form-control" value="<?=$profile->applicantName;?>" name="applicantName" placeholder="প্রার্থীর নাম" type="text" required >
      </div>
      </div>
<div class="row pb-3">    
      <div class="col-md-3"> 
        <h5 class="eng"> Father Name :  <span class="red" >* </span></h5>
      </div>
      <div class="col-md-8"> 
        <input class="form-control-lg form-control" value="<?=$profile->fatherName;?>" name="fatherName" placeholder="পিতার নাম" type="text" required>
      </div>
 </div>

 <div class="row pb-3">   
      <div class="col-md-3"> 
        <h5 class="eng"> Mother Name :<span class="red" >* </span> </h5>
      </div>
      <div class="col-md-8 pb-3"> 
        <input class="form-control-lg form-control" name="motherName" value="<?=$profile->motherName;?>" placeholder="মাতার নাম" type="text" required >
      </div>
 </div>
 
 <div class="row pb-3">   
      <div class="col-md-3"> 
        <h5 class="eng"> Date of Birth :<span class="red" >* </span> </h5>
      </div>

      <div class="col-md-2 pb-3"> 

     
            <select name="birthDate" class="form-control" id="b_day">
              <option value="" selected="selected">দিন</option>
                <option value="01" 
                
                <?php if($profile->birthDate=='01'){
                    echo 'selected';
                    }?>

                >01</option>
                <option value="02"                 
                <?php if($profile->birthDate=='02'){
                    echo 'selected';
                    }?>
>02</option>
                    <option value="03"                 
                <?php if($profile->birthDate=='03'){
                    echo 'selected';
                    }?>
>03</option>
                    <option value="04"                 
                <?php if($profile->birthDate=='04'){
                    echo 'selected';
                    }?>
>04</option>
                    <option value="05"                 
                <?php if($profile->birthDate=='05'){
                    echo 'selected';
                    }?>
>05</option>
                    <option value="06"                 
                <?php if($profile->birthDate=='06'){
                    echo 'selected';
                    }?>
>06</option>
                    <option value="07"                 
                <?php if($profile->birthDate=='07'){
                    echo 'selected';
                    }?>
>07</option>
                    <option value="08"                 
                <?php if($profile->birthDate=='08'){
                    echo 'selected';
                    }?>
>08</option>
                    <option value="09"                 
                <?php if($profile->birthDate=='09'){
                    echo 'selected';
                    }?>
>09</option>
                    <option value="10"                 
                <?php if($profile->birthDate=='10'){
                    echo 'selected';
                    }?>
>10</option>
                    <option value="11"                
                <?php if($profile->birthDate=='11'){
                    echo 'selected';
                    }?>
>11</option>
                    <option value="12"                 
                <?php if($profile->birthDate=='12'){
                    echo 'selected';
                    }?>
>12</option>
                    <option value="13"                 
                <?php if($profile->birthDate=='13'){
                    echo 'selected';
                    }?>
>13</option>
                    <option value="14"                 
                <?php if($profile->birthDate=='14'){
                    echo 'selected';
                    }?>
>14</option>
                    <option value="15"                
                <?php if($profile->birthDate=='15'){
                    echo 'selected';
                    }?>
>15</option>
                    <option value="16"                 
                <?php if($profile->birthDate=='16'){
                    echo 'selected';
                    }?>
>16</option>
                    <option value="17"                 
                <?php if($profile->birthDate=='17'){
                    echo 'selected';
                    }?>
>17</option>
                    <option value="18"                 
                <?php if($profile->birthDate=='18'){
                    echo 'selected';
                    }?>
>18</option>
                    <option value="19"                 
                <?php if($profile->birthDate=='19'){
                    echo 'selected';
                    }?>
>19</option>
                    <option value="20"                 
                <?php if($profile->birthDate=='20'){
                    echo 'selected';
                    }?>
>20</option>
                    <option value="21"                 
                <?php if($profile->birthDate=='21'){
                    echo 'selected';
                    }?>
>21</option>
                        <option value="22"                 
                <?php if($profile->birthDate=='22'){
                    echo 'selected';
                    }?>
>22</option>
                        <option value="23"                 
                <?php if($profile->birthDate=='23'){
                    echo 'selected';
                    }?>
>23</option>
                        <option value="24"                 
                <?php if($profile->birthDate=='24'){
                    echo 'selected';
                    }?>
>24</option>
                        <option value="25"                 
                <?php if($profile->birthDate=='25'){
                    echo 'selected';
                    }?>
>25</option>
                        <option value="26"                 
                <?php if($profile->birthDate=='26'){
                    echo 'selected';
                    }?>
>26</option>
                        <option value="27"                 
                <?php if($profile->birthDate=='27'){
                    echo 'selected';
                    }?>
>27</option>
                        <option value="28"                 
                <?php if($profile->birthDate=='28'){
                    echo 'selected';
                    }?>
>28</option>
                        <option value="29"                 
                <?php if($profile->birthDate=='29'){
                    echo 'selected';
                    }?>
>29</option>
                        <option value="30"                 
                <?php if($profile->birthDate=='30'){
                    echo 'selected';
                    }?>
>30</option>
                        <option value="31"                 
                <?php if($profile->birthDate=='31'){
                    echo 'selected';
                    }?>
>31</option>
            </select>
      </div>

      <div class="col-md-2 pb-3"> 
<select name="birthMonth" class="form-control" id="b_day">
  <option value="" selected="selected">মাস</option>
  <option value="01" 

                <?php if($profile->birthMonth=='01'){
                    echo 'selected';
                    }?>

                >01 -জানুয়ারী</option>
                <option value="02"

                <?php if($profile->birthMonth=='02'){
                    echo 'selected';
                    }?>

                  >02 - ফেব্রুয়ারি</option>
                <option value="03" 
                <?php if($profile->birthMonth=='03'){echo 'selected';}?>
                >03 - মার্চ</option>
				<option value="04" 
                <?php if($profile->birthMonth=='04'){echo 'selected';}?>
                >04 - এপ্রিল</option>
				<option value="05" 
                <?php if($profile->birthMonth=='05'){echo 'selected';}?>
                >05 - মে</option>
				<option value="06" 
                <?php if($profile->birthMonth=='06'){echo 'selected';}?>
                >06 - জুন</option>
				<option value="07" 
                <?php if($profile->birthMonth=='07'){echo 'selected';}?>
                >07 - জুলাই</option>
				<option value="08" 
                <?php if($profile->birthMonth=='08'){echo 'selected';}?>
                >08 - আগস্ট</option>
				<option value="09" 
                <?php if($profile->birthMonth=='09'){echo 'selected';}?>
                >09 - সেপ্টেম্বর</option>
				<option value="10" 
                <?php if($profile->birthMonth=='10'){echo 'selected';}?>
                >10 - অক্টোবর</option>
				<option value="11" 
                <?php if($profile->birthMonth=='11'){echo 'selected';}?>
                >11 - নভেম্বর </option>
				<option value="12" 
                <?php if($profile->birthMonth=='12'){echo 'selected';}?>
                >12 - ডিসেম্বর</option>
</select>
</div>

<div class="col-md-2 pb-3"> 
<select name="birthYear" class="form-control" id="b_year">
              <option value="" selected="selected">জন্ম সাল</option>
                
              <option value="1974"
                <?php if($profile->birthYear=='1974'){
                    echo 'selected';
                    }?>
                >1974</option>

        <option value="1975" 
        <?php if($profile->birthYear=='1975'){
                    echo 'selected';
                    }?>
        >1975</option>

        <option value="1976" 
        <?php if($profile->birthYear=='1976'){
                    echo 'selected';
                    }?>
        >1976</option>

        <option value="1977"
        <?php if($profile->birthYear=='1977'){
                    echo 'selected';
                    }?>
        >1977</option>

        <option value="1978"
        <?php if($profile->birthYear=='1978'){
                    echo 'selected';
                    }?>
        >1978</option>

        <option value="1979"
        <?php if($profile->birthYear=='1979'){
                    echo 'selected';
                    }?>
        >1979</option>

        <option value="1980" 
        <?php if($profile->birthYear=='1980'){
                    echo 'selected';
                    }?>
        >1980</option>

        <option value="1981" 
        <?php if($profile->birthYear=='1981'){
                    echo 'selected';
                    }?>
        >1981</option>

        <option value="1982" 
        <?php if($profile->birthYear=='1982'){
                    echo 'selected';
                    }?>
        >1982</option>

        <option value="1983" 
        <?php if($profile->birthYear=='1983'){
                    echo 'selected';
                    }?>
        >1983</option>

        <option value="1984"
        <?php if($profile->birthYear=='1984'){
                    echo 'selected';
                    }?>
        >1984</option>

        <option value="1985"
        <?php if($profile->birthYear=='1985'){
                    echo 'selected';
                    }?>
        >1985</option>

        <option value="1986"
        <?php if($profile->birthDate=='1986'){
                    echo 'selected';
                    }?>
        >1986</option>

        <option value="1987"
        <?php if($profile->birthYear=='1987'){
                    echo 'selected';
                    }?>
        >1987</option>

        <option value="1988"
        <?php if($profile->birthYear=='1988'){
                    echo 'selected';
                    }?>
        >1988</option>

        <option value="1989"
        <?php if($profile->birthYear=='1989'){
                    echo 'selected';
                    }?>
        >1989</option>

        <option value="1990"
        <?php if($profile->birthYear=='1990'){
                    echo 'selected';
                    }?>
        >1990</option>

        <option value="1991" 
        <?php if($profile->birthYear=='1991'){
                    echo 'selected';
                    }?>
        >1991</option>

        <option value="1992"
        <?php if($profile->birthYear=='1992'){
                    echo 'selected';
                    }?>
        >1992</option>

        <option value="1993"
        <?php if($profile->birthYear=='1993'){
                    echo 'selected';
                    }?>
        >1993</option>

        <option value="1994"
        <?php if($profile->birthYear=='1994'){
                    echo 'selected';
                    }?>
        >1994</option>

        <option value="1995"
        <?php if($profile->birthYear=='1995'){
                    echo 'selected';
                    }?>
        >1995</option>

        <option value="1996" 
        <?php if($profile->birthYear=='1996'){
                    echo 'selected';
                    }?>
        >1996</option>

        <option value="1997" 
        <?php if($profile->birthYear=='1997'){
                    echo 'selected';
                    }?>
        >1997</option>

        <option value="1998"
        <?php if($profile->birthYear=='1998'){
                    echo 'selected';
                    }?>
        >1998</option>

        <option value="1999"
        <?php if($profile->birthYear=='1999'){
                    echo 'selected';
                    }?>
        >1999</option>

        <option value="2000" 
        <?php if($profile->birthYear=='2000'){
                    echo 'selected';
                    }?>
        >2000</option>

        <option value="2001" 
        <?php if($profile->birthYear=='2001'){
                    echo 'selected';
                    }?>
        >2001</option>

        <option value="2002"
        <?php if($profile->birthYear=='2002'){
                    echo 'selected';
                    }?>
        >2002</option>
       
        <option value="2003"
        <?php if($profile->birthYear=='2003'){
                    echo 'selected';
                    }?>
        >2003</option>

   
        <option value="2004" 
        <?php if($profile->birthYear=='2004'){
                    echo 'selected';
                    }?>
        >2004</option>

        <option value="2005" 
        <?php if($profile->birthYear=='2005'){
                    echo 'selected';
                    }?>
        >2005</option>

        <option value="2006" 
        <?php if($profile->birthYear=='2006'){
                    echo 'selected';
                    }?>
        >2006</option>

        <option value="2007"
        <?php if($profile->birthYear=='2007'){
                    echo 'selected';
                    }?>
        >2007</option>
         
        <option value="2008"
        <?php if($profile->birthYear=='2008'){
                    echo 'selected';
                    }?>
        >2008</option>

        <option value="2009"
        <?php if($profile->birthYear=='2009'){
                    echo 'selected';
                    }?>
                     >2009</option>

        <option value="2010"
        <?php if($profile->birthYear=='2010'){
                    echo 'selected';
                    }?>
                     >2010</option>

            </select>

</div>

 </div>

<div class="row"> 
<div class="col-md-3"> 
        <h5 class="eng"> Gender :<span class="red" >*</span> </h5>
      </div>

    <div class="col-md-1">  
    <div class="form-check">
          <input class="form-check-input" type="radio" name="gender" id="male" value="Male"
          <?php if($profile->gender=='Male'){
                    echo 'checked';
                    }?>
          >
          <label class="form-check-label eng" for="male">
            Male
          </label>
        </div>
    </div>

    <div class="col-md-1">  
    <div class="form-check">
          <input class="form-check-input" type="radio" name="gender" id="female" value="female" 
          <?php if($profile->gender=='female'){
                    echo 'checked';
                    }?>
          >
          <label class="form-check-label eng" for="female">
            Female
          </label>
        </div>
    </div>

    <div class="col-md-1">  
    <div class="form-check">
          <input class="form-check-input" type="radio" name="gender" 
            <?php if($profile->gender=='Others'){
                    echo 'checked';
                    }?> id="others" value="Others"  >
          <label class="form-check-label eng" for="others">
            Others
          </label>
        </div>
    </div>
    <div class="col-md-2"> </div>
 <div class="row pb-4"> 
 <div class="col-md-5"> 
        <h5 class="eng"> Religion :<span class="red" >* </span> </h5>
      </div>
      <div class="col-md-7">  
      <select name="religion" class="form-control" id="religion">
                <option value="" selected="selected">নির্বাচন করুন</option>
                <option value="Islam" 
                <?php if($profile->religion=='Islam'){
                    echo 'selected';
                    }?> 
                >ইসলাম</option>

                <option value="Hinduism"
                <?php if($profile->religion=='Hinduism'){
                    echo 'selected';
                    }?> 
                >হিন্দু</option>
                <option value="Christianity"
                <?php if($profile->religion=='Christianity'){
                    echo 'selected';
                    }?> 
                >খ্রিষ্টান</option>
                <option value="Buddhism"
                <?php if($profile->religion=='Buddhism'){
                    echo 'selected';
                    }?> 
                >বুদ্ধ</option>
                <option value="Others" 
                <?php if($profile->religion=='Others'){
                    echo 'selected';
                    }?> 
                >অন্যন</option>
              </select>
      </div>
 </div>
</div>

<div class="row pb-3">   
  <div class="col-md-3"> 
    <h5 class="eng"> Place of brith :</h5>
  </div>
  <div class="col-md-3"> 
    <input class="form-control-md form-control" value="<?=$profile->birthPlace;?>" name="birthPlace" placeholder="জন্ম স্থান" type="text">
  </div>
  <div class="col-md-2 "> 
    <h5 class="eng"> Blood Group :</h5>
  </div>
  <div class="col-md-3">   
    <input class="form-control-md form-control" name="bloodGroup" value="<?=$profile->bloodGroup;?>"  placeholder=" রক্তের গ্রুপ" type="text">
  </div>
</div>


<div class="row pb-3">   
  <div class="col-md-3"> 
    <h5 class="eng"> National ID:</h5>
  </div>
  <div class="col-md-3"> 
    <input class="form-control-md form-control" name="nid" value="<?=$profile->nid;?>" placeholder=" জাতীয় পরিচয়পত্র নম্বর" type="text">
  </div>
  <div class="col-md-2 "> 
    <h5 class="eng"> Passport No :</h5>
  </div>
  <div class="col-md-3">   
    <input class="form-control-md form-control" name="passport" value="<?=$profile->passport;?>" placeholder=" পাসপোর্ট নম্বর" type="text">
  </div>
</div>


<div class="row pb-3">   
  <div class="col-md-3"> 
    <h5 class="eng"> Birth Registration:</h5>
  </div>
  <div class="col-md-3"> 
    <input class="form-control-md form-control" name="birthCirtificate" value="<?=$profile->birthCirtificate;?>" placeholder=" জন্ম নিবন্ধন নম্বর" type="text">
  </div>
  <div class="col-md-2 "> 
    <h5 class="eng"> Marital Status :<span class="red" >* </span> </h5>
  </div>
 
  <div class="col-md-3" id="marrige"> 

    <select name="maritalStatus"  onchange="random()" class="form-control " id="marry" > 
    <option value="" selected="selected">Select One</option>
    <option  value="Married" 
    <?php if($profile->maritalStatus=='Married'){
                    echo 'selected';
                    }?> 
    > Married </option>
    <option  value="Single"
    <?php if($profile->maritalStatus=='Single'){
                    echo 'selected';
                    }?>
    
    > Single</option>
    </select>
    <div id="myDIV" style="margin-top:10px; "> 
    <label for="college_name"></lable><input name="wifeName" value="<?=$profile->wifeName;?>" class="form-control" type="text" placeholder="স্বামী / স্ত্রীর নাম">
    </div>
  </div>
</div>
<div class="row pb-3">   
  <div class="col-md-3"> 
    <h5 class="eng"> Quota: <span class="red"> *</span> </h5> 
  </div>
  <div class="col-md-3"> 
  <select name="quota" class="form-control" id="rel"> 
  <option value="" selected="selected">নির্বাচন করুন</option>
  <option value="Freedom Fighter">Freedom Fighter</option>
                <option value="Child of Freedom Fighter" 
                <?php if($profile->quota=='Child of Freedom Fighter'){
                    echo 'selected';
                    }?>
    
                >Child of Freedom Fighter</option>
                <option value="Grand Child of Freedom Fighter"
                <?php if($profile->quota=='Grand Child of Freedom Fighter'){
                    echo 'selected';
                    }?>
    
                >Grand Child of Freedom Fighter</option>
        <option value="Physically Handicapped"
        <?php if($profile->quota=='Physically Handicapped'){
                    echo 'selected';
                    }?>
    
        >Physically Handicapped</option>
                <option value="Orphan"
                
                >Orphan</option>
                <option value="Ethnic Minority"
                <?php if($profile->quota=='Ethnic Minority'){
                    echo 'selected';
                    }?>
                >Ethnic Minority</option>
				<option value="Ansar-VDP">
                <?php if($profile->quota=='Ansar-VDP'){
                    echo 'selected';
                    }?>
                Ansar-VDP</option>
                <option value="Non Quota"
                <?php if($profile->quota=='Non Quota'){
                    echo 'selected';
                    }?>
                >Non Quota</option>
                <option value="Women Quota"
                <?php if($profile->quota=='Women Quota'){
                    echo 'selected';
                    }?>
                >Women Quota</option>
    </select>
  </div>
  <div class="col-md-1 "> 
    <h5 class="eng"> Email : </h5>
  </div>
  <div class="col-md-4">   
  <input class="form-control-md form-control" value="<?=$profile->email;?>" name="email" placeholder="আপনার ইমেল দিন" type="email" required disabled readonly> 
  </div>
</div>



<div class="row pb-3">   
  <div class="col-md-3"> 
    <h5 class="eng"> Mobile Number: <span class="red"> *</span> </h5> 
  </div>
  <div class="col-md-3"> 
  <input class="form-control-md form-control" value="<?=$profile->number;?>" name="number" placeholder="আপনার  মোবাইল নম্বর দিন" type="text" readonly disabled>
  </div>


    


<div class="row p-4 bg-success"> 
     <div class="col-md-6"> <h5 class="eng pb-3">Mailing/Present Address <span class="red"> *</span> </h5>
      <div class="row mt-4"> 
        <div class="col-md-4"> <h5 class="eng">Care of </h5> </div>
        <div class="col-md-8"> 
        <input id="homeaddress"  value="<?=$profile->presentGurdianName;?>" placeholder="প্রার্থীর অভিভাবকের নাম" type="text" name="presentGurdianName" class="form-control-md form-control" ></div>

        <div class="col-md-4 mt-3"> 
        <h5 class="eng">Village/Town/ <span> <br /></span>  Road/House/Flat</h5> </div>
        <div class="col-md-8"> 
      <textarea class="form-control mt-3"   placeholder="গ্রাম/শহর/রোড/বাড়ি/ফ্লাট" name="PresentVillage" id="homeaddress2" cols="15" rows="4"><?=$profile->PresentVillage;?></textarea>
        </div>
        <div class="col-md-4 mt-3">  
        <h5 class="eng"> District : </h5>
        </div>
        <div class="col-md-8 mt-3"> 
        <input type="text" id="homeaddress3"  value="<?=$profile->presentDistrict;?>" name="presentDistrict" placeholder="জেলা" class="form-control-md form-control"></div>
        
        <div class="col-md-4 mt-3">  
        <h5 class="eng"> P.S/Upazila : </h5>
        </div>
        <div class="col-md-8 mt-3"> 
        <input type="text" placeholder="উপজেলা"  value="<?=$profile->presentUpazila;?>" id="homeaddress4" name="presentUpazila" class="form-control-md form-control"></div>

        <div class="col-md-4 mt-3">  
        <h5 class="eng"> Post Office : </h5>
        </div>
        <div class="col-md-8 mt-3"> 
        <input type="text" id="homeaddress5"  value="<?=$profile->presenPostOffice;?>" name="presenPostOffice" placeholder="পোষ্ট-অফিস"  class="form-control-md form-control"></div>

        <div class="col-md-4 mt-3">  
        <h5 class="eng"> Post Code : </h5>
        </div>
        <div class="col-md-8 mt-3"> 
        <input type="text" id="homeaddress6"  value="<?=$profile->presentPostCode;?>" name="presentPostCode" placeholder="পোষ্ট-কোড" class="form-control-md form-control"></div>

      </div>
     </div>
<!----left side address box  start here------->
     <div class="col-md-6"> <h5 class="eng pb-3">Parmanent Address <span class="red"> *</span> <div>
      <input type="checkbox" name="homePostalCHeck" id="homepostalcheck"> 
      <label for="homepostalcheck"> Same as Present address</label></div> </h5>
     
      <div class="row"> 
        <div class="col-md-4"> <h5 class="eng">Care of </h5> </div>
        <div class="col-md-8"> 
        <input type="text" id="billingaddress"  value="<?=$profile->parmanentGurdianName;?>" placeholder="প্রার্থীর অভিভাবকের নাম"  name="parmanentGurdianName" class="form-control-md form-control" ></div>

        <div class="col-md-4 mt-3"> 
          <h5 class="eng">Village/Town/ <span> <br /></span>  Road/House/Flat</h5> </div>
        <div class="col-md-8"> 
      <textarea class="form-control mt-3"  value="" placeholder="গ্রাম/শহর/রোড/বাড়ি/ফ্লাট" name="parmanentVillage" id="billingaddress2" cols="15" rows="4"><?=$profile->parmanentVillage;?></textarea>
        </div>
        <div class="col-md-4 mt-3">  
        <h5 class="eng"> District : </h5>
        </div>
        <div class="col-md-8 mt-3"> 
        <input type="text" placeholder="জেলা"  value="<?=$profile->parmanentDistrict;?>" id="billingaddress3" name="parmanentDistrict" class="form-control-md form-control" ></div>
        
        <div class="col-md-4 mt-3">  
        <h5 class="eng"> P.S/Upazila : </h5>
        </div>
        <div class="col-md-8 mt-3"> 
        <input type="text" id="billingaddress4"  value="<?=$profile->parmanentUpazila;?>" placeholder="উপজেলা" name="parmanentUpazila" class="form-control-md form-control" ></div>

        <div class="col-md-4 mt-3">  
        <h5 class="eng"> Post Office : </h5>
        </div>
        <div class="col-md-8 mt-3"> 
        <input type="text" id="billingaddress5"  value="<?=$profile->parmanentPostOffice;?>" placeholder="পোষ্ট-অফিস" name="parmanentPostOffice" class="form-control-md form-control"></div>

        <div class="col-md-4 mt-3">  
        <h5 class="eng"> Post Code : </h5>
        </div>
        <div class="col-md-8 mt-3"> 
        <input type="text" placeholder="পোষ্ট-কোড"  value="<?=$profile->parmanentPostCode;?>" id="billingaddress6" name="parmanentPostCode" class="form-control-md form-control"></div>

      </div>
     </div>
</div>

<div class="row bg-primary text-light"> 
<div class="col-md-12 text-center py-3"> <h4> Academic Qualifications</h4></div>

<div class="col-md-12 text-center pb-4"> <h5> JSC or Equalvalent Level<span class="red"> *</span> </h5></div>

<div class="col-md-4"> 
     <div class="row"> 
       <div class="col-md-6"><h5> Examination</h5> </div>
       <div class="col-md-6"> 
       <select name="jscExam" class="form-control" id="exam1" >
                            <option value="" selected="selected">Select One</option>
                     <option value="JDC"
                     <?php if($profile->jscExam=='JDC'){
                    echo 'selected';
                    }?>
    
                     
                     >JDC</option>
                     <option value="JSC"
                     <?php if($profile->jscExam=='JSC'){
                    echo 'selected';
                    }?>
                     
                     >JSC</option> 
                          </select>
       </div>
     </div>

</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> Board</h5> </div>
       <div class="col-md-6"> 
       <select name="jscBoard" class="form-control" id="institute1">
                            <option value="" selected="selected">Select One</option>
                            <option value="Dhaka"
                            <?php if($profile->jscBoard=='Dhaka'){
                       echo 'selected';
                          }?>
                            
                            >Dhaka</option>
                            <option value="Cumilla"
                            <?php if($profile->jscBoard=='Cumilla'){
                       echo 'selected';
                          }?>
                            
                            >Cumilla</option>
                            <option value="Rajshahi"
                            <?php if($profile->jscBoard=='Rajshahi'){
                       echo 'selected';
                          }?>
                            
                            >Rajshahi</option>
                            <option value="Jashore"
                            <?php if($profile->jscBoard=='Jashore'){
                       echo 'selected';
                          }?>
                            >Jashore</option>
                            <option value="Chittagong"
                            <?php if($profile->jscBoard=='Chittagong'){
                       echo 'selected';
                          }?>
                            >Chittagong</option>
                            <option value="Barishal"
                            <?php if($profile->jscBoard=='Barishal'){
                       echo 'selected';
                          }?>
                            >Barishal</option>
                            <option value="Sylhet"
                            <?php if($profile->jscBoard=='Sylhet'){
                       echo 'selected';
                          }?>
                            >Sylhet</option>
                            <option value="Dinajpur"
                            <?php if($profile->jscBoard=='Dinajpur'){
                       echo 'selected';
                          }?>
                            
                            >Dinajpur</option>
                            <option value="Madrasah"
                            <?php if($profile->jscBoard=='Madrasah'){
                       echo 'selected';
                          }?>
                            
                            >Madrasah</option>
                            </select>
       </div>
     </div>

</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> Roll No</h5> </div>
       <div class="col-md-6"> 
         <input class="form-control form-control-md" value="<?=$profile->jscRoll;?>" name="jscRoll" type="text" >
       </div>
     </div>

</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6"><h5> Group/Subject</h5> </div>
    <div class="col-md-6"> 
      <select name="jscSubject" class="form-control" id="subject1">
        <option value=""selected="selected">Select One</option>
        <option value="Genarel"
        <?php
        if($profile->jscSubject=='Genarel'){
          echo 'selected';
        }
        
        
        ?>
        
        >Genarel</option>
      </select>
    </div>
  </div>

</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Result :</h5>
    </div>
    <div class="col-md-6" id="resultdiv"> 
    
    <select name="jscResult"  onchange="jscresultbox()" class="form-control" id="jscresult"  >
                                
    <option value="1st Division"
                                <?php if($profile->jscResult=='1st Division'){
                                  echo 'selected';
                                  }?>
                                >1st Division</option>
                                <option value="2nd Division"
                                <?php if($profile->jscResult=='2nd Division'){
                                  echo 'selected';
                                  }?>
                                >2nd Division</option>
                                <option value="3rd Division"
                                <?php if($profile->jscResult=='3rd Division'){
                                  echo 'selected';
                                  }?>
                                >3rd Division</option>
                                <option value="GPA(out of 4)"
                                <?php if($profile->jscResult=='GPA(out of 4)'){
                                  echo 'selected';
                                  }?>
                                >GPA(out of 4)</option>
                                <option value="GPA(out of 5)"
                                <?php if($profile->jscResult=='GPA(out of 5)'){
                                  echo 'selected';
                                  }?>
                                >GPA(out of 5)</option>
												
                              </select>

                              <div id="gpajsc" style="margin-top: 10px;"> 
                              <input class="form-control form-control-md" name="jscGPA" value="<?=$profile->jscGPA;?>" type="text" placeholder="পয়েন্ট ">
                              </div>
     
    </div>
    
   
  </div>
</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Passing Year :</h5>
    </div>
    <div class="col-md-6" id="passing year"> 
    
      <select name="jscYear" class="form-control" id="year2">
        <option value="" >Select One</option>
        <option value="<?=$profile->jscYear?>" selected="selected"><?=$profile->jscYear?></option>
        <option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option>
      </select>
     
    </div>
  </div>
</div>

 


<div class="col-md-12 text-center py-5"> <h5> SSC or Equalvalent Level<span class="red"> *</span> </h5></div>

<div class="col-md-4"> 
     <div class="row"> 
       <div class="col-md-6"><h5> Examination</h5> </div>
       <div class="col-md-6"> 
       <select name="sscExam" class="form-control" id="exam1">
                      <option value="">Select One</option>
                      <option value="<?=$profile->sscExam?>" selected="selected"><?=$profile->sscExam?></option>
                     <option value="S.S.C">S.S.C</option>
                     <option value="Dakhil">Dakhil</option>
                     <option value="S.S.C Vocational">S.S.C Vocational</option>
                     <option value="O Level/Cambridge">O Level/Cambridge</option>
                     <option value="S.S.C Equivalent">S.S.C Equivalent</option>
                          </select>
       </div>
     </div>

</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> Board</h5> </div>
       <div class="col-md-6"> 
       <select name="sscBoard" class="form-control" id="institute1">
                            <option value="" >Select One</option>
                      <option value="<?=$profile->sscBoard?>" selected="selected"><?=$profile->sscBoard?></option>

                            <option value="Dhaka">Dhaka</option>
                            <option value="Cumilla">Cumilla</option>
                            <option value="Rajshahi">Rajshahi</option>
                            <option value="Jashore">Jashore</option>
                            <option value="Chittagong">Chittagong</option>
                            <option value="Barishal">Barishal</option>
                            <option value="Sylhet">Sylhet</option>
                            <option value="Dinajpur">Dinajpur</option>
                            <option value="Madrasah">Madrasah</option>
                            <option value="Technical">Technical</option>
                            <option value="Cambridge International - IGCE">Cambridge International - IGCE</option>
                            <option value="Edexcel International">Edexcel International</option>
                            <option value="Bangladesh Technical Education Board (BTEB)">Bangladesh Technical Education Board (BTEB)</option>
                            <option value="Others">Others</option>
                          </select>
       </div>
     </div>

</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> Roll No</h5> </div>
       <div class="col-md-6"> 
         <input name="sscRoll" value="<?=$profile->sscRoll?>" class="form-control form-control-md" type="text">
       </div>
     </div>

</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6"><h5> Group/Subject</h5> </div>
    <div class="col-md-6"> 
      <select name="sscGroup" class="form-control" id="subject1">
        <option value="">Select One</option>
        <option value="<?=$profile->sscGroup?>" selected="selected"><?=$profile->sscGroup?></option>

        <option value="Science">Science</option>
        <option value="Humanities">Humanities</option>
        <option value="Business Studies">Business Studies</option>
        <option value="Agriculture Technology">Agriculture Technology</option><option value="Architecture and Interior Design Technology">Architecture and Interior Design Technology</option><option value="Automobile Technology">Automobile Technology</option><option value="Civil Technology">Civil Technology</option><option value="Computer Science &amp; Technology">Computer Science &amp; Technology</option><option value="Chemical Technology">Chemical Technology</option><option value="Electrical Technology">Electrical Technology</option><option value="Data Telecommunication and Network Technology">Data Telecommunication and Network Technology</option><option value="Electrical and Electronics Technology">Electrical and Electronics Technology</option><option value="Environmental Technology">Environmental Technology</option><option value="Instrumentation &amp; Process Control Technology">Instrumentation &amp; Process Control Technology</option><option value="Mechanical Technology">Mechanical Technology</option><option value="Mechatronics Technology">Mechatronics Technology</option><option value="Power Technology">Power Technology</option><option value="Refregeration &amp; Air Conditioning Technology">Refregeration &amp; Air Conditioning Technology</option><option value="Telecommunication Technology">Telecommunication Technology</option><option value="Electronics Technology">Electronics Technology</option><option value="Library Science">Library Science</option><option value="Survey">Survey</option><option value="General Mechanics">General Mechanics</option><option value="Firm Machinery">Firm Machinery</option><option value="Textile Technology">Textile Technology</option><option value="Others">Others</option>
      </select>
    </div>
  </div>

</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Result :</h5>
    </div>
    <div class="col-md-6" id="sscresultbox"> 
    
    <select name="sscResult" onchange=" sscpoint()" class="form-control" id="sscresult"  >
                                <option value=""selected="selected">Select</option>
                                <option value="1st Division"
                                <?php if($profile->sscResult=='1st Division'){
                                  echo 'selected';
                                  }?>
                                >1st Division</option>
                                <option value="2nd Division"
                                <?php if($profile->sscResult=='2nd Division'){
                                  echo 'selected';
                                  }?>
                                >2nd Division</option>
                                <option value="3rd Division"
                                <?php if($profile->sscResult=='3rd Division'){
                                  echo 'selected';
                                  }?>
                                >3rd Division</option>
                                <option value="GPA(out of 4)"
                                <?php if($profile->sscResult=='GPA(out of 4)'){
                                  echo 'selected';
                                  }?>
                                >GPA(out of 4)</option>
                                <option value="GPA(out of 5)"
                                <?php if($profile->sscResult=='GPA(out of 5)'){
                                  echo 'selected';
                                  }?>
                                >GPA(out of 5)</option>
												
                              </select>
        <div id="sscpointbox"><input class="mt-2 form-control" value="<?=$profile->sscGPA;?>" name="sscGPA" placeholder="জিপিএ নাম্বার" type="text"> </div>
     
    </div>
    
  
  </div>
</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Passing Year :</h5>
    </div>
    <div class="col-md-6" id="passing year"> 
    
      <select name="sscYear" class="form-control" id="year2">
        <option value="" >Select One</option>
        <option value="<?=$profile->sscYear?>" selected="selected"><?=$profile->sscYear?></option>

        <option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option>
      </select>
     
    </div>
  </div>
</div>
<div class="col-md-4"> 
    <div class="row"> 
      <div class="col-md-6"> <h5>SSC Registration</h5></div>
      <div class="col-md-6"> 
      <input class="mt-2 form-control" value="<?=$profile->sscRegistration?>"  name="sscRegistration" placeholder= "এস এস সি রেজিস্ট্রেশন নাম্বার দিন" type="text">
      </div>
    </div>

</div>



<div class="col-md-12 text-center py-5"> <h5> HSC or Equalvalent Level<span class="red"> *</span> </h5></div>

<div class="col-md-4"> 
     <div class="row"> 
       <div class="col-md-6"><h5> Examination</h5> </div>
       <div class="col-md-6"> 
       <select name="hscExam" onchange="hscexamination()" class="form-control" id="hscexam" >
        <option value="">Select One</option>
        <option value="<?=$profile->hscExam?>" selected="selected"><?=$profile->hscExam?></option>
        
        <option value="H.S.C">H.S.C</option><option value="Alim">Alim</option><option value="Business Management">Business Management</option><option value="Diploma Engineering">Diploma Engineering</option><option value="A Level/Sr. Cambridge">A Level/Sr. Cambridge</option><option value="H.S.C Equivalent">H.S.C Equivalent</option><option value="Diploma in Pharmacy">Diploma in Pharmacy</option>
        <option value="others"> Others</option>
                          </select>

                          <div id="hscbox2">     </div>
       </div>
     
     </div>
    
</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> Board</h5> </div>
       <div class="col-md-6"> 
       <select name="hscBoard" class="form-control" id="institute1">
                            <option value="" >Select One</option>
        <option value="<?=$profile->hscBoard?>" selected="selected"><?=$profile->hscBoard?></option>

                            <option value="Dhaka">Dhaka</option>
                            <option value="Cumilla">Cumilla</option>
                            <option value="Rajshahi">Rajshahi</option>
                             <option value="Jassore">Jassore</option>
                            <option value="Chittagong">Chittagong</option>
                            <option value="Barishal">Barishal</option>
                            <option value="Sylhet">Sylhet</option>
                            <option value="Dinajpur">Dinajpur</option>
                            <option value="Madrasah">Madrasah</option>
                            <option value="Technical">Technical</option>
                            <option value="Cambridge International - IGCE">Cambridge International - IGCE</option>
                            <option value="Edexcel International">Edexcel International</option>
                            <option value="Bangladesh Technical Education Board (BTEB)">Bangladesh Technical Education Board (BTEB)</option>
                            <option value="Others">Others</option>
                          </select>
       </div>
     </div>

</div>
<div class="col-md-4">  
<div class="row"> 
       <div class="col-md-6"><h5> Roll No</h5> </div>
       <div class="col-md-6"> 
         <input name="hscRoll" value="<?=$profile->hscRoll;?>" class="form-control form-control-md" type="text">
       </div>
     </div>

</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6"><h5> Group/Subject</h5> </div>
    <div class="col-md-6" name="hscSubject"  id="hscsubject"> 
     
         <div id="extra"> 
         <input name="hscSubject" value="<?=$profile->hscSubject;?>" class="form-control form-control-md" type="text">
        </div>
     
     
    </div>
  </div>
  
<script>
    
  function hscexamination(){
      var hsc = document.getElementById('hscexam').value;

    if (hsc==='H.S.C' || hsc==='Alim' || hsc==='Business Management' || hsc==='A Level/Sr. Cambridge' || hsc==='H.S.C Equivalent') {
      document.getElementById('hscsubject').innerHTML='<select name="hscSubject" id="sub" class="form-control "><option>Select One</option><option value="<?=$profile->hscSubject;?>"><?=$profile->hscSubject;?></option><option value="Science">Science</option><option value="Humanities">Humanities</option> <option value="Bussiness Studies">Bussiness Studies</option> <option value="Others">Others</option> </select>'

    }else if(hsc==='Diploma Engineering' || hsc==='Diploma in Pharmacy'){
      document.getElementById('hscsubject').innerHTML='<select onchange="newbox()" id="submenu" name="hscSubject" class="form-control "><option value="<?=$profile->hscSubject;?>"><?=$profile->hscSubject;?></option><option value="Agriculture Technology">Agriculture Technology</option> <option value="Architecture and Interior Design Technology">Architecture and Interior Design Technology</option><option value="Automobile Technology">Automobile Technology</option><option value="Civil Technology">Civil Technology</option><option value="Computer Science & Technology">Computer Science & Technology</option><option value="Chemical Technology">Chemical Technology</option><option value="Electrical Technology">Electrical Technology</option><option value="Data Telecommunication and Network Technology">Data Telecommunication and Network Technology</option><option value="Electrical and Electronics Technology">Electrical and Electronics Technology</option><option value="Environmental Technology">Environmental Technology</option><option value="Instrumentation & Process Control Technology">Instrumentation & Process Control Technology</option><option value="Mechanical Technology">Mechanical Technology</option><option value="Mechatronics Technology">Mechatronics Technology</option><option value="Power Technology">Power Technology</option><option value="Refregeration & Air Conditioning Technology">Refregeration & Air Conditioning Technology</option><option value="Telecommunication Technology">Telecommunication Technology</option><option value="Electronics Technology">Electronics Technology</option><option value="Library Science">Library Science</option><option value="Survey">Survey</option><option value="General Mechanics">General Mechanics</option><option value="Firm Machinery">Firm Machinery</option><option value="Printing Technology">Printing Technology</option><option value="others">Others</option></select>'
   
    }else if( hsc==='others'){
      document.getElementById('hscsubject').innerHTML='<input name="hscSubject" value="<?=$profile->hscSubject;?>" class="form-control mt-2" type="text">'
    }
    }

  </script>
</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Result :</h5>
    </div>
    <div class="col-md-6" id="hscresultbox"> 
    
    <select name="hscResult" class="form-control" id="hscgradpoint" onchange="hscpoint()"  >
                                <option value=""selected="selected">Select</option>
                                <option value="1st Division"
                                <?php if($profile->hscResult=='1st Division'){
                                  echo 'selected';
                                  }?>
                                >1st Division</option>
                                <option value="2nd Division"
                                <?php if($profile->hscResult=='2nd Division'){
                                  echo 'selected';
                                  }?>
                                >2nd Division</option>
                                <option value="3rd Division"
                                <?php if($profile->hscResult=='3rd Division'){
                                  echo 'selected';
                                  }?>
                                >3rd Division</option>
                                <option value="GPA(out of 4)"
                                <?php if($profile->hscResult=='GPA(out of 4)'){
                                  echo 'selected';
                                  }?>
                                >GPA(out of 4)</option>
                                <option value="GPA(out of 5)"
                                <?php if($profile->hscResult=='GPA(out of 5)'){
                                  echo 'selected';
                                  }?>
                                >GPA(out of 5)</option>
												
												
                              </select>
                <div id="hscpointbox2"><input name="hscGPA" value="<?=$profile->hscGPA;?>" class="mt-2 form-control" placeholder="জিপিএ নাম্বার" type="text"> </div>
     
    </div>
    
   
  </div>
</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Passing Year :</h5>
    </div>
    <div class="col-md-6" id="passing year"> 
    
      <select name="hscYear" class="form-control" id="year2">
        <option value="" >Select One</option>
        <option value="<?=$profile->hscYear;?>" selected="selected"><?=$profile->hscYear;?></option>
        
        <option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option>
      </select>
     
    </div>
  </div>
</div>
<div class="col-md-4"> 
    <div class="row"> 
      <div class="col-md-6"> <h5>HSC Registration</h5></div>
      <div class="col-md-6"> 
      <input class="mt-2 form-control"  value="<?=$profile->hscRegistration;?>" name="hscRegistration" placeholder= "এইচ এস সি রেজিস্ট্রেশন নাম্বার দিন" type="text">
      </div>
    </div>

</div>




<div class="col-md-12 text-center py-5"> <h5> Degree/Honars or Equalvalent Level<span class="red"> *</span> </h5></div>

<div class="col-md-4"> 
     <div class="row"> 
       <div class="col-md-6"><h5> Examination</h5> </div>
       <div class="col-md-6"> 
       <select name="degreeExam" onchange="dgree()" class="form-control" id="dgreeid">
                      <option value="" >Select One</option>
        <option value="<?=$profile->degreeExam;?>" selected="selected"><?=$profile->degreeExam;?></option>
                      
                      <option value="B.A (Honors)">B.A (Honors) </option>
                      <option value="B.Com (Honors)">B.Com (Honors)</option>
                      <option value="B.Ed (Honors)">B.Ed (Honors)</option>
                      <option value="B.S.S (Honors)"> B.S.S (Honors)</option>
                      <option value="B.Sc (Honors)">B.Sc (Honors)</option>
                      <option value="LL.B (Honors) "> LL.B (Honors) </option>
                      <option value="B.A(PassCourse)"> B.A(PassCourse) </option>
                      <option value="B.Com (pass Course)"> B.Com (pass Course)</option>
                      <option value="BBS (pass course)"> BBS (pass course) </option>
                      <option value="B.Sc (pass course)">B.Sc (pass course) </option>
                      <option value="B.S.S (pass Course)">B.S.S (pass Course) </option>
                      <option value="L.L.B (pass course)">L.L.B (pass course)</option>
                      <option value="M.B.B.S/ B.D.S ">M.B.B.S/ B.D.S </option>
                      <option value="B.Sc(Engineering/Architecture)">B.Sc(Engineering/Architecture)</option><option value="B.Sc(Agricultural Science)">B.Sc(Agricultural Science)</option>
                      <option value="B.tech"> B.tech</option>
                      <option value="BBA">BBA</option>
                      <option value="BBS">BBS</option>
                      <option value="Fazil">Fazil</option>
                      <option value="Others">Others</option>
                    </select>
       </div>
     </div>
     
</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> Subject / Degree</h5> </div>
       <div class="col-md-6" id="dgreesub"> 
       <input type="text" class="form-control" name="degreeSubject" value="<?=$profile->degreeSubject;?>">

       </div>
     </div>

</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> Course Duration</h5> </div>
       <div class="col-md-6"> 
        <select name="degreeCourseDuration" class="form-control form-control-md" id="duration3">
          <option value="">Select One</option>
        <option value="<?=$profile->degreeCourseDuration;?>" selected="selected"><?=$profile->degreeCourseDuration;?></option>

          <option value="02 Years">02 Years</option>
          <option value="03 Years">03 Years</option>
          <option value="04 Years">04 Years</option>
          <option value="05 Years">05 Years</option>
          
        </select>
       </div>
     </div>

</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6"><h5> University/Institute</h5> </div>
    <div class="col-md-6"> 
<input type="text" class="form-control form-control-md " value="<?=$profile->degreeUniversity;?>" name="degreeUniversity">
    </div>
  </div>

</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Result :</h5>
    </div>
    <div class="col-md-6" id="dgreebox"> 
    
    <select name="degreeResult" onchange="dgreeresultbox()" class="form-control" id="degreeid"  >
                                 <option value="">Select One</option>
                                <option value="1st Division"
                                <?php if($profile->degreeResult=='1st Division'){
                                  echo 'selected';
                                  }?>
                                >1st Division</option>
                                <option value="2nd Division"
                                <?php if($profile->degreeResult=='2nd Division'){
                                  echo 'selected';
                                  }?>
                                >2nd Division</option>
                                <option value="3rd Division"
                                <?php if($profile->degreeResult=='3rd Division'){
                                  echo 'selected';
                                  }?>
                                >3rd Division</option>
                                <option value="GPA(out of 4)"
                                <?php if($profile->degreeResult=='GPA(out of 4)'){
                                  echo 'selected';
                                  }?>
                                >GPA(out of 4)</option>
                                <option value="GPA(out of 5)"
                                <?php if($profile->degreeResult=='GPA(out of 5)'){
                                  echo 'selected';
                                  }?>
                                >GPA(out of 5)</option>
												
												
                              </select>
                              <div id="degree2"><input name="degreeCGPA" value="<?=$profile->degreeCGPA;?>" class="mt-2 form-control" placeholder="সি জিপিএ নাম্বার" type="text"> </div>
     
    </div>
    
  
  </div>
</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Passing Year :</h5>
    </div>
    <div class="col-md-6" id="passing year"> 
    
      <select name="degreePassingYear" class="form-control" id="year2">
        <option value="">Select One</option>
        <option value="<?=$profile->degreePassingYear;?>" selected="selected"><?=$profile->degreePassingYear;?></option>
        <option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option>
      </select>
    
    </div>
  </div>
</div>
<div class="col-md-12 text-center py-5"> <h5> Master Equalvalent Level<span class="red"> *</span> </h5></div>

<div class="col-md-4"> 
     <div class="row"> 
       <div class="col-md-6"><h5> Examination</h5> </div>
       <div class="col-md-6"> 
       <select name="masterExam" class="form-control" id="exam1" >
                            <option value="">Select One</option>
        <option value="<?=$profile->masterExam;?>" selected="selected"><?=$profile->masterExam;?></option>

                     <option value="M.A">M.A</option>
                     <option value="M.S.S">M.S.S</option>
                     <option value="M.SC">M.SC</option>
                     <option value="M.COM">M.COM</option>
                     <option value="M.B.A">M.B.A</option>
                     <option value="L.L.M">L.L.M</option>
                     <option value="M.Phil">M.Phil</option>
                     <option value="Kamil">Kamil</option>
                     <option value="Others">Others</option>
                </select>
       </div>
     </div>
</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> University/Institute</h5> </div>
       <div class="col-md-6"> 
          <input name="masterUniversity" value="<?=$profile->masterUniversity;?>" class="form-control form-control-md" type="text" >
       </div>
     </div>
</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5>Course Duration </h5> </div>
       <div class="col-md-6"> 
         <select class="form-control" name="masterCourseDuration" id=""> 
        <option value="<?=$profile->masterCourseDuration;?>" selected="selected"><?=$profile->masterCourseDuration;?></option>

            <option value=""> Select one</option>
            <option value="2 Years"> 2 Years</option>
            <option value="1 Years"> 1 Years </option>
         </select>
       </div>
     </div>

</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6"><h5> Subject/Degree</h5> </div>
    <div class="col-md-6"> <input name="masterSubject" value="<?=$profile->masterSubject;?>" class="form-control form-control-md" type="text">  
      </div>
  </div>
</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Result :</h5>
    </div>
    <div class="col-md-6" id="masterbox"> 
    
    <select name="masterResult" onchange="masterresultbox()" class="form-control" id="masterid"  >
                                <option value="">Select</option>
        <option value="<?=$profile->masterResult;?>" selected="selected"><?=$profile->masterResult;?></option>

											        	<option value="1st Class">1st Class</option>
                                <option value="2nd Class">2nd Class</option>
                                <option value="3rd Class">3rd Class</option>
                                <option value="CGPA(out of 4)">CGPA(out of 4)</option>
											        	<option value="CGPA(out of 5)">CGPA(out of 5)</option>
												
                              </select>
                              <div id="master2"> <input class="mt-2 form-control"  value="<?=$profile->masterCGPA;?>" name="masterCGPA" placeholder="সি জিপিএ নাম্বার" type="text"></div>
     
    </div>
  
  </div>
</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Passing Year :</h5>
    </div>
    <div class="col-md-6" id="passing year"> 
    
      <select name="masterPassingYear" class="form-control" id="year2">
        <option value="" >Select One</option>
        <option value="<?=$profile->masterPassingYear;?>" selected="selected"><?=$profile->masterPassingYear;?></option>

        <option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option>
      </select>
     
    </div>
  </div>
</div>
</div>
<div class="row bg-primary text-light"> 
  <div class="col-md-12 py-5 text-center"> 
    <h4>Professional Experience</h4>
  </div>
    <div class="col-md-3"><h5> Designation/Post name</h5> </div>
    <div class="col-md-3"> <input class="form-control form-control-md" value="<?=$profile->postName;?>" name="postName" placeholder="পদবী/পদের নাম" type="text"></div>
    <div class="col-md-3"><h5> Organization Name</h5> </div>
    <div class="col-md-3"> <input class="form-control form-control-md" value="<?=$profile->organizationName;?>" name="organizationName" placeholder="প্রতিষ্ঠানের নাম" type="text"></div>
    <div class="col-md-3 mt-3 "><h5> Service Start Date</h5> </div>
    <div class="col-md-3 mt-3"> <input class="form-control form-control-md" value="<?=$profile->jobStartTime;?>" name="jobStartTime" placeholder="সার্ভিস শুরুর তারিখ" type="text"></div>
    <div class="col-md-3 mt-3"><h5> Service End date</h5> </div>
    <div class="col-md-3 mt-3"> <input class="form-control form-control-md" value="<?=$profile->jobEndTime;?>" name="jobEndTime" placeholder="সার্ভিস সমাপ্তির তারিখ" type="text"></div>

    <div class="col-md-6 mt-5"><h5 class="text-center"> Responsibilities</h5> </div>
    <div class="col-md-6 mt-5"> <input class="form-control form-control-md" value="<?=$profile->jobResponsibility;?>" name="jobResponsibility" placeholder="দায়িত্ব" type="text"></div>
</div>

<div class="row mt-4 bg-primary text-light"> 
       <div class="col-md-4"><h5 class=""> Departmental Candidate Status</h5> </div>
       <div class="col-md-3"> 
       <input class="form-control form-control-md" value="<?=$profile->departmentalCandidateStatus;?>" name="departmentalCandidateStatus" placeholder="বিভাগীয় প্রার্থীর অবস্থা" type="text">
       </div>
       <div class="col-md-5"> 
        <div class="row"> 
          <div class="col-md-6"> <h5>Minimum weight </h5></div>
          <div class="col-md-6"> <input class="form-control form-control-md" value="<?=$profile->weight;?>" name="weight" placeholder="আপনার ওজন" type="text"> </div>
        </div>   
      </div>

      <div class="col-md-3 mt-3"><h5>Minimum Height (Feet-inc)</h5> </div>
    <div class="col-md-3 mt-3"> <input class="form-control form-control-md" value="<?=$profile->height;?>" name="height" placeholder="আপনার উচ্চতা" type="text"></div>
    <div class="col-md-3 mt-3"><h5>Minimum Chest Size </h5> </div>
    <div class="col-md-3 mt-3"> <input class="form-control form-control-md" value="<?=$profile->chestSize;?>"  name="chestSize" placeholder="আপনার বুকের মাপ" type="text"></div>

    <div class="col-md-6 text-center mt-4 "><h5> Computer Training Institute Name </h5> </div>
    <div class="col-md-6 mt-4"> <input class="form-control form-control-md" value="<?=$profile->computerTrainingServices;?>" name="computerTrainingServices" placeholder=" " type="text"></div>
       <div class="col-md-6 mt-4"> <h5> Typeing speed (1 min)</h5></div>
       <div class="col-md-3 mt-4"> <input class="form-control form-control-md" placeholder=" Bangla" value="<?=$profile->banglaTypingSpeed;?>" name="banglaTypingSpeed" type="text"> </div>
       <div class="col-md-3 mt-4">  <input class="form-control form-control-md" placeholder=" English" type="text" value="<?=$profile->englishTypingSpeed;?>" name="englishTypingSpeed"></div>
    <div class="col-md-3 mt-4"> 
       <h5> Upload photo <span class="red">*</span></h5>
     </div>
    <div class="col-md-3 mt-4"> 
    <img src="images/<?=$profile->applicantPhoto;?>" width="100px" height="100px">

    <input class="form-control-file form-control-file-md" type="hidden" value="<?=$profile->applicantPhoto;?>"   name="applicantPhoto" >
       <input class="form-control-file form-control-file-md" type="file"    name="applicantPhotoNew" >
    </div>

    <div class="col-md-3 mt-4"> 
       <h5> Upload Signuture <span class="red">*</span></h5>
     </div>
    <div class="col-md-3 mt-4"> 
      <img src="images/<?=$profile->applicantSignature;?>" width="100px" height="100px">
    <input class="form-control-file form-control-file-md" type="hidden" value="<?=$profile->applicantSignature;?>"   name="applicantSignature" required>
       <input class="form-control-file form-control-file-md" type="file"   name="applicantSignatureNew">
    </div>
  <!-- password field here -->

</div>

<div class="col-md-12 mt-5 text-center"> 
<button type="submit" class="btn btn-lg btn-warning ">
								Apply now
							</button>
</div>

  </form>
</div>
</body>
<script type="text/javascript">
  function setBillingAddress(){
    if ($("#homepostalcheck").is(":checked")) {
      $('#billingaddress').val($('#homeaddress').val());
      $('#billingaddress2').val($('#homeaddress2').val());
      $('#billingaddress3').val($('#homeaddress3').val());
      $('#billingaddress4').val($('#homeaddress4').val());
      $('#billingaddress5').val($('#homeaddress5').val());
      $('#billingaddress6').val($('#homeaddress6').val());
    } else {
      $('#billingaddress').removeAttr('disabled');
      $('#billingaddress2').removeAttr('disabled');
      $('#billingaddress3').removeAttr('disabled');
      $('#billingaddress4').removeAttr('disabled');
      $('#billingaddress5').removeAttr('disabled');
      $('#billingaddress6').removeAttr('disabled');
    }
  }
  
  $('#homepostalcheck').click(function(){
    setBillingAddress();
  })
  
  </script>

<script src="js/jquery-3.5.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
</html>
