<?php
ob_start();
session_start();

require_once 'db.php';
$success = "";
$error_message = "";

if(isset($_SESSION['otpemail'])){
    $email=$_SESSION['otpemail'];

   
    $sql="SELECT * FROM users WHERE email='{$email}' ";
    $statement=$connection->prepare($sql);
    $statement->execute();
    $count = $statement->fetchColumn();

    if ($count>0) {
        // generate OTP
        $otp = rand(100000, 999999);
        // Send OTP
        require_once("mail_function.php");
        $mail_status = sendOTP($email, $otp);

        if ($mail_status == 1) {
            $sql="INSERT INTO otp_expiry(otp,is_expired) VALUES (:otp,0)";
            $statement=$connection->prepare($sql);
            $result=$statement->execute([':otp'=>$otp]);

            $current_id = $connection->lastInsertId();
            if (!empty($current_id)){
                $success=1;
             $resendOtpSuccess="Otp sent again";
             $_SESSION['resendOtpSuccess']=$resendOtpSuccess;
                header('location:submitotp.php');
            }
        }
    } else {
        $error_message = "Email not exists!";
        $_SESSION['error_message']=$error_message;
        header('location:ForgetPassword.php');


    }
}


if(isset($_SESSION['otpnumber'])){
    $number=$_SESSION['otpnumber'];

   
    $sql="SELECT * FROM users WHERE number='{$number}' ";
    $statement=$connection->prepare($sql);
    $statement->execute();
    $count = $statement->fetchColumn();

    if ($count>0) {
        // generate OTP
        $otp = rand(100000, 999999);
        // Send OTP
        require_once("sms.php");
      

        if (send_sms($number, $otp)) {
            $sql="INSERT INTO otp_expiry(otp,is_expired) VALUES (:otp,0)";
            $statement=$connection->prepare($sql);
            $result=$statement->execute([':otp'=>$otp]);

            $current_id = $connection->lastInsertId();
            if (!empty($current_id)){
                $success=1;
             $resendOtpSuccess="Otp sent again";
             $_SESSION['resendOtpSuccess']=$resendOtpSuccess;
                header('location:submitotp.php');
            }
        }
    } else {
        $error_message = "Number not exists!";
        $_SESSION['error_message']=$error_message;
        header('location:ForgetPassword.php');


    }
}
?>