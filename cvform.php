<?php
ob_start();
session_start();
require 'header.php';
?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Welcome to studio website</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script type="text/javascript" src="js/jquery.min.js"></script>
 
  <link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
 
</head>
<body>
	<div class="container px-2"> 




<!---------copy---------->
<div class="container p-5 bg-primary"> 
  <div >
<?php


if(array_key_exists('emailExistErr',$_SESSION)){
  echo '<p class="text-center bg-danger text-white p-2 mb-2">'.$_SESSION['emailExistErr'].'</p><br>';
  unset($_SESSION['emailExistErr']);
}

if(array_key_exists('numberExistErr',$_SESSION)){
  echo '<p class="text-center bg-danger text-white p-2 mb-2">'.$_SESSION['numberExistErr'].'</p><br>';
  unset($_SESSION['numberExistErr']);
}
  if(array_key_exists('numberErr',$_SESSION)){
    echo '<p class="text-center bg-danger text-white p-2 mb-2">'.$_SESSION['numberErr'].'</p><br>';
    unset($_SESSION['numberErr']);
  }
  if(array_key_exists('fileUploadError',$_SESSION)){
    echo  '<p class="text-center bg-danger text-white p-2 mb-2">'. $_SESSION['fileUploadError'].'</p><br>';
    unset($_SESSION['fileUploadError']);
  }
if(array_key_exists('passwordErr',$_SESSION)){
  echo  '<p class="text-center bg-danger text-white p-2 mb-2">'.$_SESSION['passwordErr'].'</p><br>';
  unset($_SESSION['passwordErr']);
  }
  if(array_key_exists('emailError',$_SESSION)){
    echo  '<p class="text-center bg-danger text-white p-2 mb-2">'.$_SESSION['emailError'].'</p><br>';
    unset($_SESSION['emailError']);
    }

?>
  </div>
   <form enctype="multipart/form-data" method="POST" action="storeProcess.php" enctype="multipart/form-data">
     <div class="row pb-3">  
      <div class="col-md-3"> 
        <h5 class="eng"> Applicant Name :  <span class="red" >* </span> </h5>
      </div>
      <div class="col-md-8"> 
        <input class="form-control-lg  form-control" name="applicantName" placeholder="প্রার্থীর নাম" type="text" required >
      </div>
      </div>
<div class="row pb-3">    
      <div class="col-md-3"> 
        <h5 class="eng"> Father Name :  <span class="red" >* </span></h5>
      </div>
      <div class="col-md-8"> 
        <input class="form-control-lg form-control" name="fatherName" placeholder="পিতার নাম" type="text" required>
      </div>
 </div>

 <div class="row pb-3">   
      <div class="col-md-3"> 
        <h5 class="eng"> Mother Name :<span class="red" >* </span> </h5>
      </div>
      <div class="col-md-8 pb-3"> 
        <input class="form-control-lg form-control" name="motherName" placeholder="মাতার নাম" type="text" required >
      </div>
 </div>
 
 <div class="row pb-3">   
      <div class="col-md-3"> 
        <h5 class="eng"> Date of Birth :<span class="red" >* </span> </h5>
      </div>

      <div class="col-md-2 pb-3"> 

     
            <select name="birthDate" class="form-control" id="b_day" required>
              <option value="" selected="selected">দিন</option>
				<option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option>
            </select>
      </div>

      <div class="col-md-2 pb-3"> 
<select name="birthMonth" class="form-control" id="b_day" required>
  <option value="" selected="selected">মাস</option>
  <option value="01">01 -জানুয়ারী</option>
				<option value="02">02 - ফেব্রুয়ারি</option>
				<option value="03">03 - মার্চ</option>
				<option value="04">04 - এপ্রিল</option>
				<option value="05">05 - মে</option>
				<option value="06">06 - জুন</option>
				<option value="07">07 - জুলাই</option>
				<option value="08">08 - আগস্ট</option>
				<option value="09">09 - সেপ্টেম্বর</option>
				<option value="10">10 - অক্টোবর</option>
				<option value="11">11 - নভেম্বর </option>
				<option value="12">12 - ডিসেম্বর</option>
</select>
</div>

<div class="col-md-2 pb-3"> 
<select name="birthYear" class="form-control" id="b_year" required>
              <option value="" selected="selected">জন্ম সাল</option>
				<option value="1974">1974</option>
        <option value="1975">1975</option>
        <option value="1976">1976</option>
        <option value="1977">1977</option>
        <option value="1978">1978</option>
        <option value="1979">1979</option>
        <option value="1980">1980</option>
        <option value="1981">1981</option>
        <option value="1982">1982</option>
        <option value="1983">1983</option>
        <option value="1984">1984</option>
        <option value="1985">1985</option>
        <option value="1986">1986</option>
        <option value="1987">1987</option>
        <option value="1988">1988</option>
        <option value="1989">1989</option>
        <option value="1990">1990</option>
        <option value="1991">1991</option>
        <option value="1992">1992</option>
        <option value="1993">1993</option>
        <option value="1994">1994</option>
        <option value="1995">1995</option>
        <option value="1996">1996</option>
        <option value="1997">1997</option>
        <option value="1998">1998</option>
        <option value="1999">1999</option>
        <option value="2000">2000</option>
        <option value="2001">2001</option>
        <option value="2002">2002</option>
        <option value="2002">2002</option>
        <option value="2002">2002</option>
        <option value="2003">2002</option>
        <option value="2004">2002</option>
        <option value="2005">2002</option>
        <option value="2006">2006</option>
        <option value="2007">2007</option>
        <option value="2008">2008</option>
        <option value="2009">2009</option>
        <option value="2010">2010</option>
            </select>

</div>

 </div>

<div class="row"> 
<div class="col-md-3"> 
        <h5 class="eng"> Gender :<span class="red" >*</span> </h5>
      </div>

    <div class="col-md-1">  
    <div class="form-check">
          <input class="form-check-input" type="radio" name="gender" id="male" value="Male" required>
          <label class="form-check-label eng" for="male">
            Male
          </label>
        </div>
    </div>

    <div class="col-md-1">  
    <div class="form-check">
          <input class="form-check-input" type="radio" name="gender" id="female" value="female" required >
          <label class="form-check-label eng" for="female">
            Female
          </label>
        </div>
    </div>

    <div class="col-md-1">  
    <div class="form-check">
          <input class="form-check-input" type="radio" name="gender" id="others" value="Others" required >
          <label class="form-check-label eng" for="others">
            Others
          </label>
        </div>
    </div>
    <div class="col-md-2"> </div>
 <div class="row pb-4"> 
 <div class="col-md-5"> 
        <h5 class="eng"> Religion :<span class="red" >* </span> </h5>
      </div>
      <div class="col-md-7">  
      <select name="religion" class="form-control" id="religion" required>
                <option value="" selected="selected">নির্বাচন করুন</option>
                <option value="Islam">ইসলাম</option>
                <option value="Hinduism">হিন্দু</option>
                <option value="Christianity">খ্রিষ্টান</option>
                <option value="Buddhism">বুদ্ধ</option>
                <option value="Others">অন্যন</option>
              </select>
      </div>
 </div>
</div>

<div class="row pb-3">   
  <div class="col-md-3"> 
    <h5 class="eng"> Place of brith :</h5>
  </div>
  <div class="col-md-3"> 
    <input class="form-control-md form-control" name="birthPlace" placeholder="জন্ম স্থান" type="text">
  </div>
  <div class="col-md-2 "> 
    <h5 class="eng"> Blood Group :</h5>
  </div>
  <div class="col-md-3">   
    <input class="form-control-md form-control" name="bloodGroup" placeholder=" রক্তের গ্রুপ" type="text">
  </div>
</div>


<div class="row pb-3">   
  <div class="col-md-3"> 
    <h5 class="eng"> National ID:</h5>
  </div>
  <div class="col-md-3"> 
    <input class="form-control-md form-control" name="nid" placeholder=" জাতীয় পরিচয়পত্র নম্বর" type="text">
  </div>
  <div class="col-md-2 "> 
    <h5 class="eng"> Passport No :</h5>
  </div>
  <div class="col-md-3">   
    <input class="form-control-md form-control" name="passport" placeholder=" পাসপোর্ট নম্বর" type="text">
  </div>
</div>


<div class="row pb-3">   
  <div class="col-md-3"> 
    <h5 class="eng"> Birth Registration:</h5>
  </div>
  <div class="col-md-3"> 
    <input class="form-control-md form-control" name="birthCirtificate" placeholder=" জন্ম নিবন্ধন নম্বর" type="text">
  </div>
  <div class="col-md-2 "> 
    <h5 class="eng"> Marital Staus :<span class="red" >* </span> </h5>
  </div>
 
  <div class="col-md-3" id="marrige"> 

    <select name="maritalStatus"  onchange="random()" class="form-control " id="marry" required> 
    <option value="" selected="selected">Select One</option>
    <option  value="Married"> Married </option>
    <option  value="Single"> Single</option>
    </select>
    <div id="myDIV" style="margin-top:10px; "> </div>
  </div>
</div>
 
<script>  

  function random(){
        var a = document.getElementById('marry').value;
        if(a==='Married'){
           document.getElementById('myDIV').innerHTML='<label for="college_name"></lable><input name="wifeName" class="form-control" type="text" placeholder="স্বামী / স্ত্রীর নাম" required >';
        }else if(a==='Single'){
          document.getElementById('myDIV').innerHTML='<div id="my"> </div>';
        }
   
      }

</script>


<div class="row pb-3">   
  <div class="col-md-3"> 
    <h5 class="eng"> Quota: <span class="red"> *</span> </h5> 
  </div>
  <div class="col-md-3"> 
  <select name="quota" class="form-control" id="rel" required> 
  <option value="" selected="selected">নির্বাচন করুন</option>
  <option value="Freedom Fighter">Freedom Fighter</option>
				<option value="Child of Freedom Fighter">Child of Freedom Fighter</option>
				<option value="Grand Child of Freedom Fighter">Grand Child of Freedom Fighter</option>
        <option value="Physically Handicapped">Physically Handicapped</option>
				<option value="Orphan">Orphan</option>
				<option value="Ethnic Minority">Ethnic Minority</option>
				<option value="Ansar-VDP">Ansar-VDP</option>
				<option value="Non Quota">Non Quota</option>
				<option value="Women Quota">Women Quota</option>
    </select>
  </div>
  <div class="col-md-2 "> 
    <h5 class="eng"> Email :<span class="red"> *</span> </h5>
  </div>
  <div class="col-md-3">   
  <input class="form-control-md form-control" name="email" placeholder="আপনার ইমেল দিন" type="email" required>
  </div>
</div>



<div class="row pb-3">   
  <div class="col-md-3"> 
    <h5 class="eng"> Mobile Number: <span class="red"> *</span> </h5> 
  </div>
  <div class="col-md-3"> 
  <input class="form-control-md form-control" name="number" placeholder="আপনার  মোবাইল নম্বর দিন" type="text" required>
  </div>
  <div class="col-md-2"> 
    <h5 class="eng"> Confirm Mobile : <span class="red"> *</span> </h5> 
  </div>
  <div class="col-md-3"> 
  <input class="form-control-md form-control" name="confirmNumber" placeholder="পুনরায় মোবাইল নম্বর দিন" type="text" required>
  </div>
</div>
    


<div class="row p-4 bg-success"> 
     <div class="col-md-6"> <h5 class="eng pb-3">Mailing/Present Address <span class="red"> *</span> </h5>
      <div class="row mt-4"> 
        <div class="col-md-4"> <h5 class="eng">Care of </h5> </div>
        <div class="col-md-8"> 
        <input id="homeaddress" placeholder="প্রার্থীর অভিভাবকের নাম" type="text" name="presentGurdianName" class="form-control-md form-control" required>
      </div>

        <div class="col-md-4 mt-3"> 
        <h5 class="eng">Village/Town/ <span> <br /></span>  Road/House/Flat</h5> </div>
        <div class="col-md-8"> 
      <textarea class="form-control mt-3" placeholder="গ্রাম/শহর/রোড/বাড়ি/ফ্লাট" name="PresentVillage" id="homeaddress2" cols="15" rows="4" required></textarea>
        </div>
        <div class="col-md-4 mt-3">  
        <h5 class="eng"> District : </h5>
        </div>
        <div class="col-md-8 mt-3"> 
        <input type="text" id="homeaddress3" name="presentDistrict" placeholder="জেলা" class="form-control-md form-control" required></div>
        
        <div class="col-md-4 mt-3">  
        <h5 class="eng"> P.S/Upazila : </h5>
        </div>
        <div class="col-md-8 mt-3"> 
        <input type="text" placeholder="উপজেলা" id="homeaddress4" name="presentUpazila" class="form-control-md form-control" required></div>

        <div class="col-md-4 mt-3">  
        <h5 class="eng"> Post Office : </h5>
        </div>
        <div class="col-md-8 mt-3"> 
        <input type="text" id="homeaddress5" name="presenPostOffice" placeholder="পোষ্ট-অফিস"  class="form-control-md form-control" required></div>

        <div class="col-md-4 mt-3">  
        <h5 class="eng"> Post Code : </h5>
        </div>
        <div class="col-md-8 mt-3"> 
        <input type="text" id="homeaddress6" name="presentPostCode" placeholder="পোষ্ট-কোড" class="form-control-md form-control" required></div>

      </div>
     </div>
<!----left side address box  start here------->
     <div class="col-md-6"> <h5 class="eng pb-3">Parmanent Address <span class="red"> *</span> <div>
      <input type="checkbox" name="homePostalCHeck" id="homepostalcheck"> 
      <label for="homepostalcheck"> Same as Present address</label></div> </h5>
     
      <div class="row"> 
        <div class="col-md-4"> <h5 class="eng">Care of </h5> </div>
        <div class="col-md-8"> 
        <input type="text" id="billingaddress" placeholder="প্রার্থীর অভিভাবকের নাম"  name="parmanentGurdianName" class="form-control-md form-control" required></div>

        <div class="col-md-4 mt-3"> 
          <h5 class="eng">Village/Town/ <span> <br /></span>  Road/House/Flat</h5> </div>
        <div class="col-md-8"> 
      <textarea class="form-control mt-3" placeholder="গ্রাম/শহর/রোড/বাড়ি/ফ্লাট" name="parmanentVillage" id="billingaddress2" cols="15" rows="4" required></textarea>
        </div>
        <div class="col-md-4 mt-3">  
        <h5 class="eng"> District : </h5>
        </div>
        <div class="col-md-8 mt-3"> 
        <input type="text" placeholder="জেলা" id="billingaddress3" name="parmanentDistrict" class="form-control-md form-control" required></div>
        
        <div class="col-md-4 mt-3">  
        <h5 class="eng"> P.S/Upazila : </h5>
        </div>
        <div class="col-md-8 mt-3"> 
        <input type="text" id="billingaddress4" placeholder="উপজেলা" name="parmanentUpazila" class="form-control-md form-control" required></div>

        <div class="col-md-4 mt-3">  
        <h5 class="eng"> Post Office : </h5>
        </div>
        <div class="col-md-8 mt-3"> 
        <input type="text" id="billingaddress5" placeholder="পোষ্ট-অফিস" name="parmanentPostOffice" class="form-control-md form-control" required></div>

        <div class="col-md-4 mt-3">  
        <h5 class="eng"> Post Code : </h5>
        </div>
        <div class="col-md-8 mt-3"> 
        <input type="text" placeholder="পোষ্ট-কোড" id="billingaddress6" name="parmanentPostCode" class="form-control-md form-control" required></div>

      </div>
     </div>
</div>

<div class="row bg-primary text-light"> 
<div class="col-md-12 text-center py-3"> <h4> Academic Qualifications</h4></div>

<div class="col-md-12 text-center pb-4"> <h5> JSC or Equalvalent Level<span class="red"> *</span> </h5></div>

<div class="col-md-4"> 
     <div class="row"> 
       <div class="col-md-6"><h5> Examination</h5> </div>
       <div class="col-md-6"> 
       <select name="jscExam" class="form-control" id="exam1" >
                            <option value="" selected="selected">Select One</option>
                     <option value="JDC">JDC</option>
                     <option value="JSC">JSC</option> 
                          </select>
       </div>
     </div>

</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> Board</h5> </div>
       <div class="col-md-6"> 
       <select name="jscBoard" class="form-control" id="institute1">
                            <option value="" selected="selected">Select One</option>
                            <option value="Dhaka">Dhaka</option><option value="Cumilla">Cumilla</option><option value="Rajshahi">Rajshahi</option><option value="Jashore">Jashore</option><option value="Chittagong">Chittagong</option><option value="Barishal">Barishal</option><option value="Sylhet">Sylhet</option><option value="Dinajpur">Dinajpur</option><option value="Madrasah">Madrasah</option>
                            </select>
       </div>
     </div>

</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> Roll No</h5> </div>
       <div class="col-md-6"> 
         <input class="form-control form-control-md" name="jscRoll" type="text" >
       </div>
     </div>

</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6"><h5> Group/Subject</h5> </div>
    <div class="col-md-6"> 
      <select name="jscSubject" class="form-control" id="subject1">
        <option value=""selected="selected">Select One</option>
        <option value="Genarel">Genarel</option>
      </select>
    </div>
  </div>

</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Result :</h5>
    </div>
    <div class="col-md-6" id="resultdiv"> 
    
    <select name="jscResult"  onchange="jscresultbox()" class="form-control" id="jscresult"  >
                                <option value=""selected="selected">Select</option>
											        	<option value="1st Division">1st Division</option>
                                <option value="2nd Division">2nd Division</option>
                                <option value="3rd Division">3rd Division</option>
                                <option value="GPA(out of 4)">GPA(out of 4)</option>
											        	<option value="GPA(out of 5)">GPA(out of 5)</option>
                              </select>

                              <div id="gpajsc" style="margin-top: 10px;"> 
                             
                              </div>
     
    </div>
    
    <script>
    
    function jscresultbox(){
        var jsc = document.getElementById('jscresult').value;
       if(jsc==='GPA(out of 4)'||jsc=== 'GPA(out of 5)'){
         document.getElementById('gpajsc').innerHTML='<input class="form-control form-control-md" name="jscGPA" type="text" placeholder="পয়েন্ট ">'

       }else{
        document.getElementById('gpajsc').innerHTML="<span> </span>"
       }
      }

    </script>
  </div>
</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Passing Year :</h5>
    </div>
    <div class="col-md-6" id="passing year"> 
    
      <select name="jscYear" class="form-control" id="year2">
        <option value="0" selected="selected">Select One</option>
        <option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option>
      </select>
     
    </div>
  </div>
</div>

 


<div class="col-md-12 text-center py-5"> <h5> SSC or Equalvalent Level<span class="red"> *</span> </h5></div>

<div class="col-md-4"> 
     <div class="row"> 
       <div class="col-md-6"><h5> Examination</h5> </div>
       <div class="col-md-6"> 
       <select name="sscExam" class="form-control" id="exam1" required>
                            <option value="" selected="selected">Select One</option>
                     <option value="S.S.C">S.S.C</option>
                     <option value="Dakhil">Dakhil</option>
                     <option value="S.S.C Vocational">S.S.C Vocational</option>
                     <option value="O Level/Cambridge">O Level/Cambridge</option>
                     <option value="S.S.C Equivalent">S.S.C Equivalent</option>
                          </select>
       </div>
     </div>

</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> Board</h5> </div>
       <div class="col-md-6"> 
       <select name="sscBoard" class="form-control" id="institute1" required>
                            <option value="" selected="selected">Select One</option>
                            <option value="Dhaka">Dhaka</option>
                            <option value="Cumilla">Cumilla</option>
                            <option value="Rajshahi">Rajshahi</option>
                            <option value="Jashore">Jashore</option>
                            <option value="Chittagong">Chittagong</option>
                            <option value="Barishal">Barishal</option>
                            <option value="Sylhet">Sylhet</option>
                            <option value="Dinajpur">Dinajpur</option>
                            <option value="Madrasah">Madrasah</option>
                            <option value="Technical">Technical</option>
                            <option value="Cambridge International - IGCE">Cambridge International - IGCE</option>
                            <option value="Edexcel International">Edexcel International</option>
                            <option value="Bangladesh Technical Education Board (BTEB)">Bangladesh Technical Education Board (BTEB)</option>
                            <option value="Others">Others</option>
                          </select>
       </div>
     </div>

</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> Roll No</h5> </div>
       <div class="col-md-6"> 
         <input name="sscRoll" class="form-control form-control-md" type="text" required>
       </div>
     </div>

</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6"><h5> Group/Subject</h5> </div>
    <div class="col-md-6"> 
      <select name="sscGroup" class="form-control" id="subject1" required>
        <option value=""selected="selected">Select One</option>
        <option value="Science">Science</option>
        <option value="Humanities">Humanities</option>
        <option value="Business Studies">Business Studies</option>
        <option value="Agriculture Technology">Agriculture Technology</option><option value="Architecture and Interior Design Technology">Architecture and Interior Design Technology</option><option value="Automobile Technology">Automobile Technology</option><option value="Civil Technology">Civil Technology</option><option value="Computer Science &amp; Technology">Computer Science &amp; Technology</option><option value="Chemical Technology">Chemical Technology</option><option value="Electrical Technology">Electrical Technology</option><option value="Data Telecommunication and Network Technology">Data Telecommunication and Network Technology</option><option value="Electrical and Electronics Technology">Electrical and Electronics Technology</option><option value="Environmental Technology">Environmental Technology</option><option value="Instrumentation &amp; Process Control Technology">Instrumentation &amp; Process Control Technology</option><option value="Mechanical Technology">Mechanical Technology</option><option value="Mechatronics Technology">Mechatronics Technology</option><option value="Power Technology">Power Technology</option><option value="Refregeration &amp; Air Conditioning Technology">Refregeration &amp; Air Conditioning Technology</option><option value="Telecommunication Technology">Telecommunication Technology</option><option value="Electronics Technology">Electronics Technology</option><option value="Library Science">Library Science</option><option value="Survey">Survey</option><option value="General Mechanics">General Mechanics</option><option value="Firm Machinery">Firm Machinery</option><option value="Textile Technology">Textile Technology</option><option value="Others">Others</option>
      </select>
    </div>
  </div>

</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Result :</h5>
    </div>
    <div class="col-md-6" id="sscresultbox"> 
    
    <select name="sscResult" onchange="sscpoint()" class="form-control" id="sscresult" required>
                                <option value="0"selected="selected">Select</option>
											        	<option value="1st Division">1st Division</option>
                                <option value="2nd Division">2nd Division</option>
                                <option value="3rd Division">3rd Division</option>
                                <option value="GPA(out of 4)">GPA(out of 4)</option>
											        	<option value="GPA(out of 5)">GPA(out of 5)</option>
												
                              </select>
        <div id="sscpointbox"> </div>
     
    </div>
    
    <script type="text/javascript">
    function sscpoint(){
      var point = document.getElementById('sscresult').value;

if (point==='GPA(out of 4)'|| point==='GPA(out of 5)') {
  document.getElementById('sscpointbox').innerHTML='<input class="mt-2 form-control" name="sscGPA" placeholder="জিপিএ নাম্বার" type="text"> '
}else{
  document.getElementById('sscpointbox').innerHTML=''
}
    }
    </script>
  </div>
</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Passing Year :</h5>
    </div>
    <div class="col-md-6" id="passing year"> 
    
      <select name="sscYear" class="form-control" id="year2" required>
        <option value="" selected="selected">Select One</option>
        <option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option>
      </select>
     
    </div>
  </div>
</div>

<div class="col-md-4 mt-3"> 
    <div class="row"> 
      <div class="col-md-6"> <h5>SSC Registration</h5></div>
      <div class="col-md-6"> 
      <input class="mt-2 form-control" name="sscRegistration" placeholder="এস এস সি রেজিস্ট্রেশন নাম্বার দিন" type="text">
      </div>
    </div>

</div>



<div class="col-md-12 text-center py-5"> <h5> HSC or Equalvalent Level<span class="red"> *</span> </h5></div>

<div class="col-md-4"> 
     <div class="row"> 
       <div class="col-md-6"><h5> Examination</h5> </div>
       <div class="col-md-6"> 
       <select name="hscExam" onchange="hscexamination()" class="form-control" id="hscexam" >
        <option value=""selected="selected">Select One</option>
        <option value="H.S.C">H.S.C</option><option value="Alim">Alim</option><option value="Business Management">Business Management</option><option value="Diploma Engineering">Diploma Engineering</option><option value="A Level/Sr. Cambridge">A Level/Sr. Cambridge</option><option value="H.S.C Equivalent">H.S.C Equivalent</option><option value="Diploma in Pharmacy">Diploma in Pharmacy</option>
        <option value="others"> Others</option>
                          </select>

                          <div id="hscbox2">     </div>
       </div>
     
     </div>
    
</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> Board</h5> </div>
       <div class="col-md-6"> 
       <select name="hscBoard" class="form-control" id="institute1">
                            <option value="0" selected="selected">Select One</option>
                            <option value="Dhaka">Dhaka</option>
                            <option value="Cumilla">Cumilla</option>
                            <option value="Rajshahi">Rajshahi</option>
                            <option value="Jassore">Jassore</option>
                            <option value="Chittagong">Chittagong</option>
                            <option value="Barishal">Barishal</option>
                            <option value="Sylhet">Sylhet</option>
                            <option value="Dinajpur">Dinajpur</option>
                            <option value="Madrasah">Madrasah</option>
                            <option value="Technical">Technical</option>
                            <option value="Cambridge International - IGCE">Cambridge International - IGCE</option>
                            <option value="Edexcel International">Edexcel International</option>
                            <option value="Bangladesh Technical Education Board (BTEB)">Bangladesh Technical Education Board (BTEB)</option>
                            <option value="Others">Others</option>
                          </select>
       </div>
     </div>

</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> Roll No</h5> </div>
       <div class="col-md-6"> 
         <input name="hscRoll" class="form-control form-control-md" type="text">
       </div>
     </div>

</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6"><h5> Group/Subject</h5> </div>
    <div class="col-md-6" name="hscSubject"  id="hscsubject"> 
     
         <div id="extra"> 
         <input name="" class="form-control form-control-md" type="text">
        </div>
 
     
    </div>
  </div>
  
<script>
    
  function hscexamination(){
      var hsc = document.getElementById('hscexam').value;

    if (hsc==='H.S.C' || hsc==='Alim' || hsc==='Business Management' || hsc==='A Level/Sr. Cambridge' || hsc==='H.S.C Equivalent') {
      document.getElementById('hscsubject').innerHTML='<select name="hscSubject" id="sub" class="form-control "><option>Select One</option><option>Science</option><option>Humanities</option> <option>Bussness Studes</option> <option value="Others">Others</option> </select>'

    }else if(hsc==='Diploma Engineering' || hsc==='Diploma in Pharmacy'){
      document.getElementById('hscsubject').innerHTML='<select onchange="newbox()" id="submenu" name="hscSubject" class="form-control "><option value="Agriculture Technology">Agriculture Technology</option> <option value="Architecture and Interior Design Technology">Architecture and Interior Design Technology</option><option value="Automobile Technology">Automobile Technology</option><option value="Civil Technology">Civil Technology</option><option value="Computer Science & Technology">Computer Science & Technology</option><option value="Chemical Technology">Chemical Technology</option><option value="Electrical Technology">Electrical Technology</option><option value="Data Telecommunication and Network Technology">Data Telecommunication and Network Technology</option><option value="Electrical and Electronics Technology">Electrical and Electronics Technology</option><option value="Environmental Technology">Environmental Technology</option><option value="Instrumentation & Process Control Technology">Instrumentation & Process Control Technology</option><option value="Mechanical Technology">Mechanical Technology</option><option value="Mechatronics Technology">Mechatronics Technology</option><option value="Power Technology">Power Technology</option><option value="Refregeration & Air Conditioning Technology">Refregeration & Air Conditioning Technology</option><option value="Telecommunication Technology">Telecommunication Technology</option><option value="Electronics Technology">Electronics Technology</option><option value="Library Science">Library Science</option><option value="Survey">Survey</option><option value="General Mechanics">General Mechanics</option><option value="Firm Machinery">Firm Machinery</option><option value="Printing Technology">Printing Technology</option><option value="others">Others</option></select>'
   
    }else if( hsc==='others'){
      document.getElementById('hscsubject').innerHTML='<input name ="hscSubject" class="form-control mt-2" type="text">'
    }
    }

  </script>
</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Result :</h5>
    </div>
    <div class="col-md-6" id="hscresultbox"> 
    
    <select name="hscResult" class="form-control" id="hscgradpoint" onchange="hscpoint()"  >
                                <option value=""selected="selected">Select</option>
											        	<option value="1st Division">1st Division</option>
                                <option value="2nd Division">2nd Division</option>
                                <option value="3rd Division">3rd Division</option>
                                <option value="GPA(out of 4)">GPA(out of 4)</option>
											        	<option value="GPA(out of 5)">GPA(out of 5)</option>
												
                              </select>
                <div id="hscpointbox2"> </div>
     
    </div>
    
    <script type="text/javascript">

    function hscpoint(){
      var point = document.getElementById('hscgradpoint').value;

if (point==='GPA(out of 4)'|| point==='GPA(out of 5)') {
  document.getElementById('hscpointbox2').innerHTML='<input name="hscGPA" class="mt-2 form-control" placeholder="জিপিএ নাম্বার" type="text"> '
}else{
  document.getElementById('hscpointbox2').innerHTML=''
}
    }
    </script>
  </div>
</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Passing Year :</h5>
    </div>
    <div class="col-md-6" id="passing year"> 
    
      <select name="hscYear" class="form-control" id="year2">
        <option value="0" selected="selected">Select One</option>
        <option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option>
      </select>
     
    </div>
  </div>
</div>

<div class="col-md-4 mt-3"> 
    <div class="row"> 
      <div class="col-md-6"> <h5>HSC Registration</h5></div>
      <div class="col-md-6"> 
      <input class="mt-2 form-control" name="hscRegistration" placeholder= "এইচ এস সি রেজিস্ট্রেশন নাম্বার দিন" type="text">
      </div>
    </div>

</div>


<div class="col-md-12 text-center py-5"> <h5> Degree/Honars or Equalvalent Level<span class="red"> *</span> </h5></div>

<div class="col-md-4"> 
     <div class="row"> 
       <div class="col-md-6"><h5> Examination</h5> </div>
       <div class="col-md-6"> 
       <select name="degreeExam" onchange="dgree()" class="form-control" id="dgreeid">
                      <option value="" selected="selected">Select One</option>
                      <option value="B.A (Honors)">B.A (Honors) </option>
                      <option value="B.Com (Honors)">B.Com (Honors)</option>
                      <option value="B.Ed (Honors)">B.Ed (Honors)</option>
                      <option value="B.S.S (Honors)"> B.S.S (Honors)</option>
                      <option value="B.Sc (Honors)">B.Sc (Honors)</option>
                      <option value="LL.B (Honors) "> LL.B (Honors) </option>
                      <option value="B.A(PassCourse)"> B.A(PassCourse) </option>
                      <option value="B.Com (pass Course)"> B.Com (pass Course)</option>
                      <option value="BBS (pass course)"> BBS (pass course) </option>
                      <option value="B.Sc (pass course)">B.Sc (pass course) </option>
                      <option value="B.S.S (pass Course)">B.S.S (pass Course) </option>
                      <option value="L.L.B (pass course)">L.L.B (pass course)</option>
                      <option value="M.B.B.S/ B.D.S ">M.B.B.S/ B.D.S </option>
                      <option value="B.Sc(Engineering/Architecture)">B.Sc(Engineering/Architecture)</option><option value="B.Sc(Agricultural Science)">B.Sc(Agricultural Science)</option>
                      <option value="B.tech"> B.tech</option>
                      <option value="BBA">BBA</option>
                      <option value="BBS">BBS</option>
                      <option value="Fazil">Fazil</option>
                      <option value="Others">Others</option>
                    </select>
       </div>
     </div>
     
</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> Subject / Degree</h5> </div>
       <div class="col-md-6" id="dgreesub"> 

       <input class="form-control" name="degreeSubject" type="text" >
       </div>
     </div>

</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> Course Duration</h5> </div>
       <div class="col-md-6"> 
        <select name="degreeCourseDuration" class="form-control form-control-md" id="duration3">
          <option value="" selected="selected">Select One</option>
          <option value="02 Years">02 Years</option>
          <option value="03 Years">03 Years</option>
          <option value="04 Years">04 Years</option>
          <option value="05 Years">05 Years</option>
          
        </select>
       </div>
     </div>

</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6"><h5> University/Institute</h5> </div>
    <div class="col-md-6"> 
<input type="text" class="form-control form-control-md " name="degreeUniversity">
    </div>
  </div>

</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Result :</h5>
    </div>
    <div class="col-md-6" id="dgreebox"> 
    
    <select name="degreeResult" onchange="dgreeresultbox()" class="form-control" id="degreeid"  >
                                <option value=""selected="selected">Select</option>
											        	<option value="1st Division">1st Division</option>
                                <option value="2nd Division">2nd Division</option>
                                <option value="3rd Division">3rd Division</option>
                                <option value="GPA(out of 4)">GPA(out of 4)</option>
											        	<option value="GPA(out of 5)">GPA(out of 5)</option>
												
                              </select>
                              <div id="degree2"> </div>
     
    </div>
    
    <script type="text/javascript">

function dgreeresultbox(){
  var point = document.getElementById('degreeid').value;

if (point==='GPA(out of 4)'|| point==='GPA(out of 5)') {
document.getElementById('degree2').innerHTML='<input name="degreeCGPA" class="mt-2 form-control" placeholder="সি জিপিএ নাম্বার" type="text"> '
}else{
document.getElementById('degree2').innerHTML=''
}
}
</script>
  </div>
</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Passing Year :</h5>
    </div>
    <div class="col-md-6" id="passing year"> 
    
      <select name="degreePassingYear" class="form-control" id="year2">
        <option value="" selected="selected">Select One</option>
        <option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option>
      </select>
     
    </div>
  </div>
</div>



<div class="col-md-12 text-center py-5"> <h5> Master Equalvalent Level<span class="red"> *</span> </h5></div>

<div class="col-md-4"> 
     <div class="row"> 
       <div class="col-md-6"><h5> Examination</h5> </div>
       <div class="col-md-6"> 
       <select name="masterExam" class="form-control" id="exam1" >
                            <option value="" selected="selected">Select One</option>
                     <option value="M.A">M.A</option>
                     <option value="M.S.S">M.S.S</option>
                     <option value="M.SC">M.SC</option>
                     <option value="M.COM">M.COM</option>
                     <option value="M.B.A">M.B.A</option>
                     <option value="L.L.M">L.L.M</option>
                     <option value="M.Phil">M.Phil</option>
                     <option value="Kamil">Kamil</option>
                     <option value="Others">Others</option>
                          </select>
       </div>
     </div>

</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5> University/Institute</h5> </div>
       <div class="col-md-6"> 
          <input name="masterUniversity" class="form-control form-control-md" type="text" >
       </div>
     </div>
</div>
<div class="col-md-4"> 
<div class="row"> 
       <div class="col-md-6"><h5>Course Duration </h5> </div>
       <div class="col-md-6"> 
         <select class="form-control" name="masterCourseDuration" id=""> 
            <option value=""> Select one</option>
            <option value="2 Years"> 2 Years</option>
            <option value="1 Years"> 1 Years </option>
         </select>
       </div>
     </div>

</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6"><h5> Subject/Degree</h5> </div>
    <div class="col-md-6"> <input name="masterSubject" class="form-control form-control-md" type="text">  
      </div>
  </div>
</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Result :</h5>
    </div>
    <div class="col-md-6" id="masterbox"> 
    
    <select name="masterResult" onchange="masterresultbox()" class="form-control" id="masterid"  >
                                <option value=""selected="selected">Select</option>
											        	<option value="1st Class">1st Class</option>
                                <option value="2nd Class">2nd Class</option>
                                <option value="3rd Class">3rd Class</option>
                                <option value="CGPA(out of 4)">CGPA(out of 4)</option>
											        	<option value="CGPA(out of 5)">CGPA(out of 5)</option>
												
                              </select>
                              <div id="master2"> </div>
     
    </div>
    
    <script type="text/javascript">

function masterresultbox(){  
  var point = document.getElementById('masterid').value;

if (point==='CGPA(out of 4)'|| point==='CGPA(out of 5)') {
document.getElementById('master2').innerHTML='<input class="mt-2 form-control" name="masterCGPA" placeholder="সি জিপিএ নাম্বার" type="text"> '
}else{
document.getElementById('master2').innerHTML=''
}
}
</script>
  </div>
</div>

<div class="col-md-4 mt-3"> 
  <div class="row"> 
    <div class="col-md-6 "> 
      <h5 class="eng"> Passing Year :</h5>
    </div>
    <div class="col-md-6" id="passing year"> 
    
      <select name="masterPassingYear" class="form-control" id="year2">
        <option value="0" selected="selected">Select One</option>
        <option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option>
      </select>
     
    </div>
  </div>
</div>





</div>

<div class="row bg-primary text-light"> 
  <div class="col-md-12 py-5 text-center"> 
    <h4>Professional Experience</h4>
  </div>
    <div class="col-md-3"><h5> Designation/Post name</h5> </div>
    <div class="col-md-3"> <input class="form-control form-control-md" name="postName" placeholder="পদবী/পদের নাম" type="text"></div>
    <div class="col-md-3"><h5> Organization Name</h5> </div>
    <div class="col-md-3"> <input class="form-control form-control-md" name="organizationName" placeholder="প্রতিষ্ঠানের নাম" type="text"></div>
    <div class="col-md-3 mt-3 "><h5> Service Start Date</h5> </div>
    <div class="col-md-3 mt-3"> <input class="form-control form-control-md" name="jobStartTime" placeholder="সার্ভিস শুরুর তারিখ" type="text"></div>
    <div class="col-md-3 mt-3"><h5> Service End date</h5> </div>
    <div class="col-md-3 mt-3"> <input class="form-control form-control-md" name="jobEndTime" placeholder="সার্ভিস সমাপ্তির তারিখ" type="text"></div>

    <div class="col-md-6 mt-5"><h5 class="text-center"> Responsibilities</h5> </div>
    <div class="col-md-6 mt-5"> <input class="form-control form-control-md" name="jobResponsibility" placeholder="দায়িত্ব" type="text"></div>
</div>

<div class="row mt-4 bg-primary text-light"> 
       <div class="col-md-4"><h5 class=""> Departmental Candidate Status</h5> </div>
       <div class="col-md-3"> 
       <input class="form-control form-control-md" name="departmentalCandidateStatus" placeholder="বিভাগীয় প্রার্থীর অবস্থা" type="text">
       </div>
       <div class="col-md-5"> 
        <div class="row"> 
          <div class="col-md-6"> <h5>Minimum weight </h5></div>
          <div class="col-md-6"> <input class="form-control form-control-md" name="weight" placeholder="আপনার ওজন" type="text"> </div>
        </div>   
      </div>

      <div class="col-md-3 mt-3"><h5>Minimum Height (Feet-inc)</h5> </div>
    <div class="col-md-3 mt-3"> <input class="form-control form-control-md" name="height" placeholder="আপনার উচ্চতা" type="text"></div>
    <div class="col-md-3 mt-3"><h5>Minimum Chest Size </h5> </div>
    <div class="col-md-3 mt-3"> <input class="form-control form-control-md"   name="chestSize" placeholder="আপনার বুকের মাপ" type="text"></div>

    <div class="col-md-6 text-center mt-4 "><h5> Computer Training Institute Name </h5> </div>
    <div class="col-md-6 mt-4"> <input class="form-control form-control-md" name="computerTrainingServices" placeholder=" " type="text"></div>
       <div class="col-md-6 mt-4"> <h5> Typeing speed (1 min)</h5></div>
       <div class="col-md-3 mt-4"> <input class="form-control form-control-md" placeholder=" Bangla" name="banglaTypingSpeed" type="text"> </div>
       <div class="col-md-3 mt-4">  <input class="form-control form-control-md" placeholder=" English" type="text" name="englishTypingSpeed"></div>
    <div class="col-md-3 mt-4"> 
       <h5> Upload photo <span class="red">*</span></h5>
     </div>
    <div class="col-md-3 mt-4"> 
       <input class="form-control-file form-control-file-md" type="file"  name="applicantPhoto" required>
    </div>

    <div class="col-md-3 mt-4"> 
       <h5> Upload Signuture <span class="red">*</span></h5>
     </div>
    <div class="col-md-3 mt-4"> 
       <input class="form-control-file form-control-file-md" type="file" name="applicantSignature" required>
    </div>
  <!-- password field here -->
  <div class="mt-4 row password-field"> 
  <div class="col-md-3 mt-4"> 
       <h5> Type Password <span class="red">*</span></h5>
     </div>
    <div class="col-md-3 mt-4"> 
    <input class="form-control form-control-md" placeholder="পাসওয়ার্ড দিন " name="password" type="password" required>
    </div>
    <div class="col-md-3 mt-4"> 
       <h5> Confirm  Password <span class="red">*</span></h5>
     </div>
    <div class="col-md-3 mt-4"> 
    <input class="form-control form-control-md" placeholder="পুনরায় পাসওয়ার্ড দিন " name="confirmPassword" type="password" required>
    </div>
  </div>

</div>

<div class="col-md-12 mt-5 text-center"> 
<button type="submit" class="btn btn-lg btn-warning ">
								Apply now
							</button>
</div>

  </form> 

</div>
</body>
<script type="text/javascript">
  function setBillingAddress(){
    if ($("#homepostalcheck").is(":checked")) {
      $('#billingaddress').val($('#homeaddress').val()); 
      $('#billingaddress2').val($('#homeaddress2').val());
      $('#billingaddress3').val($('#homeaddress3').val()); 
      $('#billingaddress4').val($('#homeaddress4').val());
      $('#billingaddress5').val($('#homeaddress5').val());
      $('#billingaddress6').val($('#homeaddress6').val());
    } else {
      $('#billingaddress').removeAttr('disabled');
      $('#billingaddress2').removeAttr('disabled');
      $('#billingaddress3').removeAttr('disabled');
      $('#billingaddress4').removeAttr('disabled');
      $('#billingaddress5').removeAttr('disabled');
      $('#billingaddress6').removeAttr('disabled');
    }
  }
  
  $('#homepostalcheck').click(function(){
    setBillingAddress();
  })
  
  </script>

<script src="js/jquery-3.5.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="custom.js"></script>
</html>
