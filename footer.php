<?php

$sql="SELECT * FROM footer limit 1";
$statement=$connection->prepare($sql);
$statement->execute();
$footer=$statement->fetch(PDO::FETCH_OBJ);



?>
<!-----Footer Part -->
<footer>  
<section id="Contact" class="bg-dark text-light"> 
	<div class="container"> 
		<div class="row py-4"> 
		
			<div class="col-md-5 py-3">
				<h4 style="
				padding: 11px;"><strong>  <?=$footer->name;?></strong></h4>
				<p> <?=$footer->fullDescription;?> </p>
				<br>
				<div> <h6>We are Accept Payment: </h6></div>
					<div class="d-flex"> 
						<div> <img style="width:auto;height:62px" src="images/icon/BKash-Icon.svg" alt="Bikash"></div>
						<div><img style="width:auto;height:62px" src="images/icon/Nagad-Vertical-Logo.wine.svg" alt="Nagad">
						</div>
						<div><img style="width:auto;height:50px;margin-right:12px" src="images/icon/184568.svg" alt="Rocket">
						</div>
						<div><img style="width:auto;height:50px" src="images/icon/png-transparent-mastercard-logo-logo-payment-visa-mastercard-paypal-mastercard-icon-text-service-mobile-payment-thumbnail.png" alt="">
						</div>
					</div>
					<img  style="width:auto;height:34px" src="images/icon/footer_logo.png" alt="SSLCOMMARZE">
				</div>
				<div class="col-md-3 py-3"> 
				<h5>গুরুত্বপূর্ন লিংক সমূহ</h5>
					<ul class="links text-light">
						<li><a class="text-light" href="index.php">হোম</a></li>
						<li><a class="text-light" href="privacy.php"> গোপনীয়তা </a></li>
						<li><a class="text-light" href="aboutus.php">আমাদের সম্পর্কে</a></li>
					
					</ul>
				</div>
			
			<div class="col-md-4 py-3"> 
				<h5>আমাদের সাথে যোগাযোগ করুন</h5>
					<ul class="contactes">
						<li><?=$footer->phone;?></li>
						<li><?=$footer->email;?></li>
					</ul>
			
				<div class="follow_us">
					<ul class="socialIcon">
						<li><p class="lead">ফলো করুন</p></li>
						<li class="icons">
						<a target="_blank" href="https://web.facebook.com/osbdgovtjobcircular/"> <img src="images/icon/002-facebook.png" alt=""> </a>
						</li>
						<li class="icons">
						<a target="_blank" href="https://twitter.com/osbd78"> <img src="images/icon/003-twitter.png" alt=""> </a>
						</li>
						<li class="icons">
						<a target="_blank" href="https://instagram.com/osbd78/"><img src="images/icon/001-instagram.png" alt=""></a>
						</li>
						<li class="icons"><img src="images/p+.png" alt=""></li>						
					</ul>
				</div>

				</div>
				
			</div>
		</div>
</section>

</footer>

</div>
	<script src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/acor.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script async src="https://static.addtoany.com/menu/page.js"></script>
<script type="text/javascript"> 
$('.time').carousel({
  interval: 3000
})
</script>

</body>
</html>