<?php
ob_start();
session_start();
require 'db.php';
require 'test.php';

$email='';
if(array_key_exists('email',$_SESSION)){
  $email=$_SESSION['email'];
     
  }
  
  $number='';
  if(array_key_exists('number',$_SESSION)){
      $number=$_SESSION['number'];
    
      }

      if( $email=='' && $number==''){
        require 'header.php';
      }
    
    
    if(!empty($email) || !empty($number)){
        require 'profile_header.php';
    }



    
$limit = 5;

$sql='SELECT * from blog';
$statement=$connection->prepare($sql);

$statement->execute();
$blogs=$statement->fetchAll(PDO::FETCH_OBJ);


$total_results = $statement->rowCount();
$total_pages = ceil($total_results/$limit);

if (!isset($_GET['page'])) {
    $page = 1;
} else{
    $page = $_GET['page'];
}


$start = ($page-1)*$limit;

$stmt = $connection->prepare("SELECT * FROM blog ORDER BY id DESC LIMIT $start, $limit");
$stmt->execute();

// set the resulting array to associative
$stmt->setFetchMode(PDO::FETCH_OBJ);

$blog = $stmt->fetchAll();
   
?>

<!-- Responsive mode View -->
<style> 
a {
    color: #060606;
    text-decoration: none;
    background-color: transparent;
}
a:hover {
    color: #0c0cdb;
    text-decoration: none;
}
@media only screen and (max-width: 600px) {
  .blimg {
    height: 100px;
  }
  .description{
  
    display: none;
  }
}
</style>



<div class="py-5 bg-light"> 
<div class="container"> 
   
<ul class="list-unstyled">
  <?php foreach($blog as $singleBlog): ?>
          
  <li class="media mb-5 pb-2">

    <img class="w-25 blimg mr-3" src="images/<?=$singleBlog->image;?>" height="182px" alt="Generic placeholder image">
    <a href="blogFullDescription.php?id=<?=$singleBlog->id;?>">   
    <div class="media-body"> 
      <h5 class="mt-0 mb-1"><?=$singleBlog->title;?></h5>
      <p class="text-muted">
    
         <?php 
         $origDate=substr($singleBlog->date,0,10);
     

        $newDate = date("d-m-Y", strtotime($origDate));
        $onlyDate=substr($newDate,0,2);
        echo $onlyDate;
        
        $yrdata= strtotime($newDate);
        $fdate= date('-M-Y', $yrdata);
        echo $fdate;

      ?>
      </p>
    <div class="description"> 
      <p class=" mt-0 mb-1"><?=lengthCheck($singleBlog->shortDescription,0,500);?></p>
      
      </div>
      </a>
    </div>
  </li>
  
  <?php endforeach;?>

</ul>

<ul class="pagination">
<li class="page-item"><a class="page-link" href="?page=1">First</a></li>
        
        <?php for($p=1; $p<=$total_pages; $p++){?>
            
            <li class="<?= $page == $p ? 'active' : ''; ?>"><a class="page-link" href="<?= '?page='.$p; ?>"><?= $p; ?></a></li>
        <?php }?>
        <li class="page-item"><a class="page-link" href="?page=<?= $total_pages; ?>">Last</a></li>
</ul> 
</div>
</div>

<?php require_once("footer.php")?>