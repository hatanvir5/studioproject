<?php 
include "db.php";

$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

 
	$page = basename($_SERVER['PHP_SELF']);

	switch ($page) {
		case 'index.php':
			$page_title = "Welcome to Online Studio";
			break;
			case 'chakri.php':
				$page_title = "Job Post || Welcome to Online Studio";
				break;
				case 'userlogin.php':
					$page_title = "Log in || Welcome to Online Studio";
					break;
					case 'cvform.php':
						$page_title = "Create Cv || Welcome to Online Studio";
						break;
						case 'Blogpost.php':
							$page_title = "Blog Post || Welcome to Online Studio";
							break;
							case 'Contact.php':
								$page_title = "Contact Us || Welcome to Online Studio";
								break;
								case 'profilr.php':
									$page_title = "Profile|| Welcome to Online Studio";
									break;
									case 'activity.php':
										$page_title = "Activity || Welcome to Online Studio";
										break;
										case 'changepass.php':
											$page_title = "Password Change || Welcome to Online Studio";
											break;
											case 'jobapplication.php':
												$page_title = "Apply Job || Welcome to Online Studio";
												break;
			case 'blogFullDescription.php':
				
				if (isset($_GET['id'])) {
					$id = $_GET['id'];
				$sql_title = "SELECT * FROM blog WHERE id = '{$id}'";
				$result_title = $connection->prepare($sql_title);
				$result_title->execute();
				$blog = $result_title->fetch(PDO::FETCH_OBJ);
				$page_title = $blog->title;
				$page_description =htmlspecialchars($blog->shortDescription);
			
				}
				break;
		default:
		$page_title = "Welcome to Online Studio";
		$page_description = "Welcome to Online Studio";
			break;
	}
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title><?=  $page_title; ?></title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta property="og:title" content="<?= $page_title; ?>" />
  <meta property="og:description" content="<?= $page_description; ?>" />
  <meta property="og:url" content="<?= $url;?>" />
  <meta property="og:site_name" content="Online Studio BD" />
  
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/aos.css" media="all" />	
  <link rel="shortcut icon" type="image/x-icon" href="img/mlogo.png">
 
  <script src="js/jquery.min.js"></script>
	<script src="js/bjqs-1.3.min.js"></script>	
	<link rel="stylesheet" href="css/bjqs.css">
  <link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
  <link rel="icon" href="images/mainlogo.png" type="image/gif" sizes="16x16"> 

<!-- delete this -->

</head>
<body>
<script>window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t;
}(document, "script", "twitter-wjs"));</script>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v8.0" nonce="zMqk8hI0"></script>
	<div class="container"> 
<!-------------- navbar header ------------------>
<nav class="navbar navbar-dark bg-dark py-3 navbar-expand-md">
<div class="container"> 
		<a class="navbar-brand" href="index.php">
		
		<h3 class="d-inline text-light align-middle">
		<img src="images/mainlogo.png" alt="Logo" style="height: 46px;"> </h3> </a>		
	<button class="navbar-toggler" data-toggle="collapse" data-target="#navbar-item" > <span class="navbar-toggler-icon" > </span></button>
	<div class="collapse navbar-collapse" id="navbar-item"> 	
		<ul class="navbar-nav ml-auto"> 
	<li class="nav-item">
		<a class="nav-link" href="index.php">হোম</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="chakri.php">চাকুরি</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="jobapplication.php">আবেদন করুন</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="Blogpost.php">ব্লগ</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="cvform.php">সিভি</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="userlogin.php">লগ ইন</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="Contact.php">যোগাযোগ</a>
	</li>
</ul>
	</div>
</div>
</nav>

