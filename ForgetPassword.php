<?php 
session_start();
require_once("header.php");
?>
<!-- Custom css start just use only contact form -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<!-- Custom  css end just use only contact form -->

<div class="container-contact1000 py-3 bg-primary">
		<div class="wrap-contact100 ">
			<div class="ml-auto py-4"> <h2> Forget Password ! </h2></div>
<div>
	<?php 
		  if(array_key_exists('error_message',$_SESSION)){
			echo  '<p class="text-center bg-warning text-white p-2 mb-2">'. $_SESSION['error_message'].'</p><br>';
			unset($_SESSION['error_message']);
		  }
	?>
</div>

<div id="accordion">
<div class="d-flex"> 
	<div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-success" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
         Recovery With Email
        </button>
      </h5>
	</div>
	<div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-success collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Recovery With Phone
        </button>
      </h5>
    </div>
</div>
  <div class="card">
   

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
	  <form action="otpProcess.php" method="post" class="contact100-form validate-form">
				<div class="wrap-input100 validate-input">
					<span class="label-input100">Email:</span>
					<input class="input100" type="email" name="email" placeholder="Enter Email address">
					<span class="focus-input100"></span>
				</div>
			<div class="container-contact100-form-btn">
					<button type="submit" class="contact100-form-btn">
						 Submit
					</button>
				</div>
			</form>

      </div>
    </div>
  </div>
  <div class="card">
 
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
	  <form action="smsOtpProcess.php" method="post" class="contact100-form validate-form">
				<div class="wrap-input100 validate-input">
					<span class="label-input100">Phone:</span>
					<input class="input100" type="text" name="phone" placeholder="Enter Phone Number">
					<span class="focus-input100"></span>
				</div>
			<div class="container-contact100-form-btn">
					<button type="submit" class="contact100-form-btn">
						 Submit
					</button>
				</div>
			</form>
      </div>
    </div>
  </div>

</div>


		
		</div>
	</div>
<?php require_once("footer.php")?>