-- for general users login
create TABLE users(
    id int(11) not null auto_increment primary key,
    email varchar(50) not null,
    password varchar(50) not null,
    number VARCHAR(15) not null,
    registrationId int(11) not null,
    FOREIGN KEY (registrationId) REFERENCES registrations(id) ON delete cascade on update cascade
);

CREATE TABLE registrations1(
registrationsId1 int(11) not null auto_increment primary key,
applicantName VARCHAR(40),
fatherName VARCHAR(40),
motherName VARCHAR(40),
birthDate VARCHAR(5),
birthMonth VARCHAR(15),
birthYear VARCHAR(8),
gender VARCHAR(10),
quota varchar(40),
religion VARCHAR(15),
birthPlace VARCHAR(30),
bloodGroup VARCHAR(15),
nid VARCHAR(30),
passport VARCHAR(30),
birthCirtificate VARCHAR(30),
maritalStatus VARCHAR(20),
wifeName VARCHAR(40),
number VARCHAR(15),
presentGurdianName VARCHAR(40),
PresentVillage VARCHAR(40),
presentDistrict VARCHAR(20),
presentUpazila VARCHAR(20),
presenPostOffice VARCHAR(20),
presentPostCode  VARCHAR(20),
homePostalCHeck TINYINT,
parmanentGurdianName VARCHAR(40),
parmanentVillage varchar(40),
parmanentDistrict VARCHAR(20),
parmanentUpazila VARCHAR(20),
parmanentPostOffice VARCHAR(20),
parmanentPostCode VARCHAR(20)
)

CREATE TABLE registrations(
id int(11) not null auto_increment primary key,
registrationsId1 int(11) not null
registrationsId2 int(11) not null

FOREIGN KEY(registrationsId1) REFERENCES registrations1(registrationsId1) ON delete cascade on update cascade
FOREIGN KEY(registrationsId2) REFERENCES registrations2(registrationsId2) ON delete cascade on update cascade

)
-- for general users registrations info

CREATE TABLE registrations2(
registrationsId2 int(11) not null auto_increment primary key,
jscExam VARCHAR(30),
jscBoard VARCHAR(20),
jscRoll VARCHAR(20),
jscSubject VARCHAR(20),
jscResult VARCHAR(20),
jscGPA float(4,2),
jscYear VARCHAR(10),
sscExam VARCHAR(30),
sscBoard VARCHAR(20),
sscRoll VARCHAR(20),
sscGroup VARCHAR(40),
sscResult VARCHAR(20),
sscGPA float(4,2),
sscYear VARCHAR(10),
hscExam VARCHAR(30),
hscBoard VARCHAR(20),
hscRoll VARCHAR(20),
hscSubject VARCHAR(40),
hscResult VARCHAR(20),
hscGPA float(4,2),
hscYear VARCHAR(10),
degreeExam VARCHAR(30),
degreeUniversity VARCHAR(70),
degreeCourseDuration VARCHAR(10),
degreeResult VARCHAR(20),
degreeCGPA  float(4,2),
degreePassingYear VARCHAR(10),
masterExam VARCHAR(30),
degreeSubject varchar(40),
masterUniversity VARCHAR(70),
masterCourseDuration VARCHAR(10),
masterSubject VARCHAR(60),
masterResult VARCHAR(40),
masterCGPA float(4,2),
masterPassingYear VARCHAR(10),
postName VARCHAR(40),
organizationName VARCHAR(70),
jobStartTime VARCHAR(40),
jobEndTime VARCHAR(40),
jobResponsibility VARCHAR(140),
departmentalCandidateStatus VARCHAR(100),
weight VARCHAR(10),
height VARCHAR(10),
chestSize VARCHAR(10),
computerTrainingServices text,
banglaTypingSpeed VARCHAR(10),
englishTypingSpeed VARCHAR(10),
applicantPhoto VARCHAR(40),
applicantSignature VARCHAR(40)
);
















CREATE TABLE registrations(
id int(11) not null auto_increment primary key,
applicantName VARCHAR(40),
fatherName VARCHAR(40),
motherName VARCHAR(40),
birthDate VARCHAR(5),
birthMonth VARCHAR(15),
birthYear VARCHAR(8),
gender VARCHAR(10),
quota varchar(40),
religion VARCHAR(15),
birthPlace VARCHAR(30),
bloodGroup VARCHAR(15),
nid VARCHAR(30),
passport VARCHAR(30),
birthCirtificate VARCHAR(30),
maritalStatus VARCHAR(20),
wifeName VARCHAR(40),
number VARCHAR(15),
presentGurdianName VARCHAR(40),
PresentVillage VARCHAR(40),
presentDistrict VARCHAR(20),
presentUpazila VARCHAR(20),
presenPostOffice VARCHAR(20),
presentPostCode  VARCHAR(20),
homePostalCHeck TINYINT,
parmanentGurdianName VARCHAR(40),
parmanentVillage varchar(40),
parmanentDistrict VARCHAR(20),
parmanentUpazila VARCHAR(20),
parmanentPostOffice VARCHAR(20),
parmanentPostCode VARCHAR(20),


jscExam VARCHAR(30),
jscBoard VARCHAR(20),
jscRoll VARCHAR(20),
jscSubject VARCHAR(20),
jscResult VARCHAR(20),
jscGPA float(4,2),
jscYear VARCHAR(10),
sscExam VARCHAR(30),
sscBoard VARCHAR(20),
sscRoll VARCHAR(20),
sscGroup VARCHAR(40),
sscResult VARCHAR(20),
sscGPA float(4,2),
sscYear VARCHAR(10),
hscExam VARCHAR(30),
hscBoard VARCHAR(20),
hscRoll VARCHAR(20),
hscSubject VARCHAR(40),
hscResult VARCHAR(20),
hscGPA float(4,2),
hscYear VARCHAR(10),
degreeExam VARCHAR(30),
degreeUniversity VARCHAR(70),
degreeCourseDuration VARCHAR(10),
degreeResult VARCHAR(20),
degreeCGPA  float(4,2),
degreePassingYear VARCHAR(10),
masterExam VARCHAR(30),
degreeSubject varchar(40),
masterUniversity VARCHAR(70),
masterCourseDuration VARCHAR(10),
masterSubject VARCHAR(60),
masterResult VARCHAR(40),
masterCGPA float(4,2),
masterPassingYear VARCHAR(10),
postName VARCHAR(40),
organizationName VARCHAR(70),
jobStartTime VARCHAR(40),
jobEndTime VARCHAR(40),
jobResponsibility VARCHAR(140),
departmentalCandidateStatus VARCHAR(100),
weight VARCHAR(10),
height VARCHAR(10),
chestSize VARCHAR(10),
computerTrainingServices text,
banglaTypingSpeed VARCHAR(10),
englishTypingSpeed VARCHAR(10),
applicantPhoto VARCHAR(40),
applicantSignature VARCHAR(40)


        )


create TABLE studioRegistrations(
id int(11) not null auto_increment primary key,
userName VARCHAR(40) not null,
email VARCHAR(45) not null UNIQUE,
password varchar(30) not null 
)

create TABLE jobApplicaton(
id int(11) not null auto_increment primary key,
organizationName VARCHAR(40) not null,
positionName VARCHAR(45) not null,
  userId int(11) not null,
FOREIGN KEY (userId) REFERENCES users(id) ON delete cascade on update cascade
)

create TABLE blog(
id int(11) not null auto_increment primary key,
title VARCHAR(100) not null,
catagory VARCHAR(50),
shortDescription VARCHAR(155) not null,
fullDescription VARCHAR(255) not null,
image VARCHAR(30) not null,
date CURRENT_TIMESTAMP,
)

create TABLE slider(
id int(11) not null auto_increment primary key,
imageOne VARCHAR(30) not null,
imageTwo VARCHAR(30) not null,
imageThree VARCHAR(30) not null
 
)


create TABLE(
id int(11) not null auto_increment primary key,
imageOne VARCHAR(30) not null,
imageTwo VARCHAR(30) not null,
imageThree VARCHAR(30) not null
 
)

create TABLE chakrinews(
id int(11) not null auto_increment primary key,
title VARCHAR(100) not null,
catagory VARCHAR(50),
shortDescription VARCHAR(155) not null,
fullDescription VARCHAR(255) not null,
image VARCHAR(30) not null,
date timestamp DEFAULT CURRENT_TIMESTAMP
)

create Table serviceDesign(
  serviceDesignId int(11) not null auto_increment primary key,
  serviceTitle VARCHAR(100) not null,
  titleOne VARCHAR(100) not null,
  titleTwo VARCHAR(100) not null,
  titleThree VARCHAR(100) not null,
  image VARCHAR(30) not null
)



create Table notice(
  id int(11) not null auto_increment primary key,
notice text
)



create Table serviceDetails(
serviceDetailsId int(11) not null auto_increment primary key,
title VARCHAR(100) not null,
catagory VARCHAR(50),
shortDescription VARCHAR(155) not null,
fullDescription VARCHAR(255) not null,
image VARCHAR(30) not null,
date timestamp DEFAULT CURRENT_TIMESTAMP,
serviceDesignId int(11) null,
FOREIGN KEY(serviceDesignId) REFERENCES serviceDesign(serviceDesignId) ON delete cascade on update cascade
)


create Table getServiceDesign(
  getServiceDesignId int(11) not null auto_increment primary key,
  serviceTitle VARCHAR(100) not null,
  image VARCHAR(30) not null
);
create Table getServiceDetails(
getServiceDetailsId int(11) not null auto_increment primary key,
title VARCHAR(100) not null,
catagory VARCHAR(50),
shortDescription VARCHAR(155) not null,
fullDescription VARCHAR(255) not null,
serviceImage VARCHAR(30) not null,
date timestamp DEFAULT CURRENT_TIMESTAMP,
getServiceDesignId int(11) null,
FOREIGN KEY(getServiceDesignId) REFERENCES getServiceDesign(getServiceDesignId) ON delete cascade on update cascade
);

create Table review(
id int(11) not null auto_increment primary key,
name VARCHAR(70),
designation VARCHAR(50),
fullDescription text ,
image VARCHAR(70)
)

create Table videoPage(
id int(11) not null auto_increment primary key,
title varchar(120),
fullDescription text ,
url text
)


CREATE TABLE IF NOT EXISTS `otp_expiry` (
id int(11) not null auto_increment primary key,
  `otp` varchar(20) NOT NULL,
  `is_expired` int(11) NOT NULL,
  `create_at` datetime NOT NULL
)

create Table footer(
id int(11) not null auto_increment primary key,
name VARCHAR(70),
fullDescription text ,
phone VARCHAR(15),
email VARCHAR(50)
)

create Table contact(
id int(11) not null auto_increment primary key,
name VARCHAR(70),
fullDescription text ,
phone VARCHAR(15),
email VARCHAR(50)
)



create TABLE footerExtra(
id int(11) not null auto_increment primary key,
title text(100) not null,
catagory VARCHAR(250),
shortDescription text not null,
fullDescription text not null,
image VARCHAR(70) not null,
date CURRENT_TIMESTAMP,
)

create TABLE contactimage(
id int(11) not null auto_increment primary key,
image VARCHAR(70) not null
)

