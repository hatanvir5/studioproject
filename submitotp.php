<?php 
ob_start();
session_start();
require 'header.php';

?>
<!-- Custom css start just use only contact form -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<!-- Custom  css end just use only contact form -->

<div class="container-contact1000 py-3 bg-primary">
		<div class="wrap-contact100 ">
			
			<div class="ml-auto py-4"> <h2> Submit OTP</h2></div>
<div><p class="text-center">Please Check your email or phone and get otp</p></div>
<div>
<?php 
		  if(array_key_exists('invalidOtp',$_SESSION)){
			echo  '<p class="text-center bg-warning text-white p-2 mb-2">'. $_SESSION['invalidOtp'].'</p><br>';
			unset($_SESSION['invalidOtp']);
		  }
		  if(array_key_exists('resendOtpSuccess',$_SESSION)){
			echo  '<p class="text-center bg-success text-white p-2 mb-2">'. $_SESSION['resendOtpSuccess'].'</p><br>';
			unset($_SESSION['resendOtpSuccess']);
		  }
	?>
</div>
			<form action="submitOtpProcess.php" method="post" class="contact100-form validate-form">
				<div class="wrap-input100 validate-input">
					<span class="label-input100">OTP:</span>
					<input class="input100" type="text" name="otp" placeholder="Enter OTP">
					<span class="focus-input100"></span>
				</div>
			<div class="container-contact100-form-btn">
					<button type="submit" class="contact100-form-btn">
						 Submit
					</button>
					<a class="btn btn-primary contact100-form-btn ml-3" href="resendOtp.php">Resend Otp</a>
				</div>
			</form>
		</div>
	</div>
<?php require_once("footer.php")?>