<?php
ob_start();
session_start();
require 'db.php';


$id='';
if(array_key_exists('id',$_GET)){
    $id=$_GET['id'];

}

if (array_key_exists('id', $_SESSION)) {
    $id=$_SESSION['id'];
}


$email='';
if(array_key_exists('email',$_SESSION)){
  $email=$_SESSION['email'];
     
  }
  
  $number='';
  if(array_key_exists('number',$_SESSION)){
      $number=$_SESSION['number'];
    
      }


  if( $email=='' && $number==''){
    require 'header.php';
  }


if(!empty($email) || !empty($number)){
    require 'profile_header.php';
}




$sql="SELECT * from footerextra LIMIT 1,1";
$statement=$connection->prepare($sql);
$statement->execute();
$footer=$statement->fetch(PDO::FETCH_OBJ);


?>


<style>
* {
  box-sizing: border-box;
}

/* Add a gray background color with some padding */

/* Header/Blog Title */
.header {
  padding: 30px;
  font-size: 40px;
  text-align: center;
  background: white;
}
.title{
    margin-top:28px;
}

/* Create two unequal columns that floats next to each other */
/* Left column */
.leftcolumn {   
  float: left;
  width: 75%;
}

/* Right column */
.rightcolumn {
  float: left;
  width: 25%;
  padding-left: 20px;
}

/* Fake image */
.fakeimg {
  background-color: #aaa;
  width: 100%;
  padding: 20px;
  margin-top:10px;
}

/* Add a card effect for articles */
.card {
   background-color: white;
   padding: 20px;

}
.share{
    margin-top: 20px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Footer */
.footer {
  padding: 20px;
  text-align: center;
  background: #ddd;
  margin-top: 20px;
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 800px) {
  .leftcolumn, .rightcolumn {   
    width: 100%;
    padding: 0;
  }
}
</style>
<div class="container bg-light"> 
<div class="row">
  <div class="leftcolumn">
    <div class="card">
      <h3><?=$footer->title;?></h3>
      <p>Published: <span> 
      <?php 
           $origDate=substr($footer->date,0,10);
     

           $newDate = date("d-m-Y", strtotime($origDate));
           $onlyDate=substr($newDate,0,2);
           echo $onlyDate;
           
           $yrdata= strtotime($newDate);
           $fdate= date('-M-Y', $yrdata);
           echo $fdate;
      ?>
        </span></p>
      <div><img src="images/<?=$footer->image;?>" height="auto" width="100%"></div>
      <h3 class="title"><?=$footer->catagory;?></h3>
      <p><?=$footer->fullDescription;?></p>
    </div>
  </div>
  <div class="rightcolumn">

    <div class="card share">
      <h3>Share Post</h3>
      <div class="d-flex mb-2">
      <iframe src="https://www.facebook.com/plugins/share_button.php?href=<?=$url;?>&layout=button_count&size=large&width=96&height=28&appId" width="96" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

        <a class=" twitter-share-button"
  href="https://twitter.com/intent/tweet?text=<?=$url;?>"
  data-size="large">
</a>
      </div>
      <div class="a2a_kit">
    <a class="a2a_button_linkedin_share" data-url="<?=$url;?>"></a>
</div>


    </div>
  </div>
</div>

</div>
<?php require_once("footer.php")  ?>